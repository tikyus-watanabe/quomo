package jp.co.tikyus.QuomoSDKSampleReader;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Handler;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import jp.co.tikyus.quomo.sdk;
import android.Manifest;
import android.content.pm.PackageManager;


@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity implements CameraView.OnListener{
    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("quomosdk");
    }
    private sdk _quomosdk;
    static private final int permission_camera = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        if(Build.VERSION.SDK_INT >= 23){
            //カメラ利用許可を取得
            permissionOfCamera();
        }
        else {
            start();
        }
    }
    @Override
    public void onDecode(CameraView cameraView, long id) {

        Log.d("android_onDecode","id:" + id);
/*
        String text="テキストですよ";
        ImageView view = (ImageView)findViewById(R.id.imageViewBlue);
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(1000);
        view.startAnimation(animation);
        //
        TextView textView = (TextView)findViewById(R.id.textView);
        textView.setText(text);

        Toast.makeText(this, "取得しました", Toast.LENGTH_SHORT).show();
*/

        ToneGenerator toneGenerator
                = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);

        //id:QuomoInterface::decoderの結果
        /* 一部のコードを読み取ったら1を返す
         * コードを全て読み取ったら2を返す
         * 何も読み取れない場合は0を返す
        */
        if(id == 1) {
            toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP);
            //qq
            ImageView view = (ImageView)findViewById(R.id.imageViewGreen);
            AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
            animation.setDuration(100);
            view.startAnimation(animation);
        }
        if(id == 2) {
            String text="";
            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ANSWER);
            int count = _quomosdk.QuomoGetDecodeCodeCount();

            Log.d("decoder_No14","count:" + count);

            for(int i=0;i<count;i++){
                long code  = _quomosdk.QuomoGetDecodeCode(i);
                //Log.d("MAIN","code:" + code);
                if( code != 0){
                    Log.d("decoder_No15","code:" + code);
                    text = text + code + "\n";
                }
            }
            ImageView view = (ImageView)findViewById(R.id.imageViewBlue);
            AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
            animation.setDuration(1000);
            view.startAnimation(animation);
            //
            TextView textView = (TextView)findViewById(R.id.textView);
            textView.setText(text);
            Toast.makeText(this, "取得しました", Toast.LENGTH_SHORT).show();
        }
    }

    private void start(){
        setContentView(R.layout.activity_main);
        //
        //Quomo初期設定
        _quomosdk=new sdk();
        _quomosdk.QuomoInitialize();
        boolean licenseResult;

        //Quomo証明書読み込み
        //証明書が無効である場合falseが返ってきます。有効である場合trueが返ってきます。
        licenseResult = _quomosdk.QuomoSetProductKey("1148112069b0be3b20dacf2399a8266e88afcd15476589febf64a6fd4ec07800276c605e7c5a0c336fd6f566c1a187cb6cb2f15368b8be66ac7d333c97b1557b");


        //リーダー用カメラ設定
        CameraView cameraView = (CameraView)findViewById(R.id.viewCamera);
        cameraView.setOnListener(this);
        cameraView._quomosdk = _quomosdk;
    }
    @TargetApi(23)
    private void permissionOfCamera(){
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            // permissionが許可されていません
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            }
            // 許可ダイアログの表示
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    MainActivity.permission_camera);

            return;
        }
        else{
            start();
        }
    }
    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MainActivity.permission_camera: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start();
                } else {
                    Toast.makeText(this, "カメラ使用権限を許可してください", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}

