package jp.co.tikyus.quomo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import org.opencv.core.Mat;
import org.opencv.android.Utils;
import jp.co.tikyus.quomosdk.R;
import android.graphics.Rect;
import android.util.Log;
/**
 * Created by saitouhiroyuki on 16/06/08.
 */
public class sdk {
    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("quomosdk");
    }
    public native int QuomoInitialize();
    public native boolean QuomoSetProductKey(String prodductKey);
    public native int QuomoAddCode(long code);
    public native int QuomoMakeBitmap(long mat);
    public native int QuomoMakeColors(int[] mat);
    public native void QuomoDecodeYUV420SP(byte[] yuv420sp, int width, int height, int[] retArray);
    public native long QuomoCzDecoder(int width, int height, byte pCapturedImage[]);
    public native  int QuomoGetDecodeCodeCount();
    public native long QuomoGetDecodeCode(int index) ;
    //public native long openssltest() ;

    private Mat _m;
    private Bitmap _bitmap=null;
    private Bitmap _bitmapOverlay=null;

    public int test(){
        return(99);
    }
    public Bitmap makeBitmap(){
        _m = new Mat();
        QuomoMakeBitmap(_m.getNativeObjAddr());
        _bitmap = Bitmap.createBitmap(_m.width(), _m.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(_m, _bitmap);
        return(_bitmap);
    }
    public Bitmap makeBitmapWithImage(Bitmap partsBitmap,Bitmap frameBitmap){
        //Quomoカラー取得
        int[] colors = new int[6*6*3*100];
        int result = QuomoMakeColors(colors);
        //Log.d("sdk","result:" + colors[3] + " / " + colors[4] + " / " + colors[5]);
        //下地画像作成
        Bitmap bitmapQuomo = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapQuomo);
        float w = 500 / 6;
        float h = 500 / 6;
        for(int y=0;y<6;y++){
            for(int x=0;x<6;x++) {
                Paint paint = new Paint();
                paint.setColor(Color.rgb(colors[y*18+x*3+0],colors[y*18+x*3+1],colors[y*18+x*3+2]));

                Rect rect = new Rect(0,0,(int)w,(int)h);
                rect.offset((int)(w*x),(int)(h*y));
                canvas.drawRect(rect,paint);
            }
        }
        //
        _bitmap = blendBitmap(bitmapQuomo, partsBitmap,frameBitmap);
        //
        bitmapQuomo = null;
        //
        return(_bitmap);
    }
    public void deleteBitmap(){
        _m = null;
        _bitmap = null;
    }
    private Bitmap blendBitmap(Bitmap currentBitmap, Bitmap partsBitmap, Bitmap frameBitmap) {
        int width = currentBitmap.getWidth();
        int height = currentBitmap.getHeight();
        Bitmap new_bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(new_bitmap);
        //フレーム描画
        Rect rectSrc ,rectDst ;
        if(frameBitmap != null){
            rectSrc = new Rect(0, 0, frameBitmap.getWidth(), frameBitmap.getHeight());
            rectDst = new Rect(0, 0, width, height);
            canvas.drawBitmap(frameBitmap, rectSrc, rectDst, (Paint) null);
        }
        //カラー部分描画
        rectSrc = new Rect(0, 0, width, height);
        rectDst = new Rect((int)(width * 0.05f), (int)(height * 0.05f), (int)(width * 0.95f), (int)(height*0.95f));
        canvas.drawBitmap(currentBitmap, rectSrc, rectDst, (Paint) null);
        //オーバーレイ用のビットマップ作成
        if(_bitmapOverlay == null) {
            _bitmapOverlay = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvasWork = new Canvas(_bitmapOverlay);
            int disWidth = partsBitmap.getWidth();
            int disHeight = partsBitmap.getHeight();
            rectSrc = new Rect(0, 0, disWidth, disHeight);
            float w = width / 6;
            float h = height / 6;
            for (int y = 0; y < 6; y++) {
                for (int x = 0; x < 6; x++) {
                    rectDst = new Rect(0, 0, (int) w, (int) h);
                    rectDst.offset((int) (w * x), (int) (h * y));
                    //rectDst.offset(x,y);
                    canvasWork.drawBitmap(partsBitmap, rectSrc, rectDst, (Paint) null);
                }

            }
        }
        //オーバーレイビットマッブを合成
        rectSrc = new Rect(0, 0, width,height);
        rectDst = new Rect((int)(width * 0.05), (int)(height * 0.05f), (int)(width * 0.95f), (int)(height*0.95f));
        canvas.drawBitmap(_bitmapOverlay, rectSrc, rectDst, (Paint) null);

        return new_bitmap;
    }
}
