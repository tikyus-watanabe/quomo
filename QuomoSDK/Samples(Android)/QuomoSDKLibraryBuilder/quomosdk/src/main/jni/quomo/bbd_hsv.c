#include <android/log.h>

#include "colorcode.h"
#include "bbd_hsv.h"
#include "bbd.h"
//#include "utils.h"

int ColorZip_Color_CellClassifier_HSV(czLPDecoderArea pArea)
{
	int i;
	short color;
	int bReturnVal;
	int czret;
	czCOLOR avgWhiteRGB;  // white patch

    // nMatrixSize: col ✕ row
	for( i = 0; i < pArea->nMatrixSize; i++ ){
		pArea->m_nCell[i] = -1;
	}

	//コード領域周辺のクワイエットゾーンの白いピクセルの平均値
	ColorZip_EvalWhiteColor(&avgWhiteRGB, pArea);   // avg value of white pixel in the quiet zone around of the code area
	bReturnVal = TRUE;
	for (i=0 ;  i < pArea->nMatrixSize; i++){

	    // ここで色を検出している
		color = ColorZip_Color_Vote_HSV(pArea->pointArry[i], &avgWhiteRGB, pArea);
        __android_log_print(ANDROID_LOG_DEBUG, "ColorZip_Color_CellClassifier_HSV_No01", "i:%d, color:%d",i,color);

		if (color == -1)
		{
			bReturnVal = FALSE;
		}
		pArea->m_nCell[i] = color;
	}
	if( bReturnVal ){
		czret = ColorZip_Color_ParityChecker(pArea);
		__android_log_print(ANDROID_LOG_DEBUG, "ColorZip_Color_CellClassifier_HSV_No02", "czret:%d,pCodeInfo.CodeValue:%llu",czret,pArea->czCODE->CodeValue);
        __android_log_print(ANDROID_LOG_DEBUG, "decoder ColorZip_Color_CellClassifier_HSV aaaaa3", "czret:%d,pCodeInfo.CodeValue:%llu",czret,pArea->czCODE->CodeValue);

		if ( czret == 0 ) 
		{
			return TRUE;			// success      
		} // if
	}// if
	return FALSE; 
}

/**
 * \brief ������ ����� ��ÁE�ϴ� �Լ�E
 *
 * �ڵ忡�� ���������� CHK_INTERVAL ��ŭ ����݁E���� ����̶�E�����Ͽ� ��E���� �̾� ����� ����
 *
 * \param PavgWhiteRGB : RGB�� ���� ��ȯ �ޱ�E���� ������.
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 */
void ColorZip_EvalWhiteColor(czLPCOLOR PavgWhiteRGB,czLPDecoderArea pArea)
{
	czCOLOR pixel_pos;
	short sum_R=0, sum_G=0, sum_B=0;
	short count = 0;
	
	if (pArea->m_nCrop[0].y > CHK_INTERVAL && pArea->m_nCrop[0].x > CHK_INTERVAL) {
		pixel_pos = ColorZip_find_rgb_values(pArea->m_nCrop[0].y-CHK_INTERVAL,pArea->m_nCrop[0].x-CHK_INTERVAL,
					pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
		sum_R = pixel_pos.R;
		sum_G = pixel_pos.G;
		sum_B = pixel_pos.B;
		count++;
	}
	if (pArea->m_nCrop[1].y > CHK_INTERVAL && pArea->m_nCrop[1].x < pArea->CapturedImageWidth - CHK_INTERVAL) {
		pixel_pos = ColorZip_find_rgb_values(pArea->m_nCrop[1].y-CHK_INTERVAL,pArea->m_nCrop[1].x+CHK_INTERVAL,
					pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
		sum_R += pixel_pos.R;
		sum_G += pixel_pos.G;
		sum_B += pixel_pos.B;
		count++;
	}
	if (pArea->m_nCrop[2].y < pArea->CapturedImageHeight - CHK_INTERVAL && pArea->m_nCrop[2].x > CHK_INTERVAL) {
		pixel_pos = ColorZip_find_rgb_values(pArea->m_nCrop[2].y+CHK_INTERVAL,pArea->m_nCrop[2].x-CHK_INTERVAL,
					pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
		sum_R += pixel_pos.R;
		sum_G += pixel_pos.G;
		sum_B += pixel_pos.B;
		count++;
	}
	if (pArea->m_nCrop[3].y < pArea->CapturedImageHeight - CHK_INTERVAL && pArea->m_nCrop[3].x < pArea->CapturedImageWidth - CHK_INTERVAL) {
		pixel_pos = ColorZip_find_rgb_values(pArea->m_nCrop[3].y+CHK_INTERVAL,pArea->m_nCrop[3].x+CHK_INTERVAL,
					pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);

		sum_R += pixel_pos.R;
		sum_G += pixel_pos.G;
		sum_B += pixel_pos.B;
		count++;
	}
	if ( count > 0 ) 
	{
		PavgWhiteRGB->R = (unsigned char)(sum_R / count);
		PavgWhiteRGB->G = (unsigned char)(sum_G / count);
		PavgWhiteRGB->B = (unsigned char)(sum_B / count);
	}
	else
	{
		pixel_pos = ColorZip_find_rgb_values(0,0, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
		PavgWhiteRGB->R = (unsigned char)pixel_pos.R;;
		PavgWhiteRGB->G = (unsigned char)pixel_pos.G;
		PavgWhiteRGB->B = (unsigned char)pixel_pos.B;
		
	}
	if (PavgWhiteRGB->R==0) PavgWhiteRGB->R=1;
	if (PavgWhiteRGB->G==0) PavgWhiteRGB->G=1;
	if (PavgWhiteRGB->B==0) PavgWhiteRGB->B=1;
}

/**
 * \brief �ȼ��� ���Ǻ� �Լ�E
 *
 * ���� �ȼ���հ��� ���ϰ�E��������� ������ ���� ����� ������ ������ ���� ���� �Ǻ�
 *
 * \param point : ���Ǻ��� �ȼ���E
 * \param PavgWhiteRGB : ��������� ��հ�
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 * - 0 (COLOR_G)
 * - 1 (COLOR_K)
 * - 2 (COLOR_R)
 * - 3 (COLOR_B)
 * - 4 (COLOR_W)
 */
char ColorZip_Color_Vote_HSV(czLPCOLOR point,czLPCOLOR PavgWhiteRGB, czLPDecoderArea pArea) 
{
	short	mean_R = MINPVAL; 
	short	mean_G = MINPVAL; 
	short	mean_B = MINPVAL;	
	short	i;
	short	maxC, minC; 
	short	gap;
	short	R, G, B;
	char	color = (char)-1;
	short	rate_R, rate_G, rate_B;
	short	gapWK, Pavg;

    __android_log_print(ANDROID_LOG_DEBUG, "ColorZip_Color_Vote_HSV_No01", "pArea->nNumPoints:%d",pArea->nNumPoints);

	for ( i=0; i<pArea->nNumPoints; i++) {	
		mean_R += point[i].R;
		mean_G += point[i].G;
		mean_B += point[i].B;
	} //for
	mean_R /= pArea->nNumPoints;
	mean_G /= pArea->nNumPoints;
	mean_B /= pArea->nNumPoints;


	if (MAX3(mean_R,mean_G,mean_B) <= (pArea->ThresholdValue*2/3))	// �湁E�Ǻ� ����E2006.05.02
		return COLOR_K;		// Black

	
	Pavg = (short)(PavgWhiteRGB->R+PavgWhiteRGB->G+PavgWhiteRGB->B)/3;

	rate_R = (PavgWhiteRGB->R-Pavg)*VALID_DIGIT/PavgWhiteRGB->R;    //(Rw - meanW)/meanW
	rate_G = (PavgWhiteRGB->G-Pavg)*VALID_DIGIT/PavgWhiteRGB->G;    
	rate_B = (PavgWhiteRGB->B-Pavg)*VALID_DIGIT/PavgWhiteRGB->B;

	R = (mean_R*VALID_DIGIT - mean_R*rate_R)/VALID_DIGIT;			// correction of each channels 
	G = (mean_G*VALID_DIGIT - mean_G*rate_G)/VALID_DIGIT;			
	B = (mean_B*VALID_DIGIT - mean_B*rate_B)/VALID_DIGIT;	

	if(R<0) R=0; 
	if(G<0) G=0; 
	if(B<0) B=0;

//	if (MAX3(R,G,B) <= (pArea->ThresholdValue*2/3))					// for differenciate red and black
//		return COLOR_K;		// Black
	
	maxC = MAX3(R,G,B);
	minC = MIN3(R,G,B);
	
	gap = maxC-minC;          

	gapWK=MAX3(PavgWhiteRGB->R,PavgWhiteRGB->G,PavgWhiteRGB->B)-MAX3(mean_R, mean_G, mean_B);

	if (gap == 0) gap = 1;	// to avoid divide by 0
	
	if ((gapWK > 30) && (gap < 25)) { 
		color = COLOR_K;	
	}else {	
		int rr,gg, bb;
		int v, x, delta, hue;
		
		rr = R*VALID_DIGIT/MAXPVAL;     // rate of channels 
		gg = G*VALID_DIGIT/MAXPVAL;
		bb = B*VALID_DIGIT/MAXPVAL;
		v = MAX3(rr,gg,bb);
		x = MIN3(rr,gg,bb);
		delta = v-x;              // gap
		if (delta == 0) delta = 1;	// avoid divide by 0 

		if (v == bb) 
			hue = 4*VALID_DIGIT + (bb-rr)*VALID_DIGIT/delta;      // Blue
		else if (v == gg)
			hue = 2*VALID_DIGIT + (bb-rr)*VALID_DIGIT/delta;      // Green 
		else 
			hue = (gg-bb)*VALID_DIGIT/delta;           // red

		hue *= 60;
		if (hue < 0) hue += 360*VALID_DIGIT;
		if ((hue >= (C_BR_HSV)) || (hue <= (C_RG_HSV)))  
			color = COLOR_R;	// red
		else if ((hue > (C_RG_HSV )) && (hue <= (C_GB_HSV))) 
			color = COLOR_G;	//  green
		else if ((hue > (C_GB_HSV )) && (hue < (C_BR_HSV ))) 
			color = COLOR_B;	// blue
	} // else 
	return color;		
}

