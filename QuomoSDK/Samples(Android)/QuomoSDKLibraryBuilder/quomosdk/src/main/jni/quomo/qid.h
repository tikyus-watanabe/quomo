
#ifndef _COLORCODE_H_INCLUDED
#define _COLORCODE_H_INCLUDED


#define		DEBUG

#define MULTICODE

//Macro definitions
//

/*  image format  */
#define	RGB888_IPHONE    //  for BMP RGB888_TURN RGB888 YUV422 YUV420_NV YUV420_YV RGB565  
// #define	RGB888    // for PPM RGB888_TURN RGB888 YUV422 YUV420_NV YUV420_YV RGB565  

#define		ONESHOT	// CONTINUOUS ONESHOT  

#define		ZOOM    1 // for iPhone

/*  code type */
#define		COLOR_CZCODE		1	       		//colorcode
#define		GRAY_CZCODE		2	                // gray code
#define		NUMBER_CZCODE		0                      // number code.

#define		COLOR_QCODE		1	       		
#define		GRAY_QCODE		2	                // gray code
#define		NUMBER_QCODE		0                      // number code.

/* code size type */
#define		SIX_BY_SIX_CZCODE 70          // 6 x 6

#if defined(FIVE_BY_EIGHT_CZCODE) || defined(THREE_BY_THIRTEEN_CZCODE) || defined(SIX_BY_SIX_CZCODE)
#define BIGINT
#endif

#define		NOENVPARAM			3			// No. of environmental parameter
#define		MAXNOCELLS			60			// Max. number of cells 40 -> 60 3x15
#define		NCOLORS				4			// No. of color used in a CZ code
#define		NVOTES				4
#define		NPOINTS				100			// Max. number of pixels that can be read. 50 -> 100 3x15 

#define		MAXPVAL				0xff			// Max. point value
#define		MIDPVAL				0x7f			// Mid. point value
#define		MINPVAL				0			// Min point value

#define 		VALID_DIGIT 			10000               // no of precision for fixed point expression.

#ifndef TRUE
#define	TRUE	1
#endif
#ifndef FALSE
#define	FALSE	0
#endif

#ifndef NULL
#define NULL	0
#endif 

#define		ColorZip_GetBImagePTR(y,x)		*(unsigned char*)(pArea->czBImagePTR[(y)]+(x))
#define		ColorZip_SetBImagePTR(y,x,z)	    *(unsigned char*)(pArea->czBImagePTR[(y)]+(x))=(z)
/*
#define		ABS(x)				(((x) > 0) ? (x) : -(x))
#define		MAX(x,y)			((x) > (y) ? (x) : (y)) 
#define		MIN(x,y)			((x) < (y) ? (x) : (y)) 
*/
#define		MAX3(x,y,z)			(((x) > (y)) ?  (MAX(x,z)) : (MAX(y,z)))
#define		MIN3(x,y,z)			(((x) < (y)) ?  (MIN(x,z)) : (MIN(y,z)))

// Enumerations
//
enum MainColor {COLOR_G, COLOR_K, COLOR_R, COLOR_B, COLOR_W};
enum MainGray {GRAY_G, GRAY_, GRAY_W};
enum RelativeDirection { HIGHER, HIGHER_LEFT, LEFT, LOWER_LEFT, LOWER, LOWER_RIGHT, RIGHT, HIGHER_RIGHT, HERE};



// Type definitions
//
typedef struct tagCZCODE
{
	short				CodeType;
	int				CodeShapeType;
#ifdef BIGINT
	unsigned long long   	CodeValue;
#else
	unsigned long   	CodeValue;
#endif

}CZCODE,*lpCZCODE;

typedef struct tagQCODE
{
    short				CodeType;
    int				CodeShapeType;
#ifdef BIGINT
    unsigned long long   	CodeValue;
#else
    unsigned long   	CodeValue;
#endif
    
}QCODE,*lpQCODE;

typedef struct typeCOLOR {
	unsigned char B;
	unsigned char G;
	unsigned char R;
} czCOLOR,*czLPCOLOR;

typedef struct{
	short	left;
	short	right;
	short	top;
	short	bottom;
} Sides;

// Information returned from VertexEdgeTracer()
typedef struct{
	Sides sides;
	short brightness;
	short size;
	short line_size;
} VertexInfo;

typedef struct{
	int index;
	short H;
	short S;
	short V;
	unsigned char R;
	unsigned char G;
	unsigned char B;
	unsigned char diff;
	unsigned char min;
	unsigned char max;
} STATS;

typedef struct{
	int index;
	short H;
	short nJump;
} JUMPS;


typedef struct  {
	short	x;
	short	y;
} czPOSITION;

typedef struct attribCells{
	enum MainColor group;                   // color of the cell - G:0, K:1, R:2, B:3
	short R,G,B;
	short max_PC;                  // Max color channel of the cell
	short min_PC;
	short mid_PC;
	short Gap_PMin;                // maxPC-minPC
	short Gap_PMid;                // maxPC-midPC
}attribCells;

typedef struct GroupChannel{
	short numGC;                   // number of cells in each group 
	short MaxGC;
	short MinGC;
	int AvrGC, AvrGC_R,AvrGC_G,AvrGC_B; 
	long TOTGC;  
		
	short Sort  [MAXNOCELLS];      // sorted value in the group   (ascending)  
	short CellID[MAXNOCELLS];      // Cell ID corresponding to sort[]    
	short value_interval;
}GroupChannel;                  


typedef struct _czDecoderArea
{
	unsigned char			nProcess;

	short				nParityType;					// parity types
	short				nNumPoints;					// number of pixels that were read from raw clorcode image.

	short				envParam[NOENVPARAM]; 		// 

	short				nCol;						// no. of columns in a colorcode
	short				nRow;						// no. of rows in a colorcode
	short				nMatrixSize;					// nCol * nRow
	short				m_nCell[ MAXNOCELLS ];		// color array
	czCOLOR				pointArry[MAXNOCELLS][NPOINTS];	//

	czPOSITION			m_nCellpos[ MAXNOCELLS ];		// cneter postions of cells 
	czPOSITION			m_nCrop[NVOTES];				// top, bottom, left, right positions
	attribCells				m_Cell_Class[MAXNOCELLS];
	GroupChannel			m_Group[NCOLORS];                     //G:0, K:1, R:2, B:3 , for color classifications.

	short				nLeft, nRight, nTop, nBottom;		// code area positions  
	unsigned char			ThresholdValue;  				// RGB value for dividing Black and White color.
	unsigned char			*pCapturedImageBuf;
	unsigned char			*czBImage;		//pointer of  Black & White  image  buffer
	unsigned char			**czBImagePTR;		//pointer of  Black & White  image  buffer
	
	lpCZCODE			czCODE;

	short				CapturedImageWidth;
	short				CapturedImageHeight;
	short				BinaryImageWidth;
	short				BinaryImageHeight;
#ifdef MULTICODE
  short firstScan;
#endif
} czDecoderArea,*czLPDecoderArea;

typedef struct _qDecoderArea
{
    unsigned char			nProcess;
    
    short				nParityType;					// parity types
    short				nNumPoints;					// number of pixels that were read from raw clorcode image.
    
    short				envParam[NOENVPARAM]; 		//
    
    short				nCol;						// no. of columns in a colorcode
    short				nRow;						// no. of rows in a colorcode
    short				nMatrixSize;					// nCol * nRow
    short				m_nCell[ MAXNOCELLS ];		// color array
    czCOLOR				pointArry[MAXNOCELLS][NPOINTS];	//
    
    czPOSITION			m_nCellpos[ MAXNOCELLS ];		// cneter postions of cells
    czPOSITION			m_nCrop[NVOTES];				// top, bottom, left, right positions
    attribCells				m_Cell_Class[MAXNOCELLS];
    GroupChannel			m_Group[NCOLORS];                     //G:0, K:1, R:2, B:3 , for color classifications.
    
    short				nLeft, nRight, nTop, nBottom;		// code area positions
    unsigned char			ThresholdValue;  				// RGB value for dividing Black and White color.
    unsigned char			*pCapturedImageBuf;
    unsigned char			*czBImage;		//pointer of  Black & White  image  buffer
    unsigned char			**czBImagePTR;		//pointer of  Black & White  image  buffer
    
    lpCZCODE			czCODE;
    
    short				CapturedImageWidth;
    short				CapturedImageHeight;
    short				BinaryImageWidth;
    short				BinaryImageHeight;
#ifdef MULTICODE
    short firstScan;
#endif
} qDecoderArea,*qLPDecoderArea;


#endif  /* _COLORCODE_H_INCLUDED */
