/**
 * \brief  header file for bbd_hsv.c
 *
 *
 *
 */
 
#ifndef _BBD_HSV_H_INCLUDED
#define _BBD_HSV_H_INCLUDED



#define		C_RG_HSV			20*VALID_DIGIT			// 20
#define		C_GB_HSV			170*VALID_DIGIT			// 170
#define		C_BR_HSV			320*VALID_DIGIT			// 320

#define		CHK_INTERVAL		5			// boundary check interval to evaluate white color 


// Function declarations
//
extern	int 	ColorZip_Color_CellClassifier_HSV(czLPDecoderArea pArea);
extern	char ColorZip_Color_Vote_HSV(czLPCOLOR point,czLPCOLOR PavgWhiteRGB, czLPDecoderArea pArea); 
extern	void 	ColorZip_EvalWhiteColor(czLPCOLOR PavgWhiteRGB,czLPDecoderArea pArea);


#endif /*  _BBD_HSV_H_INCLUDED */
