/*
 quomoEncode-v1.2.2.c

Copyright 2014 Tikyu Solutions
A.I. 2015-10-23 Make iOS Framework and some functions for Quomo code
A.I. 2014-10-17 Add Flex Col&Row
A.I. 2014-07-17 Add Parity Support
A.I. 2014-05-04

*/
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <android/log.h>
#if CCL
#include        <openssl/evp.h>
#include        <openssl/aes.h>
#endif

#define TYPE_ERR 1
#define TYPE_OK 0
#define PW_ERR 2
#define SZ_ERR 3

int ok = 1;

int makeCC_internal(unsigned long long , int , int[] );
int makeCC_internal2(unsigned long long , int , int, int[] );
int  ccdEncode(unsigned long long , int , char* );
int  ccdEncode2(unsigned long long , int , int, char* );
int  ccdEncodenoSP(unsigned long long , int , char* );
int  ccdEncodenoSP2(unsigned long long , int , int, char* );
//int makeCCpw(unsigned long);
int type2ColRow(int , int *, int *);
int  ccdEncodePw(char *);
int type2ColRowMaskShift(int , int *, int *, unsigned long long *, int *);
int type2ColRowMaskShift2(int , int , int *, int *, unsigned long long *, int *, unsigned long long *);
int  quomoEncode(unsigned long long , char* );

int type2ColRow(int type, int *m_Columns, int *m_Rows)
{
        if( type == 1) {

        *m_Columns = 5;           // # of columns
        *m_Rows = 5;              // # of rowsn
} else if( type == 2) {

        *m_Columns = 8;           // # of columns
        *m_Rows = 5;              // # of rowsn
} else if( type == 3) {

        *m_Columns = 6;           // # of columns
        *m_Rows = 6;              // # of rowsn
} else
        return 1; // Type Error
return TYPE_OK;
}

int type2ColRowMaskShift(int type, int *m_Columns, int *m_Rows, unsigned long long *m_Mask, int *m_Shift)
{
        if( type == 1) {

        *m_Columns = 5;           // # of columns
        *m_Rows = 5;              // # of rowsn
	*m_Mask = 0x300000000LL; 
        *m_Shift = 32;
} else if( type == 2) {

        *m_Columns = 8;           // # of columns
        *m_Rows = 5;              // # of rowsn
	*m_Mask = 0x300000000000000LL; 
	*m_Shift = 56;
} else if( type == 3) {

        *m_Columns = 6;           // # of columns
        *m_Rows = 6;              // # of rowsn
        *m_Mask = 0xC000000000000LL;
	*m_Shift = 50;
} else
        return 1; // Type Error
return TYPE_OK;
}

int type2ColRowMaskShift2(int col, int row, int *m_Columns, int *m_Rows, unsigned long long *m_Mask, int *m_Shift, unsigned long long *m_Mask2)
{
int j;
	if(col >= 9 || col <= 2) return SZ_ERR;
	if(row >= 9 || row <=2) return SZ_ERR;
	if(col*row > 40 || col*row < 9) return SZ_ERR;

    *m_Columns = col;           // # of columns
    *m_Rows = row;              // # of rowsn
    *m_Shift = (col-1)*(row-1)*2;
    *m_Mask = 0x3LL << *m_Shift;
	*m_Mask2 = 1LL; //modified saito 1 -> 1LL
	for(j=1;j<= *m_Shift; j++) {
		*m_Mask2 += 1LL<<j; //modified saito 1 -> 1LL
	}
	return TYPE_OK;
}

int  ccdEncodePw(char *text)
{
#if CCL
EVP_CIPHER_CTX  de;
char    *key,*data,*ptr;
char    buf[80];
int     datasize,c;
int     p_len,f_len=0;
char    *plaintext;
char    year[5], month[3], day[3];
int	nyear,nmonth, nday;

    time_t timer;
    struct tm *local;

#ifdef DEBUG
  /* 構造体形式のローカル(日本)時間に変換 */
  struct tm * strtim;
  time_t rtime;
rtime = time(NULL);
  strtim = localtime( &rtime );
  /* 内容を確認 */
  printf( "tm_sec=%d\n" , strtim->tm_sec );
  printf( "tm_min=%d\n" , strtim->tm_min );
  printf( "tm_hour=%d\n" , strtim->tm_hour );
  printf( "tm_mday=%d\n" , strtim->tm_mday );
  printf( "tm_mon=%d\n" , strtim->tm_mon );
  printf( "tm_year=%d\n" , strtim->tm_year + 1900 );
  printf( "tm_wday=%d\n" , strtim->tm_wday );
  printf( "tm_yday=%d\n" , strtim->tm_yday );
  printf( "tm_isdst=%d\n\n" , strtim->tm_isdst );
#endif

        key="1234567890123456";
        if(strlen(key)!=EVP_CIPHER_key_length(EVP_aes_128_ecb())){
                fprintf(stderr,"key length must be %d\n",EVP_CIPHER_key_length(EVP_aes_128_ecb()));
                return(PW_ERR);
        }

        data=malloc(strlen(text));
        for(ptr=text,c=0;*ptr!='\0';ptr+=2,c++){
                buf[0]=*ptr;
                buf[1]=*(ptr+1);
                buf[2]='\0';
                data[c]=strtol(buf,NULL,16);
        }
        datasize=c;

        EVP_CIPHER_CTX_init(&de);
        EVP_DecryptInit_ex(&de,EVP_aes_128_ecb(),NULL,(unsigned char *)key,NULL);

        p_len=datasize;
        plaintext=calloc(p_len+1,sizeof(char));

        EVP_DecryptUpdate(&de,(unsigned char *)plaintext,&p_len,(unsigned char *)data,datasize);
        EVP_DecryptFinal_ex(&de,(unsigned char *)(plaintext+p_len),&f_len);

        plaintext[p_len+f_len]='\0';
#ifdef DEBUG
        printf("[%s]\n",plaintext);
#endif

        EVP_CIPHER_CTX_cleanup(&de);


        free(data);

if(strlen(plaintext) != 10) { // YYY-MM-DD = 10 chars
        free(plaintext);
        ok = 0;
        return (PW_ERR);
}

timer = time(NULL);
local = localtime(&timer);
// YYYY-MM-DD
strncpy(year, plaintext,4);
year[4]='\0';
strncpy(month, plaintext+5,2);
month[2]='\0';
strncpy(day, plaintext+8,2);
day[2]='\0';

nyear = atoi(year);nmonth = atoi(month);nday=atoi(day);
	ok = 0;
if((local->tm_year + 1900) < nyear)
	ok = 1;
else if((local->tm_year + 1900) == nyear && ((local->tm_mon + 1) < nmonth))
	ok = 1;
else if(((local->tm_year + 1900) == nyear) && ((local->tm_mon + 1) == nmonth) && (local->tm_mday  <= nday))
        ok = 1;

	free(plaintext);
	if(ok == 0)
		return (PW_ERR);

        return(TYPE_OK);
#else
	ok = 1;
        return(TYPE_OK);
#endif
}

int makeCC_internal2(unsigned long long m_Code, int col, int row, int m_Cells[])
{
#define PARITY_MASK 3
#define TYPE_ERR 1
#define TYPE_OK 0
#define G 0
#define K 1
#define R 2
#define B 3
#define W 4

//      unsigned long long  m_Code;          // code number
//      int m_Cells[40];            // memory of cells
        int m_Columns;           // # of columns
        int m_Rows;              // # of rowsn
        unsigned long long m_Mask;        // Parity bits
        unsigned long long m_Mask2;        // Parity bits
        int m_Shift;
        //int m_ParityType;        // parity
        unsigned long long m_ParityType;        // parity
        unsigned long long int64_code;
        int i,j,index,ret;

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No00", "開始");

//      m_Rows = atoi((LPCTSTR)rows);
//      m_Columns = atoi((LPCTSTR)columns);
//      m_Code = _atoi64(LPCTSTR(code));
        if( ok != 1)
                return PW_ERR; // PW error
//ret=type2ColRow(type,&m_Columns, &m_Rows);
ret=type2ColRowMaskShift2(col, row,&m_Columns, &m_Rows,&m_Mask,&m_Shift, &m_Mask2);

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No01", "col:%d,row:%d,m_Rows:%d,m_Mask:%llu,m_Shift:%d,m_Mask2:%llu",  col,row,m_Rows,m_Mask,m_Shift,m_Mask2);

if(ret != 0)
        return ret;

m_ParityType = (m_Code & m_Mask)>>m_Shift; // even
//m_ParityType = (m_Code )>>m_Shift; // even
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No02", "m_Code:%llu,m_Mask:%llu,m_Shift:%d,m_ParityType:%llu",  m_Code,m_Mask,m_Shift,m_ParityType);



int64_code = m_Code & m_Mask2;
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No03", "m_Code:%llu,m_Mask2:%llu,int64_code:%llu",  m_Code,m_Mask2,int64_code);

//int64_code = m_Code;
                for (i=m_Rows-2; i >= 0; i--) // row parity calculation
                {
                        int row_parity = 0;
                        for (j=m_Columns-2; j >= 0; j--)
                        {
                                index = (int)(int64_code % 4);
                                int64_code >>= 2;

                                row_parity ^= m_Cells[i*m_Columns + j] = index;
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No04", "index:%d,int64_code:%llu,row_parity:%d",  m_Code,int64_code,row_parity);
                        }

                        //row parity mapping
                        if(m_ParityType == 0) m_Cells[i*m_Columns + m_Columns-1] = row_parity;                  // even
                        else if(m_ParityType == 1) m_Cells[i*m_Columns + m_Columns-1] = abs(row_parity-3);      // odd
                        else if(m_ParityType == 2) m_Cells[i*m_Columns + m_Columns-1] = (row_parity+2)%4;       // even+2
                        else if(m_ParityType == 3) m_Cells[i*m_Columns + m_Columns-1] = (abs(row_parity-3)+2)%4; // odd+2

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No05", "m_ParityType:%llu,index:%d,m_Cells[i*m_Columns + m_Columns-1]:%d",  m_ParityType,i*m_Columns + m_Columns-1,m_Cells[i*m_Columns + m_Columns-1]);

                }

                int rotation_parity = 0;                // rotation check parity calculation

                for (j=0; j < m_Columns-1; j++) // column parity calculation
                {
                        int column_parity = 0;
                        for (i=0; i < m_Rows-1; i++) {
                                column_parity ^= m_Cells[i*m_Columns + j];
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No06", "index:%d,m_Cells[i*m_Columns + j]:%d,column_parity:%d",  i*m_Columns + j,m_Cells[i*m_Columns + j],column_parity);
                        }

                        if(m_ParityType == 0) {}//column_parity = column_parity; // even
                        else if(m_ParityType == 1) column_parity = abs(column_parity-3);   // odd
                        else if(m_ParityType == 2) column_parity = (column_parity+2)%4;       // even+2
                        else if(m_ParityType == 3) column_parity = (abs(column_parity-3)+2)%4; // odd+2

                        rotation_parity ^= m_Cells[(m_Rows-1)*m_Columns + j] = column_parity;

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No07", "index:%d,m_Cells[(m_Rows-1)*m_Columns + j]:%d,rotation_parity:%d", (m_Rows-1)*m_Columns + j, m_Cells[(m_Rows-1)*m_Columns + j],rotation_parity);

                }


                // rotatio tag by prity type  :  ~ (data^parity_type)
                if(m_ParityType == 0) m_Cells[m_Rows*m_Columns -1] = rotation_parity ^ PARITY_MASK;     // ~(data^even) : (data ^ 0) ^ 11
                else if(m_ParityType == 1) m_Cells[m_Rows*m_Columns -1] = rotation_parity;              // ~(data^odd)  : (data ^ 11) ^ 11
                else if(m_ParityType == 2) m_Cells[m_Rows*m_Columns -1] = ((rotation_parity+2)%4) ^ PARITY_MASK;     // ~(data^(even+2)) : ((data+2)%4) ^ 11
                else if(m_ParityType == 3) m_Cells[m_Rows*m_Columns -1] = ((abs(rotation_parity-3)+2)%4) ^ PARITY_MASK;   // ~(data^(odd+2)) : ((abs(data- 3)+2)%4) ^ 11

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No08", "m_ParityType:%llu,index:%d,m_Cells[i*m_Columns + m_Columns-1]:%d",  m_ParityType,m_Rows*m_Columns -1,m_Cells[m_Rows*m_Columns -1]);

// Anti ColorZip
// m_Cells[0] = V00[m_Cells[0]];
// m_Cells[1] = V01[m_Cells[2]];
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No99", "終了");

        return TYPE_OK;
}




int makeCC_internal(unsigned long long m_Code, int type, int m_Cells[])
{
#define PARITY_MASK 3
#define TYPE_ERR 1
#define TYPE_OK 0
#define G 0
#define K 1
#define R 2
#define B 3
#define W 4

//	unsigned long long  m_Code;          // code number
//	int m_Cells[40];            // memory of cells 
	int m_Columns;           // # of columns   
	int m_Rows;              // # of rowsn   
	unsigned long long m_Mask;        // Parity bits
	int m_Shift;
	int m_ParityType;        // parity
	unsigned long long int64_code;
	int i,j,index,ret;

//	m_Rows = atoi((LPCTSTR)rows);			
//	m_Columns = atoi((LPCTSTR)columns);
//	m_Code = _atoi64(LPCTSTR(code));
	if( ok != 1)
		return PW_ERR; // PW error
//ret=type2ColRow(type,&m_Columns, &m_Rows);
ret=type2ColRowMaskShift(type,&m_Columns, &m_Rows,&m_Mask,&m_Shift);
if(ret != 0)
        return ret;

m_ParityType = (m_Code & m_Mask)>>m_Shift; // even


int64_code = m_Code;
		for (i=m_Rows-2; i >= 0; i--) // row parity calculation
		{
			int row_parity = 0;
			for (j=m_Columns-2; j >= 0; j--)
			{
				index = (int)(int64_code % 4);
				int64_code >>= 2;
				
				row_parity ^= m_Cells[i*m_Columns + j] = index;
			}	
			
			//row parity mapping 
			if(m_ParityType == 0) m_Cells[i*m_Columns + m_Columns-1] = row_parity;                  // even
			else if(m_ParityType == 1) m_Cells[i*m_Columns + m_Columns-1] = abs(row_parity-3);      // odd 
			else if(m_ParityType == 2) m_Cells[i*m_Columns + m_Columns-1] = (row_parity+2)%4;       // even+2
			else if(m_ParityType == 3) m_Cells[i*m_Columns + m_Columns-1] = (abs(row_parity-3)+2)%4; // odd+2
		}
		
		int rotation_parity = 0;		// rotation check parity calculation

		for (j=0; j < m_Columns-1; j++) // column parity calculation
		{
			int column_parity = 0;
			for (i=0; i < m_Rows-1; i++) {
				column_parity ^= m_Cells[i*m_Columns + j];
			}
			
			if(m_ParityType == 0) {}//column_parity = column_parity; // even
			else if(m_ParityType == 1) column_parity = abs(column_parity-3);   // odd 
			else if(m_ParityType == 2) column_parity = (column_parity+2)%4;       // even+2
			else if(m_ParityType == 3) column_parity = (abs(column_parity-3)+2)%4; // odd+2
					
			rotation_parity ^= m_Cells[(m_Rows-1)*m_Columns + j] = column_parity;
		}	
	
		// rotatio tag by prity type  :  ~ (data^parity_type)
		if(m_ParityType == 0) m_Cells[m_Rows*m_Columns -1] = rotation_parity ^ PARITY_MASK;     // ~(data^even) : (data ^ 0) ^ 11 
		else if(m_ParityType == 1) m_Cells[m_Rows*m_Columns -1] = rotation_parity;              // ~(data^odd)  : (data ^ 11) ^ 11
		else if(m_ParityType == 2) m_Cells[m_Rows*m_Columns -1] = ((rotation_parity+2)%4) ^ PARITY_MASK;     // ~(data^(even+2)) : ((data+2)%4) ^ 11 
		else if(m_ParityType == 3) m_Cells[m_Rows*m_Columns -1] = ((abs(rotation_parity-3)+2)%4) ^ PARITY_MASK;   // ~(data^(odd+2)) : ((abs(data- 3)+2)%4) ^ 11
// Anti ColorZip 
// m_Cells[0] = V00[m_Cells[0]];
// m_Cells[1] = V01[m_Cells[2]];
	return TYPE_OK;		
}

int  ccdEncodenoSP(unsigned long long m_Code, int type, char s_Cells[] )
{
int ret;
int m_Cells[40];
int i;
        int m_Columns;           // # of columns
        int m_Rows;              // # of rowsn
	unsigned long long m_Mask; // Mask
	int m_Shift;
//ret=type2ColRow(type,&m_Columns, &m_Rows);
ret=type2ColRowMaskShift(type,&m_Columns, &m_Rows, &m_Mask, &m_Shift);
if(ret != TYPE_OK)
	return ret;

ret=makeCC_internal(m_Code, type, m_Cells);
if(ret != TYPE_OK)
        return ret;
        for(i=0; i<m_Rows*m_Columns;i++) {
                if(m_Cells[i] == G)
                        s_Cells[i] = 'G';
                else if(m_Cells[i] == K)
                        s_Cells[i] = 'K';
                else if(m_Cells[i] == R)
                        s_Cells[i] = 'R';
                else if(m_Cells[i] == B)
                        s_Cells[i] = 'B';
                else
                        s_Cells[i] = 'W';
        }
s_Cells[i+1] = 0;

return TYPE_OK;
}
int  quomoEncode(unsigned long long m_Code, char s_Cells[])
{
	return ccdEncodenoSP2(m_Code, 6, 6, s_Cells); 
}

int  ccdEncode(unsigned long long m_Code, int type, char s_Cells[])
{
int ret;
int m_Cells[40];
int i,j,k,l;
        int m_Columns;           // # of columns
        int m_Rows;              // # of rowsn
	unsigned long long m_Mask;
	int m_Shift;

ret=type2ColRowMaskShift(type,&m_Columns, &m_Rows, &m_Mask,&m_Shift);
if(ret != TYPE_OK)
        return ret;

ret=makeCC_internal(m_Code, type, m_Cells);
if(ret != TYPE_OK)
        return ret;

	k = 0; l=0;
        for(i=0; i<m_Rows;i++) {
	   for(j=0; j<m_Columns;j++) {
                if(m_Cells[k] == G)
                        s_Cells[l] = 'G';
                else if(m_Cells[k] == K)
                        s_Cells[l] = 'K';
                else if(m_Cells[k] == R)
                        s_Cells[l] = 'R';
                else if(m_Cells[k] == B)
                        s_Cells[l] = 'B';
                else
                        s_Cells[l] = 'W';
		k++;l++;
            }
        s_Cells[l] = ' ';l++;
        }
 s_Cells[l] = 0;
return TYPE_OK;
}

int  ccdEncodenoSP2(unsigned long long m_Code, int col, int row, char s_Cells[] )
{
int ret;
int m_Cells[40];
int i;
        int m_Columns;           // # of columns
        int m_Rows;              // # of rowsn
        unsigned long long m_Mask; // Mask
        unsigned long long m_Mask2; // Mask
        int m_Shift;
//ret=type2ColRow(type,&m_Columns, &m_Rows);
ret=type2ColRowMaskShift2(col,row,&m_Columns, &m_Rows, &m_Mask, &m_Shift, &m_Mask2);
if(ret != TYPE_OK)
        return ret;

ret=makeCC_internal2(m_Code, col, row, m_Cells);

if(ret != TYPE_OK)
        return ret;
        for(i=0; i<m_Rows*m_Columns;i++) {
                if(m_Cells[i] == G)
                        s_Cells[i] = 'G';
                else if(m_Cells[i] == K)
                        s_Cells[i] = 'K';
                else if(m_Cells[i] == R)
                        s_Cells[i] = 'R';
                else if(m_Cells[i] == B)
                        s_Cells[i] = 'B';
                else
                        s_Cells[i] = 'W';
        }
s_Cells[i+1] = 0;

for(i=0; i<m_Rows*m_Columns;i++) {
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No09", "index:%d, s_Cells[i]:%c",  i,s_Cells[i]);
}



return TYPE_OK;
}


int  ccdEncode2(unsigned long long m_Code, int col, int row, char s_Cells[])
{
int ret;
int m_Cells[40];
int i,j,k,l;
        int m_Columns;           // # of columns
        int m_Rows;              // # of rowsn
        unsigned long long m_Mask,m_Mask2;
        int m_Shift;

ret=type2ColRowMaskShift2(col,row,&m_Columns, &m_Rows, &m_Mask, &m_Shift, &m_Mask2);
if(ret != TYPE_OK)
        return ret;


ret=makeCC_internal2(m_Code, col, row, m_Cells);

if(ret != TYPE_OK)
        return ret;

	m_Rows = row;
	m_Columns = col;

        k = 0; l=0;
        for(i=0; i<m_Rows;i++) {
           for(j=0; j<m_Columns;j++) {
                if(m_Cells[k] == G)
                        s_Cells[l] = 'G';
                else if(m_Cells[k] == K)
                        s_Cells[l] = 'K';
                else if(m_Cells[k] == R)
                        s_Cells[l] = 'R';
                else if(m_Cells[k] == B)
                        s_Cells[l] = 'B';
                else
                        s_Cells[l] = 'W';
                k++;l++;
            }
        s_Cells[l] = ' ';l++;
        }
 s_Cells[l] = 0;
return TYPE_OK;
}


#ifdef DEBUG

int main( int argc, char *argv[] ){
       char *p;
unsigned long long m_Code;
int col,row;
char m_Cells[255];
int ret;
int i,j,k;

ret = ccdEncodePw("fd69500a6d80fca8ef31a46b719fa4ff"); // 2014-12-31
printf("PW Result = %d\n", ret);
if(ret != 0)
	exit( ret);
#ifdef DEBUG5x8
//m_Code = 4039238143500010LL;
m_Code = 288230376151711743LL; // Parity = 3
col=8; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
	exit(ret);

  printf("%s\n",m_Cells);

m_Code = 216172782113783807LL; // Parity=2
col=8; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);

m_Code = 144115188075855871LL; // Parity = 1
col=8; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);

m_Code = 72057594037927935LL; // Parity = 1
col=8; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);
#endif // DEBUG5x8

#ifdef DEBUG5x5
//m_Code = 4039238143500010LL;
m_Code = 17179869183LL; // Parity = 3
col=5; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);

  printf("%s\n",m_Cells);

m_Code = 12884901887LL; // Parity=2
col=5; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);

m_Code = 8589934591LL; // Parity = 1
col=5; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);

m_Code = 4294967295LL; // Parity = 0
col=5; row=5; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);
#endif // DEBUG5x8

#ifdef DEBUG6x6
//m_Code = 4039238143500010LL;
m_Code = 4503599627370495LL; // Parity = 3
col=6; row=6; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);

  printf("%s\n",m_Cells);

m_Code = 3377699720527871LL; // Parity=2
col=6; row=6; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);

m_Code = 2251799813685247LL; // Parity = 1
col=6; row=6; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);

m_Code = 1125899906842623LL; // Parity = 0
col=6; row=6; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("6x6 Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);
#endif // DEBUG5x8

#ifdef QUOMO
m_Code = 1125899906842623LL; // Parity = 0
col=6; row=6; // 5x8
//ret =  ccdEncodenoSP(m_Code, type, m_Cells);
ret =  ccdEncode2(m_Code, col, row, m_Cells);
printf("Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);

m_Code = 1125899906842623LL; // Parity = 0
ret =  quomoEncode(m_Code, m_Cells);
printf("QUOMO Result = %d\n", ret);
if(ret != 0)
        exit(ret);
  printf("%s\n",m_Cells);
#endif



}

#endif
