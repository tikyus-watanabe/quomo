/**
 * \brief  header file for bbd_clustering.c
 *
 *
 *
 */
 
#ifndef _BBD_CLUSTERING_H_INCLUDED
#define _BBD_CLUSTERING_H_INCLUDED


// Macro Definitions
//


// Function declarations
//
extern 	int 		ColorZip_Color_CellClassifier_CLUST(czLPDecoderArea pArea);
extern	void 		ColorZip_sortV( STATS arr[], int beg, int end );
extern	void 		ColorZip_sortH( STATS arr[], int beg, int end );
extern	void 		ColorZip_sortJumps( JUMPS arr[], int beg, int end );
extern	void 		ColorZip_swapStats( STATS *a, STATS *b );
extern	void 		ColorZip_swapJumps( JUMPS *a, JUMPS *b );

//Type definitions
//


#endif /*  _BBD_CLUSTERING_H_INCLUDED */
