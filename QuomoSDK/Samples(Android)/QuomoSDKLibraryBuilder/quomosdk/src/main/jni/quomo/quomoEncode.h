/*
 * Copyright 2014 by Tikyu Solutions
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of ColorCode Labs, Inc..  ("Confidential Information").  You shall
 * not disclose such Confidential Information and shall use it
 * only in accordance with the terms of the license agreement you
 * entered into with Tikyu Solutions Inc.
 */
#ifdef __cplusplus
extern "C" {
#endif

int  quomoEncode(unsigned long long , char* );

#ifdef __cplusplus
}
#endif
