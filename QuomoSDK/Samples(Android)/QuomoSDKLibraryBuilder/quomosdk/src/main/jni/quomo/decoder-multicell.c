/**
 * \brief decoder.c : colorcode decoding modules
 *
 *
 * 
 */
#include <android/log.h>

#include "colorcode.h"
#include "decoder.h"
#include "bbd.h"
#include "binarization.h"
#include "codearea_detect.h"
#include "cell_detect.h"

//#include "utils.h"

unsigned char	nProcessCase;
#ifdef 	DEBUG
	int codeareafind;
#endif

#ifdef MULTICODE
void	czClearArea(czLPDecoderArea pArea)
{
	int x,y;
	czCOLOR Wpoint;
	
	Wpoint.R = 255;Wpoint.G = 255; Wpoint.B=255;
	
	for(y = pArea->nTop; y <= pArea->nBottom; y++)
		for(x = pArea->nLeft; x <= pArea->nRight;x++) {
			ColorZip_set_rgb_values(y,x,pArea->pCapturedImageBuf,pArea->CapturedImageHeight,pArea->CapturedImageWidth, Wpoint);
		}
	for(y = pArea->nTop/ZOOM; y <= pArea->nBottom/ZOOM; y++)
		for(x = pArea->nLeft/ZOOM; x <= pArea->nRight/ZOOM;x++) {
			ColorZip_SetBImagePTR(y,x, MAXPVAL);
		}
}
#endif

short       temptemp(short codeType, czLPDecoderArea pArea)
{
	unsigned char	Codefound;	
	unsigned char	czret;	
	unsigned char nColor[5];
	czPOSITION  non_Edge_Vertices[4];
	czPOSITION  Edge_Vertices[4];
	int nonEdgeBetter, i;
#ifdef 	DEBUG
	codeareafind=0;
#endif
	Codefound = 0; // CZ AI
	nColor[0]=0;
	nColor[1]=0;
	nColor[2]=0;
	nColor[3]=0;
	nColor[4]=0;

		ColorZip_GetRoughBoundaryRect(pArea);
		ColorZip_CodeAreaDetector(pArea);          // Code Area Detection: sets crop
		ColorZip_CalculateSides(pArea);	// set sides based on min/max of crop
		ColorZip_VerticeDetector(MINPVAL, non_Edge_Vertices, (unsigned char)codeType, pArea);
 		ColorZip_VerticeDetector(ColorZip_CodeImageFinder(pArea), Edge_Vertices, (unsigned char)codeType, pArea);
		nonEdgeBetter = ColorZip_VerticesSelector(non_Edge_Vertices, Edge_Vertices);               // Select adaptive vertices 
		if (nonEdgeBetter) {
			for (i=0; i<4; i++) pArea->m_nCrop[i] = non_Edge_Vertices[i];
		}else{    
			for (i=0; i<4; i++) pArea->m_nCrop[i] = Edge_Vertices[i];
		}
		ColorZip_CalculateSides(pArea);
		czret = ColorZip_CellDetector(pArea);
		if ( czret ==0 )
		{
#ifdef 	DEBUG
			codeareafind=1;
#endif
			if ( codeType == COLOR_CZCODE ){
				Codefound = ColorZip_Color_Decoder_BBD(nProcessCase, pArea);
			}
		}
		czret = 0;
		if (Codefound) 
		{
			if (pArea->nParityType == 1)      
				pArea->nParityType = 3;		// check parity type
			else if (pArea->nParityType == 3) 
				pArea->nParityType = 1;			  
			czret = nProcessCase;
			return czret;
		}
		else
		{
			czret=FALSE;
			return czret;
		}
} 

// short	    ColorZip_decode( short codeType, czLPDecoderArea pArea);

/**
 * \brief �������� colorcode ���ڵ�E��ǁE�Լ�E
 *
 * \return
 * - ����E: 1 ~ MAX_PROCESSCASE(���ڵ�E��ǁEȽ��E
 * - ���� : 0
 */
short  ColorZip_decode( short codeType, czLPDecoderArea pArea )
{
//	M_Int64   gTime1;
//	M_Int64   gTime2;


	unsigned char	Codefound;	
	//	unsigned char	nProcessCase;
	unsigned char	czret;	
	unsigned char nColor[5];
//	czPOSITION  non_Edge_Vertices[4];
//	czPOSITION  Edge_Vertices[4];
//	int nonEdgeBetter;
//	int x;

#ifdef 	DEBUG
	codeareafind=0;
#endif

	
	nColor[0]=0;
	nColor[1]=0;
	nColor[2]=0;
	nColor[3]=0;
	nColor[4]=0;

#ifdef 	ONESHOT
	pArea->nProcess	= 0; // Process 1,2,3 �ܰ�E��� ó�� �ϱ�E����.
	nProcessCase = 1;    // iteration for various processing method
__android_log_print(ANDROID_LOG_DEBUG, "decoder_czDecoder_ColorZip_decode", "ONESHOT:TRUE");
#endif

#ifdef  CONTINUOUS	
//ここは通っていない
	nProcessCase = pArea->nProcess;    // pArea->nProcess�� 1, 2, 3, ���� ���� �Ǹ�E��ȯ
#endif

       	do {

		Codefound = FALSE;
#ifdef MULTICODE
		if(pArea->firstScan) {
__android_log_print(ANDROID_LOG_DEBUG, "decoder_czDecoder_ColorZip_decode", "MULTICODE:TRUE");

		  ColorZip_BinaryFilter(nProcessCase,pArea); // removing background image
		    pArea->firstScan = FALSE;
      	}
		else 
      		  czClearArea(pArea);

#else
//ここは通っていない
__android_log_print(ANDROID_LOG_DEBUG, "decoder_czDecoder_ColorZip_decode", "MULTICODE:FALSE");
		  ColorZip_BinaryFilter(nProcessCase,pArea); // removing background image
#endif

		// Next try 6x6
		pArea->nCol = 6; pArea->nRow = 6;
		czret = (unsigned char)temptemp(codeType, pArea);
		if(czret > 0)
		  return czret;

		
		pArea->firstScan = TRUE;
		nProcessCase++;	
		} while ((nProcessCase < MAX_PROCESSCASE) && (pArea->nProcess == 0) );

#ifdef 	CONTINUOUS    //  Process �ܰ�E������ ���ڵ�E���� �ÿ��� ����
	pArea->nProcess++;
	if (pArea->nProcess >= MAX_PROCESSCASE){   // Process �ܰ�E1, 2, 3
		pArea->nProcess	= 1;
	}
#endif
	return 0;
	//	return czret;
}	


