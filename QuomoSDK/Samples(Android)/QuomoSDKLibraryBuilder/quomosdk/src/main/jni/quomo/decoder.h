#ifndef _DECODER_H_INCLUDED
#define _DECODER_H_INCLUDED

// Macro Definitions
// 
#define		MAX_PROCESSCASE		4		    	// Detect Case


// Function declarations
//
extern 	short	    ColorZip_decode( short codeType, czLPDecoderArea pArea);
#endif   /* _DECODER_H_INCLUDED */
