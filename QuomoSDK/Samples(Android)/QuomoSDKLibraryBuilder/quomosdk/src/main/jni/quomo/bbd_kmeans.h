/**
 * \brief  header file for bbd_kmeans.c
 *
 *
 *
 */
 
#ifndef _BBD_KMEANS_H_INCLUDED
#define _BBD_KMEANS_H_INCLUDED


// Macro Definitions
//
#define		NOGRAY             10            // Color                 


// Function declarations
//

extern	int 	ColorZip_Color_CellClassifier_KMEANS(czLPDecoderArea pArea);
//extern	int	long made_sqrt1(long a);

//Type definitions
//
typedef struct {
	long value;
	long sum;
	short saturation;
	short count;
	short id;
	short group1,group2;
} err;


#endif /*  _BBD_KMEANS_H_INCLUDED */
