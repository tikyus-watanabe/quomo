#include <android/log.h>

#include "colorcode.h"
#include "bbd_kmeans.h"
#include "bbd.h"
//#include "utils.h"


int 	ColorZip_Color_CellClassifier_KMEANS(czLPDecoderArea pArea);
long ColorZip_made_sqrt1(long a);

int ColorZip_Color_CellClassifier_KMEANS(czLPDecoderArea pArea)
{
	int czret;
//	short nNumPoints;
//	czCOLOR point[NPOINTS];
	short i,j;
	STATS cellStats[MAXNOCELLS];
//	short pixel_interval;
	short pRed, pGreen, pBlue;
//	short x1, y1;
	err sqr_err[MAXNOCELLS];
	short c1, c2;		
	err temp;
	short upper=pArea->nMatrixSize-2;
	short lower=1;
	long dist1, dist2;
	err max_grp_centroid, min_grp_centroid;
	short cnt_change = 1;
//	short min_h = 180 ;   // Hue andgle
//	short temp_h;
	err originColor_H[5]; //  0: Green, 1: Black, 2: Red, 3: Blue, 4: White, -1: undefined 
	short dist[5];   // distance from each color origins  
//	short min_dist_id;
//	short min_dist;
//	czCOLOR maxChannel, minChannel;                 // max/min values of (COLOR_R,COLOR_G,COLOR_B) 
//	czCOLOR originColor[5]; 
	short max_value[3] = {0,0,0};
	short min_value[3] = {255,255,255};
	short max_lum,min_lum;  // max & min luminance 
	short min_R, min_G, min_B;
	short min_R_id,min_G_id, min_B_id;  // cell id 
	short nHueCells = 0;                // # of NOGRAY cells 
	short cnt_loop;                     // preventing infinite loop of k-means

	for( i = 0; i < pArea->nMatrixSize; i++ ){
		pArea->m_nCell[i] = -1;
	}
	
	for (i=0 ;  i < pArea->nMatrixSize; i++){
		pRed = pGreen = pBlue = 0;
		for( j = 0; j < pArea->nNumPoints; j++ )
		{
			pRed += pArea->pointArry[i][j].R;
			pGreen += pArea->pointArry[i][j].G;
			pBlue += pArea->pointArry[i][j].B;
		}

			// Calculate the mean colour of the cell.
		pRed = pRed / pArea->nNumPoints;
		pGreen = pGreen / pArea->nNumPoints;
		pBlue = pBlue / pArea->nNumPoints;
		
		cellStats[i].index = i; // cell index
		cellStats[i].R = (unsigned char)pRed;
		cellStats[i].G = (unsigned char)pGreen;
		cellStats[i].B = (unsigned char)pBlue;

			// Calculate maximum difference between R, G and B.
		cellStats[i].max = MAX3( pRed, pGreen, pBlue );
		cellStats[i].min = MIN3( pRed, pGreen, pBlue );
		cellStats[i].diff = cellStats[i].max - cellStats[i].min;

		ColorZip_RGBtoHSV( &(cellStats[i]) );		
	}

//--------------

	// color extension - 2006.1.2 by Cheolho Cheong

	for (i=0; i<pArea->nMatrixSize; i++){
		if (cellStats[i].R < min_value[0]) min_value[0] = cellStats[i].R;
		if (cellStats[i].R > max_value[0]) max_value[0] = cellStats[i].R; 
		if (cellStats[i].G < min_value[1]) min_value[1] = cellStats[i].G; 
		if (cellStats[i].G > max_value[1]) max_value[1] = cellStats[i].G; 
		if (cellStats[i].B < min_value[2]) min_value[2] = cellStats[i].B; 
		    if (cellStats[i].B > max_value[2]) max_value[2] = cellStats[i].B; 
	}

	max_lum = MAX3(max_value[0],max_value[1],max_value[2]);
	min_lum = MIN3(max_value[0],max_value[1],max_value[2]);
	
	if ( max_lum >= min_lum * 1) {         // 2006.1.3 by Cheolho Cheong
	
		min_lum = MIN3(min_value[0],min_value[1],min_value[2]); // 2006.4.20 by Cheolho Cheong
		
		for (i=0; i<pArea->nMatrixSize; i++){
			cellStats[i].R = (unsigned char)((cellStats[i].R - min_value[0])*(max_lum-min_lum)/(max_value[0]-min_value[0]+1)+min_lum);
			cellStats[i].G = (unsigned char)((cellStats[i].G - min_value[1])*(max_lum-min_lum)/(max_value[1]-min_value[1]+1)+min_lum);
			cellStats[i].B = (unsigned char)((cellStats[i].B - min_value[2])*(max_lum-min_lum)/(max_value[2]-min_value[2]+1)+min_lum);
			ColorZip_RGBtoHSV( &(cellStats[i]) );
		}
	}
//---------	


	for (i=0; i<pArea->nMatrixSize; i++){
		sqr_err[i].id = i;
		sqr_err[i].value = (cellStats[i].R*30+cellStats[i].G*59+cellStats[i].B*11)/100; 	// Y	-- 32% 21% 
	}
// KMEANS ALGORITHM by Luminance Y - 2005.11.29


	for (i=0; i<pArea->nMatrixSize-1; i++){   // sorting ascending by STDDEV
		for (j=i; j<pArea->nMatrixSize; j++){
			if(sqr_err[i].value > sqr_err[j].value){
				temp = sqr_err[i];
				sqr_err[i] = sqr_err[j]; 
				sqr_err[j] = temp;
			}			                                      
		}
	}
	
	min_grp_centroid.value = min_grp_centroid.sum = sqr_err[0].value;
	max_grp_centroid.value = max_grp_centroid.sum = sqr_err[pArea->nMatrixSize-1].value;
	max_grp_centroid.count = min_grp_centroid.count = 1;
	 
	max_grp_centroid.group1 = sqr_err[pArea->nMatrixSize-1].group1 = NOGRAY;
	min_grp_centroid.group1 = sqr_err[0].group1 = COLOR_K;
	
	
	while(lower <= upper){   // 1st step for K-Means Alg.
		dist1= ABS(max_grp_centroid.value-sqr_err[upper].value);
		dist2 = ABS(sqr_err[lower].value-min_grp_centroid.value);
		
		if (dist1 > dist2){			
			min_grp_centroid.sum += sqr_err[lower].value;
			min_grp_centroid.count++;
			min_grp_centroid.value = min_grp_centroid.sum/min_grp_centroid.count;
			sqr_err[lower].group1 = COLOR_K;
			lower++;
		}
		else {
			max_grp_centroid.sum += sqr_err[upper].value;
			max_grp_centroid.count++;
			max_grp_centroid.value = max_grp_centroid.sum/max_grp_centroid.count;
			sqr_err[upper].group1 = NOGRAY;
			upper--;		
		}		

	}
	
	cnt_change = 1;
	cnt_loop = 0;   // count loop to prevent infinite loop
	
	while(cnt_change > 0){   // 2nd & 3rd steps for Alternative K-Means Alg.
		cnt_change = 0; 
		
		for (i=0; i<pArea->nMatrixSize; i++){   
			if (ABS((max_grp_centroid.value-sqr_err[i].value)) > ABS((sqr_err[i].value-min_grp_centroid.value))){
				if (sqr_err[i].group1 != COLOR_K){
					sqr_err[i].group1 = COLOR_K;
					cnt_change++;			
				}
			}			
			else {
				if (sqr_err[i].group1 != NOGRAY) {
					sqr_err[i].group1 = NOGRAY;
					cnt_change++;			
				}
			}
		}
		
		if (cnt_change > 0){
			max_grp_centroid.sum = 0;
			max_grp_centroid.count = 0;
			min_grp_centroid.sum = 0;
			min_grp_centroid.count = 0;
			
			for (i=0; i<pArea->nMatrixSize; i++){   // recomputing centroid 
				if (sqr_err[i].group1 == COLOR_K){
					min_grp_centroid.sum += sqr_err[i].value;
					min_grp_centroid.count++;
					min_grp_centroid.value = min_grp_centroid.sum/min_grp_centroid.count;					
				}
				else {
					max_grp_centroid.sum += sqr_err[i].value;
					max_grp_centroid.count++;
					max_grp_centroid.value = max_grp_centroid.sum/max_grp_centroid.count;
				}				 
			}					
		}
		if (cnt_loop++ > 100) break;          // preveningt infinite loop 
	}
	
	for (i=0; i<pArea->nMatrixSize-1; i++){   // sorting ascending by saturation
		for (j=i; j<pArea->nMatrixSize; j++){
			if(sqr_err[i].id > sqr_err[j].id){
				temp = sqr_err[i];
				sqr_err[i] = sqr_err[j]; 
				sqr_err[j] = temp;
			}			                                      
		}
	}	
	
	for (i=0; i<pArea->nMatrixSize; i++){
		c1 = (short)(-cellStats[i].R*30-cellStats[i].G*59+cellStats[i].B*89)/100; 	// C2	
		c2 = (short)(cellStats[i].R*70-cellStats[i].G*59-cellStats[i].B*11)/100; 	// C1	
		// 2006.01.17 sqrt ����E
		sqr_err[i].value = (long)ColorZip_made_sqrt1(c1*c1+c2*c2); 	// Square Error		// S -- 15% 11%
	}

	for (i=0; i<pArea->nMatrixSize-1; i++){   // sorting ascending by saturation
		for (j=i; j<pArea->nMatrixSize; j++){
			if(sqr_err[i].value > sqr_err[j].value){
				temp = sqr_err[i];
				sqr_err[i] = sqr_err[j]; 
				sqr_err[j] = temp;
			}			                                      
		}
	}

	min_grp_centroid.value = min_grp_centroid.sum = sqr_err[0].value;
	max_grp_centroid.value = max_grp_centroid.sum = sqr_err[pArea->nMatrixSize-1].value;
	max_grp_centroid.count = min_grp_centroid.count = 1;
	 
	max_grp_centroid.group2 = sqr_err[pArea->nMatrixSize-1].group2 = NOGRAY;
	min_grp_centroid.group2 = sqr_err[0].group2 = COLOR_K;
	
	upper=pArea->nMatrixSize-2;
	lower=1;
		
	while(lower <= upper){   // 1st step for K-Means Alg.
		dist1= ABS(max_grp_centroid.value-sqr_err[upper].value);
		dist2 = ABS(sqr_err[lower].value-min_grp_centroid.value);

		if (dist1 > dist2){			

			min_grp_centroid.sum += sqr_err[lower].value;
			min_grp_centroid.count++;
			min_grp_centroid.value = min_grp_centroid.sum/min_grp_centroid.count;
			sqr_err[lower].group2 = COLOR_K;
			lower++;
		}
		else {
			max_grp_centroid.sum += sqr_err[upper].value;
			max_grp_centroid.count++;
			max_grp_centroid.value = max_grp_centroid.sum/max_grp_centroid.count;
			sqr_err[upper].group2 = NOGRAY;
			upper--;		
		}		

	}
	
	
	cnt_change = 1;
	
	cnt_loop = 0;
	
	while(cnt_change > 0){   // 2nd & 3rd steps for Alternative K-Means Alg.
		cnt_change = 0; 
		
		for (i=0; i<pArea->nMatrixSize; i++){   
			if (ABS((max_grp_centroid.value-sqr_err[i].value)) > ABS((sqr_err[i].value-min_grp_centroid.value))){
				if (sqr_err[i].group2 != COLOR_K){
					sqr_err[i].group2 = COLOR_K;
					cnt_change++;			
				}
			}			
			else {
				if (sqr_err[i].group2 != NOGRAY) {
					sqr_err[i].group2 = NOGRAY;
					cnt_change++;			
				}
			}
		}
		if (cnt_change > 0){
			max_grp_centroid.sum = 0;
			max_grp_centroid.count = 0;
			min_grp_centroid.sum = 0;
			min_grp_centroid.count = 0;
			
			for (i=0; i<pArea->nMatrixSize; i++){   // recomputing centroid 
				if (sqr_err[i].group1 == COLOR_K){
					min_grp_centroid.sum += sqr_err[i].value;
					min_grp_centroid.count++;
					min_grp_centroid.value = min_grp_centroid.sum/min_grp_centroid.count;					
				}
				else {
					max_grp_centroid.sum += sqr_err[i].value;
					max_grp_centroid.count++;
					max_grp_centroid.value = max_grp_centroid.sum/max_grp_centroid.count;
				}				 
			}					
		}
	
		if (cnt_loop++ > 100) break;          // preveningt infinite loop  
	}
	
	
// END K- MEANS II by Saturation

	for (i=0; i<pArea->nMatrixSize; i++){  
		j = sqr_err[i].id;
		if ((sqr_err[i].group1 == sqr_err[i].group2) && (sqr_err[i].group1 == COLOR_K))
			pArea->m_nCell[j] = sqr_err[i].group1;
		else pArea->m_nCell[j] = NOGRAY;
	}
	
	for (i=0; i<pArea->nMatrixSize-1; i++){   // sorting ascending by saturation
		for (j=i; j<pArea->nMatrixSize; j++){
			if(sqr_err[i].id > sqr_err[j].id){
				temp = sqr_err[i];
				sqr_err[i] = sqr_err[j]; 
				sqr_err[j] = temp;
			}			                                      
		}
	}
	
	min_R = min_G = min_B= 180;         // minimum distanced angle 
	min_R_id = min_G_id = min_B_id = 0;
	
	for (i=0; i<pArea->nMatrixSize; i++){         // Minimum distance from origin
		if(pArea->m_nCell[i] == COLOR_K) continue;

		sqr_err[nHueCells].value = cellStats[i].H;
		sqr_err[nHueCells].id = i;		
		
//		printf("color Cell ID: %d | ",sqr_err[nHueCells].id);
		nHueCells++;  // # of NOGRAY cells
	}  	

//=========== Recomputation of each colors centroids 2005.12.06 by Cheolho Cheong 

	originColor_H[COLOR_G].value = 120 ;  // COLOR_R   Hue andgle
	originColor_H[COLOR_R].value   = 0 ;    // COLOR_G Hue andgle
	originColor_H[COLOR_B].value  = 240 ;  // COLOR_B  Hue andgle
	
	cnt_loop =0;
	cnt_change = 1;
	
	while(cnt_change > 0){   // Alternative K-Means Alg.
		cnt_change = 0;
	
		originColor_H[COLOR_G].sum = originColor_H[COLOR_R].sum = originColor_H[COLOR_B].sum = 0;
		originColor_H[COLOR_G].count = originColor_H[COLOR_R].count = originColor_H[COLOR_B].count = 0;
		
		for (i=0; i<nHueCells; i++){         // Minimum distance from origin
			dist[0] = (short)ABS(originColor_H[COLOR_G].value-sqr_err[i].value); 
			if (dist[0] > 180) dist[0] = 360-dist[0];     	//Green 
			dist[1] = (short)ABS(originColor_H[COLOR_R].value-sqr_err[i].value);
			if (dist[1]>180) dist[1] = 360-dist[1];         //Red 
			dist[2] = (short)ABS(originColor_H[COLOR_B].value-sqr_err[i].value);
			if (dist[2]>180) dist[2] = 360-dist[2];         //Blue

//			printf("G:%d R:%d B:%d |%d (%d %d %d)\n", originColor_H[COLOR_G].value,originColor_H[COLOR_R].value,originColor_H[COLOR_B].value,i, dist[0], dist[1], dist[2]);
					
			if (MIN3(dist[0],dist[1],dist[2]) == dist[0]){
				originColor_H[COLOR_G].sum += sqr_err[i].value; 
				originColor_H[COLOR_G].count++; 
				if (sqr_err[i].group1 != COLOR_G){
					sqr_err[i].group1 = COLOR_G; 
					cnt_change++;					
				}
			}
			else if (MIN3(dist[0],dist[1],dist[2]) == dist[1]){
				if (sqr_err[i].value > 180) originColor_H[COLOR_R].sum += (360-sqr_err[i].value);
				else originColor_H[COLOR_R].sum += sqr_err[i].value;
				originColor_H[COLOR_R].count++; 
				if (sqr_err[i].group1 != COLOR_R) {
					sqr_err[i].group1 = COLOR_R;					
					cnt_change++;
				}
			}
			else {
				originColor_H[COLOR_B].sum += sqr_err[i].value; 
				originColor_H[COLOR_B].count++; 
				if (sqr_err[i].group1 != COLOR_B) {
					sqr_err[i].group1 = COLOR_B;					
					cnt_change++;
				}
			}
		}
		
		if(originColor_H[COLOR_G].count > 0) originColor_H[COLOR_G].value = originColor_H[COLOR_G].sum/originColor_H[COLOR_G].count; 			
		if(originColor_H[COLOR_R].count > 0) originColor_H[COLOR_R].value = originColor_H[COLOR_R].sum/originColor_H[COLOR_R].count; 			
		if(originColor_H[COLOR_R].value < 0) originColor_H[COLOR_R].value+=360;
		if(originColor_H[COLOR_B].count > 0) originColor_H[COLOR_B].value = originColor_H[COLOR_B].sum/originColor_H[COLOR_B].count; 			
		
		if (cnt_loop++ > 100) break;          // preveningt infinite loop         
		
	}  // while -- Warning check infinite loop!
		
	for (i=0; i<nHueCells; i++){         // Minimum distance from origin
		j = sqr_err[i].id ;		
		pArea->m_nCell[j] = sqr_err[i].group1 ;	
	}  		
	
	
	czret = ColorZip_Color_ParityChecker(pArea);
	__android_log_print(ANDROID_LOG_DEBUG, "decoder ColorZip_Color_CellClassifier_KMEANS aaaaa1", "czret:%d,pCodeInfo.CodeValue:%llu",czret,pArea->czCODE->CodeValue);

	if ( czret == 0 ) 
	{
		return TRUE;			// success      
	} // if
	return FALSE;
}


/**
 * \brief  brief description for this function.
 *
 *  continued descriptions.
 *   2006.01.17 sqrt ����E
 *
 * \param  description for this parameter
 *
 * \return
 *      return values...
 */

long ColorZip_made_sqrt1(long a)
{
	//������ 
	long b = 0;

	//�ٱ��ϱ�E
	long i = 0;
	long k = 0;
	long j = 0;
//	long h = 0;

	// ����� ���ϱ�E
	for(i = 0; i < 99999999; i=i+100)
	{
		b = i * i;
		if(a < b)
		{
			i = i - 100;
			break;
		}
	}
	// �ʴ��� ���ϱ�E
	for(k = i ; k < i+100; k=k+10)
	{
		b = k * k;
		if(a < b)
		{
			k = k - 10;
			break;
		}
	}
	//�ϴ��� ���ϱ�E
	for(j = k; j < k+9 ; j ++)
	{
		b = j * j;
		if(a < b)
		{
			j = j - 1;
			break;
		}
	}
	return j;
}
