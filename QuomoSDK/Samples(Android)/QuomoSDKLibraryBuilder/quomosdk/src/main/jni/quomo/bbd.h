/**
 * \brief  header file for bbd.c
 *
 *
 *
 */
 
#ifndef _BBD_H_INCLUDED
#define _BBD_H_INCLUDED

// Macro Definitions
//
// #define		MIN_CZIMAGE_LENGTH	21		// Min. CZ image length (Default) 
 #define		MIN_CZIMAGE_LENGTH	7		// Min. CZ image length 
// #define		MIN_CZIMAGE_LENGTH	5		// Min. CZ image length 


// Function declarations
//

// Function declarations
//
extern	int 			ColorZip_Color_Decoder_BBD(short threshold, czLPDecoderArea pArea); // Binary image Based Decoder
extern	void 			ColorZip_RGBtoHSV( STATS *cellStats );
extern	unsigned char ColorZip_Color_ParityChecker(czLPDecoderArea pArea);
extern	unsigned char ColorZip_Color_CellClassifier(czLPDecoderArea pArea);
extern	czCOLOR  ColorZip_find_rgb_values(int yPos, int xPos, unsigned char *pImgBuff, int imgHeight, int imgWidth );
void  ColorZip_set_rgb_values(int yPos, int xPos, unsigned char *pImgBuff, int imgHeight, int imgWidth , czCOLOR point);
extern  int			 ColorZip_is_allowed_size(czPOSITION coordp[]);

#endif /*  _BBD_H_INCLUDED */
