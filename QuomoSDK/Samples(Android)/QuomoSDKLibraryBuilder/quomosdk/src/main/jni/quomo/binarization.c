#include "colorcode.h"
#include "binarization.h"
#include "bbd.h"


void 		ColorZip_BinaryFilter(unsigned char threshold, czLPDecoderArea pArea); // 0: min 128, 1 : avr128, 2 : avr90
short 	ColorZip_BinaryFilterThreshold(czLPDecoderArea pArea);
void 		ColorZip_Binarization(unsigned char threshold, czLPDecoderArea pArea);
void 		ColorZip_Mask_Frame(czLPDecoderArea pArea); // masking frames for removing background noise
void 		ColorZip_Mask_Noise(czLPDecoderArea pArea);  // masking background noise by MIDPVAL
void 		ColorZip_Noise_Filter(czLPDecoderArea pArea);  // removing masked background noise

void ColorZip_BinaryFilter(unsigned char threshold, czLPDecoderArea pArea) // 0: min 128, 1 : avr128, 2 : avr90
{

    //pArea->envParam は未使用っぽい

	switch ( threshold )
	{
	case 2:
		pArea->ThresholdValue = 110;
		pArea->envParam[0] = 55;		// 90	125 100 80
		pArea->envParam[1] = 60;		// 70	85  80  70
		pArea->envParam[2] = 65;		// 50	60	60  60
		break;	
	case 1:
		pArea->ThresholdValue = (unsigned char)ColorZip_BinaryFilterThreshold(pArea);//125;
		pArea->envParam[0] = 55;		// 90	125 100 80
		pArea->envParam[1] = 60;		// 70	85  80  70
		pArea->envParam[2] = 65;		// 50	60	60  60
		break;
	case 3:
		pArea->ThresholdValue = 70;
		pArea->envParam[0] = 50;		// 90	125 100 80
		pArea->envParam[1] = 55;		// 70	85  80  70
		pArea->envParam[2] = 60;		// 50	60	60  60
		break;
	}
	ColorZip_Binarization(threshold, pArea);
	ColorZip_Mask_Frame(pArea);
	ColorZip_Mask_Noise(pArea);
	ColorZip_Noise_Filter(pArea);
}

/**
 * \brief �ڵ����� �湁E���� �� ���� �Լ�E
 *
 * ��E��̹���E���� �׷��̱׷�E��� ���龁E���� ���� �׷�E� �߰� �κп��� ����E����E���� �׷�E� �̾� �湁E�Ǻ� ���ذ��� ��´�. 
 *
 * \param threshold : ���ڵ�E��ǁEȽ��E
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 * - �湁E�Ǻ��� ���ذ�
 */
short ColorZip_BinaryFilterThreshold(czLPDecoderArea pArea)
{
	short i, j;
	int bright;
	unsigned int BrightHistogram[256];
	short max_freq_id, threshholdvalue=0;
	unsigned int smooth_histo[256]; 
	long avg=0;
	short peak_lower, peak_higher;
	short mid;
	czCOLOR pix;
	short threshold_type = 3;
	short minpercentiles;
	short max_cell_value = 0;
	short min_cell_value = 255;

	for (i=0;i<=255;i++) BrightHistogram[i] = 0;
	for (j=0; j < pArea->BinaryImageHeight; j++)
	{         
		for (i=0; i < pArea->BinaryImageWidth; i++ ) 
		{
//			pix = ColorZip_find_rgb_values(j, i, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pix = ColorZip_find_rgb_values(j*ZOOM, i*ZOOM, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			bright = (pix.R+pix.G+pix.B)/3; 
			BrightHistogram[bright]++;	
			if (min_cell_value > bright)
			{
				min_cell_value = bright;
			}
			if (max_cell_value < bright)
			{
				max_cell_value = bright;
			}
		}
	}
	max_freq_id = 0;
	for (i = 0; i<= 255; i++){
		switch(i){                        // smoothing(5 points) & percentage calculation of frequencies (histo_freq*100/(w+h))
			case 0: smooth_histo[i] = (BrightHistogram[i]+BrightHistogram[i+1]+BrightHistogram[i+2])/3*100; break;
			case 1: smooth_histo[i] = (BrightHistogram[i-1]+BrightHistogram[i]+BrightHistogram[i+1]+BrightHistogram[i+2])/4*100; break;
			case 254: smooth_histo[i] = (BrightHistogram[i-2]+BrightHistogram[i-1]+BrightHistogram[i]+BrightHistogram[i+1])/4*100; break;
			case 255: smooth_histo[i] = (BrightHistogram[i-2]+BrightHistogram[i-1]+BrightHistogram[i])/3*100; break;
			default: smooth_histo[i] = (BrightHistogram[i-2]+BrightHistogram[i-1]+BrightHistogram[i]+BrightHistogram[i+1]+BrightHistogram[i+2])/5*100; break;
		}		
	
		avg+= i*BrightHistogram[i];
		if (smooth_histo[max_freq_id] < smooth_histo[i]) max_freq_id = i;
	}

	avg/=(pArea->BinaryImageWidth*pArea->BinaryImageHeight);
	peak_lower=1;
	peak_higher = 255; 
	for (i=1;i<=255;i++) {
		if(smooth_histo[i] >= smooth_histo[i-1]) peak_lower = i;   // finding the first peak 
		else if(smooth_histo[i] > 1000)break;
//		else break;
	}

	for (i=254;i>peak_lower;i--) {
		if(smooth_histo[i] >= smooth_histo[i+1]) peak_higher = i;  // finding the last peak
		else if (smooth_histo[i] > 1000) break;
//		else break;
	}
	mid = (peak_higher+peak_lower)/2;
	
	// range 1st to 3rd percentile of peaks
	minpercentiles =(3*peak_lower+peak_higher)/4;                       // finding the first percentile between peaks 
	for( i=(3*peak_lower+peak_higher)/4; i<= (peak_lower+3*peak_higher)/4; i++)
		if (smooth_histo[minpercentiles] > smooth_histo[i]) minpercentiles = i;

	switch(threshold_type) {
		case 0: threshholdvalue = (short)avg; break;
		case 1: threshholdvalue = (short)mid; break;
		case 2: threshholdvalue = (short)minpercentiles; break;
		case 3: threshholdvalue = min_cell_value+(short)(((max_cell_value-min_cell_value)*3)/5); break;
	}

	return threshholdvalue;
}

/**
 * \brief ��E� �̹����� ������� ����� �Լ�E
 *
 * �湁E�Ǻ� ���ذ���E���Ͽ� ��E� �̹����� ������� �����.
 *
 * \param threshold : ���ڵ�E��ǁEȽ��E
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 */
void ColorZip_Binarization(unsigned char threshold, czLPDecoderArea pArea) {
    czCOLOR pix;
	short i,j;
	if (threshold == 1) {                   // binarization - min 128 
		for (j=0; j < pArea->BinaryImageHeight; j++)
		{         
			for (i=0; i < pArea->BinaryImageWidth; i++ ) 
			{
//				pix = ColorZip_find_rgb_values(j, i, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
				pix = ColorZip_find_rgb_values(j*ZOOM, i*ZOOM, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			if ((( pix.R + pix.G + pix.B )/3)  > pArea->ThresholdValue)
					ColorZip_SetBImagePTR(j,i, MAXPVAL);
				else
					ColorZip_SetBImagePTR(j,i, MINPVAL);
			}
		}
	
	}else{                               // binarization - avr  
		for (j=0; j < pArea->BinaryImageHeight; j++)
		{         
			for (i=0; i < pArea->BinaryImageWidth; i++ ) 
			{
//				pix = ColorZip_find_rgb_values(j, i, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
				pix = ColorZip_find_rgb_values(j*ZOOM, i*ZOOM, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
				if (( pix.R > pArea->ThresholdValue) && (pix.G > pArea->ThresholdValue) && (pix.B > pArea->ThresholdValue)) 
					ColorZip_SetBImagePTR(j,i, MAXPVAL);
				else
					ColorZip_SetBImagePTR(j,i, MINPVAL);
			}
		}
	}
}


/**
 * \brief �̹���E�׵θ� ����� �Լ�E
 *
 * �ڵ尡 �׵θ��� ��ġ��E�ʾƾ� ������ �׵θ��� �������� ȸ������ �����.
 *
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 */
void ColorZip_Mask_Frame(czLPDecoderArea pArea) { // masking frames for removing background noise
	short i, j;
	for (j=0; j < pArea->BinaryImageHeight; j++)
	{
		if (ColorZip_GetBImagePTR(j,0) == MINPVAL)
		{
			ColorZip_SetBImagePTR(j,0,MIDPVAL);
		}
		if (ColorZip_GetBImagePTR(j,pArea->BinaryImageWidth-1) == MINPVAL)
		{
			ColorZip_SetBImagePTR(j,pArea->BinaryImageWidth-1, MIDPVAL);
		}
	}
	
	for (i=0; i < pArea->BinaryImageWidth; i++ ) 
	{
		if ( ColorZip_GetBImagePTR(0,i) == MINPVAL)
		{
			ColorZip_SetBImagePTR(0,i,MIDPVAL);
		}
		if ( ColorZip_GetBImagePTR(pArea->BinaryImageHeight-1,i) == MINPVAL )
		{
			ColorZip_SetBImagePTR(pArea->BinaryImageHeight-1,i,MIDPVAL);
		}
	}
}

/**
 * \brief ���� �ȼ� ������ ȸ���̸�Eȸ������ �ٲٴ� �Լ�E
 *
 * �׵θ��� �پ�E�ִ� �⿵�� ���� �ϱ�E���� ���� �ȼ� ������ ȸ��E�ȼ��� ������E�������� ȸ������ �ٲ۴�.
 *
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 */
void ColorZip_Mask_Noise(czLPDecoderArea pArea) {  // masking background noise by MIDPVAL
	short i,j;
	short j2,i2;
	for (j=1; j < pArea->BinaryImageHeight-1; j++)
	{
		j2 = pArea->BinaryImageHeight-j;
		for (i=1; i < pArea->BinaryImageWidth-1; i++) 
		{
			i2 = pArea->BinaryImageWidth-i;
			if ( ColorZip_GetBImagePTR(j,i) == MINPVAL )
				if ( ColorZip_GetBImagePTR(j-1,i-1) == MIDPVAL || ColorZip_GetBImagePTR(j-1,i)   == MIDPVAL || ColorZip_GetBImagePTR(j-1,i+1) == MIDPVAL ||
					 ColorZip_GetBImagePTR(j,i-1)   == MIDPVAL ||                                   ColorZip_GetBImagePTR(j,i+1)   == MIDPVAL ||
					 ColorZip_GetBImagePTR(j+1,i-1) == MIDPVAL || ColorZip_GetBImagePTR(j+1,i)   == MIDPVAL || ColorZip_GetBImagePTR(j+1,i+1) == MIDPVAL ) {
					ColorZip_SetBImagePTR(j,i,MIDPVAL);
				}

			if ( ColorZip_GetBImagePTR(j2,i2) == MINPVAL )
				if ( ColorZip_GetBImagePTR(j2+1,i2-1) == MIDPVAL || ColorZip_GetBImagePTR(j2+1,i2)   == MIDPVAL || ColorZip_GetBImagePTR(j2+1,i2+1) == MIDPVAL ||
					 ColorZip_GetBImagePTR(j2,i2-1)   == MIDPVAL ||                                     ColorZip_GetBImagePTR(j2,i2+1)   == MIDPVAL ||
					 ColorZip_GetBImagePTR(j2-1,i2-1) == MIDPVAL || ColorZip_GetBImagePTR(j2-1,i2)   == MIDPVAL || ColorZip_GetBImagePTR(j2-1,i2+1) == MIDPVAL ) {
					ColorZip_SetBImagePTR(j2,i2,MIDPVAL);
				}
		}
	}
}

/**
 * \brief ȸ��E�κ��� ������� ����� �Լ�E
 *
 * ȸ���� �ȼ��� ������� ���龁E�⿵���� ���� �Ѵ�.
 *
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 */
void ColorZip_Noise_Filter(czLPDecoderArea pArea) {  // removing masked background noise
	short i,j;
	for (j=0; j < pArea->BinaryImageHeight; j++)
		for (i=0; i < pArea->BinaryImageWidth; i++ ) 
			switch (ColorZip_GetBImagePTR(j,i)) {
				case MINPVAL: 
					i = pArea->BinaryImageWidth;	// break out of inner loop
					break;
				case MIDPVAL:
					ColorZip_SetBImagePTR(j,i,MAXPVAL);
					break;
			}
	
	for (j=0; j< pArea->BinaryImageHeight; j++)           // remove red - white
		for (i=pArea->BinaryImageWidth-1; i>=0; i--) 
			switch (ColorZip_GetBImagePTR(j,i)) {
				case MINPVAL: 
					i = 0;	// break out of inner loop
					break;
				case MIDPVAL:
					ColorZip_SetBImagePTR(j,i,MAXPVAL);
					break;
			}
	
	for (i=0; i<pArea->BinaryImageWidth; i++) 
		for (j=0; j<pArea->BinaryImageHeight; j++)           // remove red - white
			switch (ColorZip_GetBImagePTR(j,i)) {
				case MINPVAL: 
					j = pArea->BinaryImageHeight;	// break out of inner loop
					break;
				case MIDPVAL:
					ColorZip_SetBImagePTR(j,i,MAXPVAL);
					break;
			}
	
	for (i=pArea->BinaryImageWidth-1; i>=0; i--) 
		for (j=pArea->BinaryImageHeight-1; j>=0; j--)           // remove red - white
			switch (ColorZip_GetBImagePTR(j,i)) {
				case MINPVAL: 
					j = 0;	// break out of inner loop
					break;
				case MIDPVAL:
					ColorZip_SetBImagePTR(j,i,MAXPVAL);
					break;
			}
}
