/**
 * \brief  header file for binarization.c
 *
 *
 *
 */
 
#ifndef _BBD_BINARIZATION_H_INCLUDED
#define _BBD_BINARIZATION_H_INCLUDED


// Macro Definitions
//


// Function declarations
//
extern	void 	ColorZip_BinaryFilter(unsigned char threshold, czLPDecoderArea pArea); // 0: min 128, 1 : avr128, 2 : avr90
extern	short ColorZip_BinaryFilterThreshold(czLPDecoderArea pArea);
extern	void ColorZip_Binarization(unsigned char threshold, czLPDecoderArea pArea);
extern	void ColorZip_Mask_Frame(czLPDecoderArea pArea); // masking frames for removing background noise
extern	void ColorZip_Mask_Noise(czLPDecoderArea pArea);  // masking background noise by MIDPVAL
extern	void ColorZip_Noise_Filter(czLPDecoderArea pArea);  // removing masked background noise

//Type definitions
//


// Inline function definitions
//


#endif /*  _BBD_BINARIZATION_H_INCLUDED */
