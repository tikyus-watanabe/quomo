/**
 * \brief  header file for codearea_detect.c
 *
 *
 *
 */
 
#ifndef _CODEAREA_DETECT_H_INCLUDED
#define _CODEAREA_DETECT_H_INCLUDED


// Macro Definitions
//
#define		CODEAREADIVIDE		8	       		//colorcode 2의 배수만 가능



// Function declarations
//
extern	void ColorZip_CalculateSides(czLPDecoderArea pArea);
extern	void ColorZip_CodeAreaDetector(czLPDecoderArea pArea);

extern	short ColorZip_CodeImageFinder(czLPDecoderArea pArea);
extern	void 	ColorZip_VerticesEdgeTracer(VertexInfo* info, czPOSITION origin, short brightness, czLPDecoderArea pArea);

/*
extern	int 	Adv_Turtle(trace_Edge_Pt *Current, trace_Edge_Pt *Next, int *prev_diagonal, short brightness, czLPDecoderArea pArea);  // Advanced Turtle
extern	void 	Move(trace_Edge_Pt *Current, trace_Edge_Pt Next, enum Direction Next_direction);   // current = next
extern	int 	Detect_pixel(trace_Edge_Pt Current, enum RelativeDirection target_direction, trace_Edge_Pt *Next, short brightness, czLPDecoderArea pArea);         // determine white or black
extern	void 	Target_Location(int x, int y, int direction, trace_Edge_Pt *Next);

*/
extern	void 	ColorZip_VerticeDetector(short brightness, czPOSITION coordp[],unsigned char codetype, czLPDecoderArea pArea); // detect vertices and save to m_nCrop[][]
extern	int 	ColorZip_VerticesSelector(czPOSITION *vertices_nonedge,czPOSITION *vertices_edge); // detect adaptive Vertices 

extern	void 	ColorZip_GetRoughBoundaryRect(czLPDecoderArea pArea); // Finding Tag boundary rectagle 

// Enumerations
//
enum Direction {EAST, NORTHEAST, NORTH, NORTHWEST, WEST, SOUTHWEST, SOUTH, SOUTHEAST};
enum Direction turn(enum RelativeDirection next_direction, enum Direction current_direction);     // New Direction LEFT, RIGHT,...  // Current direction

//Type definitions
//
typedef struct{
	short x, y;
	enum Direction direction;
} trace_Edge_Pt;



#endif /*  _CODEAREA_DETECT_H_INCLUDED */
