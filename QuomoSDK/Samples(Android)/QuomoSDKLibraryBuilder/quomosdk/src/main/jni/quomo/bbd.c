#include <android/log.h>

#include "colorcode.h"
#include "bbd.h"
#include "bbd_clustering.h"
#include "bbd_hsv.h"
#include "bbd_kmeans.h"
#include "bbd_rgb.h"
//#include "utils.h"



int 			ColorZip_Color_Decoder_BBD(short threshold, czLPDecoderArea pArea); // Binary image Based Decoder
unsigned char ColorZip_Color_CellClassifier(czLPDecoderArea pArea);
void 			ColorZip_RGBtoHSV( STATS *cellStats );
unsigned char ColorZip_Color_ParityChecker(czLPDecoderArea pArea);
czCOLOR  ColorZip_find_rgb_values(int yPos, int xPos, unsigned char *pImgBuff, int imgHeight, int imgWidth );
void  ColorZip_set_rgb_values(int yPos, int xPos, unsigned char *pImgBuff, int imgHeight, int imgWidth , czCOLOR point);
int ColorZip_is_allowed_size(czPOSITION coordp[]);


/**
 * \brief �� ���� �ȼ��� �̴� �Լ�E
 *
 * ���� ũ�⸦ �Ǵ��Ͽ� ũ�⿡ ����E�ȼ��� ������ ���Ͽ� �ȼ��� �̴´�.
 *
 * \param threshold : ���ڵ�E��ǁEȽ��E
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 * - ����E: 1
 * - ���� : 0
 */

int ColorZip_Color_Decoder_BBD(short threshold, czLPDecoderArea pArea) // Binary image Based Decoder
{
	int i, x, y, czret;
	short pixel_interval;

// 2005. 3. 4 - adjust pixel sampling frequencies
// 2005. 3. 11 - adjusting pixel_interval
	// Set uncategorized cells to -1.
	if (ColorZip_is_allowed_size(pArea->m_nCrop)==0) return FALSE;

	if(pArea->nCol>=pArea->nRow) {
		pixel_interval = (short)(pArea->nRight-pArea->nLeft)/3/(pArea->nCol-1);
		if (pixel_interval > (short)(pArea->nBottom-pArea->nTop)/3/(pArea->nRow-1)) 
			pixel_interval = (short)(pArea->nBottom-pArea->nTop)/3/(pArea->nRow-1); 
	}
	else {
		pixel_interval = (short)(pArea->nRight-pArea->nLeft)/3/(pArea->nRow-1);
		if (pixel_interval > (short)(pArea->nBottom-pArea->nTop)/3/(pArea->nCol-1)) 
			pixel_interval = (short)(pArea->nBottom-pArea->nTop)/3/(pArea->nCol-1); 
	}

	for (i=0 ;  i < pArea->nMatrixSize; i++){
		y = pArea->m_nCellpos[i].y;         // cell positions
		x = pArea->m_nCellpos[i].x;
		// 2006.04.21 �ڵ�E�ȼ��� ������ �Ƕ�̵�EǁE����� �̹����� ����.
		if (pixel_interval >= 5)
		{                       // sampling  7*7
/*
			for (j=0; j<NPOINTS-1; j++) {
				y1 = (y-3) + j/7;
				x1 = (x-3) + j%7; 
				pArea->pointArry[i][j] = ColorZip_find_rgb_values(y1,x1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth );
			}
			pArea->nNumPoints = NPOINTS-1;	
*/
			pArea->pointArry[i][0] =  ColorZip_find_rgb_values(y-1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][1] =  ColorZip_find_rgb_values(y,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][2] =  ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][3] =  ColorZip_find_rgb_values(y,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][4] =  ColorZip_find_rgb_values(y+1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][5] =  ColorZip_find_rgb_values(y+1,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][6] =  ColorZip_find_rgb_values(y+1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][7] =  ColorZip_find_rgb_values(y-1,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][8] =  ColorZip_find_rgb_values(y-1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][9] =  ColorZip_find_rgb_values(y+2,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][10] = ColorZip_find_rgb_values(y+2,x+0, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][11] = ColorZip_find_rgb_values(y+2,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][12] = ColorZip_find_rgb_values(y-2,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][13] = ColorZip_find_rgb_values(y-2,x+0, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][14] = ColorZip_find_rgb_values(y-2,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][15] = ColorZip_find_rgb_values(y-1,x-2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][16] = ColorZip_find_rgb_values(y,x-2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][17] = ColorZip_find_rgb_values(y+1,x-2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][18] = ColorZip_find_rgb_values(y-1,x+2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][19] = ColorZip_find_rgb_values(y,x+2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][20] = ColorZip_find_rgb_values(y+1,x+2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][21] = ColorZip_find_rgb_values(y+0,x+3, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][22] = ColorZip_find_rgb_values(y+0,x-3, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][23] = ColorZip_find_rgb_values(y+3,x+0, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][24] = ColorZip_find_rgb_values(y-3,x+0, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][25] = ColorZip_find_rgb_values(y-2,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);                  // sampling
			pArea->pointArry[i][26] = ColorZip_find_rgb_values(y-1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][27] = ColorZip_find_rgb_values(y-1,x,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][28] = ColorZip_find_rgb_values(y-1,x+1,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][29] = ColorZip_find_rgb_values(y,x-2,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][30] = ColorZip_find_rgb_values(y,x-1,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][31] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][32] = ColorZip_find_rgb_values(y,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][33] = ColorZip_find_rgb_values(y,x+2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][34] = ColorZip_find_rgb_values(y+1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][35] = ColorZip_find_rgb_values(y+1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][36] = ColorZip_find_rgb_values(y+1,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][37] = ColorZip_find_rgb_values(y+2,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][38] = ColorZip_find_rgb_values(y-1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][39] = ColorZip_find_rgb_values(y,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][40] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][41] = ColorZip_find_rgb_values(y,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][42] = ColorZip_find_rgb_values(y+1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][43] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth); 

			pArea->nNumPoints = 44;
		}
		else if (pixel_interval > 3)
		{ 
			pArea->pointArry[i][0] = ColorZip_find_rgb_values(y-2,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);                  // sampling
			pArea->pointArry[i][1] = ColorZip_find_rgb_values(y-1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][2] = ColorZip_find_rgb_values(y-1,x,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][3] = ColorZip_find_rgb_values(y-1,x+1,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][4] = ColorZip_find_rgb_values(y,x-2,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][5] = ColorZip_find_rgb_values(y,x-1,pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][6] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][7] = ColorZip_find_rgb_values(y,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][8] = ColorZip_find_rgb_values(y,x+2, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][9] = ColorZip_find_rgb_values(y+1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][10] = ColorZip_find_rgb_values(y+1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][11] = ColorZip_find_rgb_values(y+1,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][12] = ColorZip_find_rgb_values(y+2,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][13] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth); 
			pArea->pointArry[i][14] = ColorZip_find_rgb_values(y-1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][15] = ColorZip_find_rgb_values(y,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][16] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][17] = ColorZip_find_rgb_values(y,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][18] = ColorZip_find_rgb_values(y+1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->nNumPoints = 19;
		}
		else if(pixel_interval >= 2)
		{
			pArea->pointArry[i][0] = ColorZip_find_rgb_values(y-1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][1] = ColorZip_find_rgb_values(y,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][2] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][3] = ColorZip_find_rgb_values(y,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][4] = ColorZip_find_rgb_values(y+1,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->pointArry[i][5] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth); 
//			pArea->pointArry[i][6] = ColorZip_find_rgb_values(y+1,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
//			pArea->pointArry[i][7] = ColorZip_find_rgb_values(y+1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
//			pArea->pointArry[i][8] = ColorZip_find_rgb_values(y-1,x+1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
//			pArea->pointArry[i][9] = ColorZip_find_rgb_values(y-1,x-1, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
//			pArea->nNumPoints = 10;
			pArea->nNumPoints = 6;
		}
		else 
		{
			pArea->pointArry[i][0] = ColorZip_find_rgb_values(y,x, pArea->pCapturedImageBuf, pArea->CapturedImageHeight, pArea->CapturedImageWidth);
			pArea->nNumPoints = 1;
//   			return FALSE; // �ڵ�E�̹����� �ʹ� ���� �濁E- �ڵ�E�̹����� ��Eũ�� E��ּ���E
		}
	}
	czret=ColorZip_Color_CellClassifier(pArea);
	return czret;
}


/**
 * \brief ������ �ȼ��� E��Ͽ� Į���ڵ�����E�ƴ���E�Ǻ� �ϴ� �Լ�E
 *
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 * - ����E: 1
 * - ���� : 0
 */
unsigned char ColorZip_Color_CellClassifier( czLPDecoderArea pArea )
{
	int ret;

	ret=ColorZip_Color_CellClassifier_RGB(pArea);
	if ( ret > 0 ) return 1;//Success
#if 0
	ret=ColorZip_Color_CellClassifier_HSV(pArea);
	if ( ret > 0 ) return 1; //Success

	ret=ColorZip_Color_CellClassifier_CLUST(pArea);
	if ( ret > 0 ) return 1;//Success

	ret=ColorZip_Color_CellClassifier_KMEANS(pArea);
	if ( ret > 0 ) return 1;//Success
#endif
	return 0; // fail
}


/**
 * \brief RGB ���� HSV ������ ��ȯ �Լ�E
 *
 * \param cellStats : RGB, HSV ��� ������ ����ü, RGB�� �޾� -> HSV�� �־��ش�.
 *
 * \return
 */
void ColorZip_RGBtoHSV( STATS *cellStats )
{
	unsigned char min, max, delta;
	unsigned char r = cellStats->R;
	unsigned char g = cellStats->G;
	unsigned char b = cellStats->B;
	int hue;

	min = MIN3( r, g, b );
	max = MAX3( r, g, b );
	cellStats->V = (short) max;				// v

	delta = max - min;

	if( max != 0 ){
		cellStats->S = delta / max;		// we don't care about saturation (I don't think, anyway)
	}else{
		// r = g = b = 0		// s = 0, v is undefined
		cellStats->S = 0;
		cellStats->H = -1;
		return;
	}

	if( delta != 0 )
	{
		if( r == max )
			hue = (((g - b) << 7) / delta);		// between yellow & magenta
		else if( g == max )
			hue = (2 << 7) + (((b - r) << 7) / delta);	// between cyan & yellow
		else
			hue = (4 << 7) + (((r - g) << 7) / delta);	// between magenta & cyan

		hue = (hue * 60) >> 7;
		if( hue < 0 )
			hue += 360;

		cellStats->H = hue;				// degrees
	}
	else
	{
		cellStats->H = 0;
	}

	// Hues: 0=red, 60=yellow, 120=green, 180=cyan, 240=blue, 300=magenta, 360=red
}


/**
 * \brief �丮Ƽ üũ �Լ�E
 *
 * �ఁE���� �丮Ƽ�� üũ�Ͽ� Į���ڵ�����E�ƴ���E�Ǻ�
 *
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 * - ����E: 0
 * - ���� : 1-������ ����� �и�Ƽ�� ���� �ٸ�
 *          2-ù ���̳� ������ ���� �и�Ƽ���� ����
 *          3-������ ������ �и�Ƽ�� ���� �ٸ�
 *          4-ù ���̳� ������ ���� �и�Ƽ���� ����
 *          5-�ఁE���� �и�Ƽ ����� ���� �ٸ�
 *          6-��ǁEŽ��E�Ҵ�
 */
unsigned char ColorZip_Color_ParityChecker(czLPDecoderArea pArea)
{
	short	i, j, k;
	short	rpc, cpc;					// row parity check , column parity check
	short	rpc_comp, cpc_comp;
	short	rpc_type, cpc_type;
	short	row_pos = -1;
	short	col_pos = -1;
	short	pos_indc;					// position indicator
	short	Parity[3];					// 1st, 2nd & n-1th parity type
	short	Count1,Count2;

__android_log_print(ANDROID_LOG_DEBUG, "decoder", "ColorZip_Color_ParityChecker");

	for (i=0,j=0; j < pArea->nRow; i+= pArea->nCol,j++) {				
		rpc_comp = pArea->m_nCell[i];                          // 1st cell 
		for (k = 1; k < pArea->nCol-1; k++) 
			rpc_comp = rpc_comp ^ pArea->m_nCell[i+k]; 		// k : row cells 2nd to n-1th
		
		rpc = rpc_comp ^ pArea->m_nCell[i+pArea->nCol-1];				// calculate parity 
		
		if (j == 0) 
			Parity[0] = rpc;					// 1st row paity
		else if (j == 1) 
			Parity[1] = rpc;				// middle rows parity  
		else if (j == (pArea->nRow-1)) 
			Parity[2] = rpc;		// final row parity
		else if (rpc != Parity[1]) 
			return 1;		// check middle parities are same? // �÷��ڵ��� ������ �и�Ƽ üũ ����(������ ����� �и�Ƽ�� ���� �ٸ�)  - �����ͼ��� �÷� �Ǻ� ����
	}
	
	
	//-----------------------------------------------------------------------------------
	// search rows & check parities      2007.01.05    japan
	//-----------------------------------------------------------------------------------
   if ((Parity[1] == (3^Parity[2])) && (Parity[1] == Parity[0])) 
   { 
       row_pos = pArea->nRow-1;    // rotation parity is in final row 
   } 
   else if ((Parity[1] == (3^Parity[0])) && (Parity[1] == Parity[2])) 
   { 
       row_pos = 0;                // in first row 
   } 
   else 
   { 
       return 2; //
   } 

	
	rpc_type = Parity[1];                              // identify row parity type
	
	//-----------------------------------------------------------------------------------
	// search columns & check parities
	//-----------------------------------------------------------------------------------
	for (i=0; i < pArea->nCol; i++) {
		cpc_comp = pArea->m_nCell[i];
		for (k=1; k < pArea->nRow-1; k++) 
			cpc_comp = cpc_comp ^ pArea->m_nCell[i+pArea->nCol*k];
		cpc = cpc_comp ^ pArea->m_nCell[i+pArea->nCol*(pArea->nRow-1)]; 
		
		if (i == 0) 
			Parity[0] = cpc;            // 1st column paity
		else if (i == 1) 
			Parity[1] = cpc;            // middle columns parity  
		else if (i == (pArea->nCol-1)) 
			Parity[2] = cpc;			// final column parity
		else if (cpc != Parity[1]) 
			return 3;				// check middle parities are same?

	}
  
	
	
	//-----------------------------------------------------------------------------------
	// search rows & check parities      2007.01.05    japan
	//-----------------------------------------------------------------------------------
	if ((Parity[1] == (3^Parity[2])) && (Parity[1] == Parity[0])) 
    { 
       col_pos = pArea->nCol-1;    // rotation parity is in final column 
    } 
    else if ((Parity[1] == (3^Parity[0])) && (Parity[1] == Parity[2])) 
    { 
       col_pos = 0;                // in first column 
    } 
    else 
    { 
       return 4; 	// �÷��ڵ�E����Ž��E�и�Ƽ üũ ����(ù ���̳� ������ ���� �и�Ƽ���� ����) - ������ Ž���� ��E����

    } 
	cpc_type = Parity[1];                               // identify row parity type
	
	
	// check row parity type && column parity type
	
	if (rpc_type != cpc_type) 
		return 5;
	  // �ఁE���� �и�Ƽ ����� ���� �ٸ�
	pArea->nParityType = cpc_type;                        // parity type 
//	printf("�и�Ƽ Ÿ�� : %d\n", pArea->nParityType);	
	// color tag rotation
	//           
	// 0   +---+---+ 4
	//     |       |
	//     +       +
	//     |       |
	//  40 +---+---+ 44		
	//     
	
	pos_indc = row_pos * 10 + col_pos; // positiob of rotate cell 	
	
	if (pos_indc == 0) 
		pos_indc = 0;
	else if (pos_indc == pArea->nCol-1) 
		pos_indc = 1;
	else if (pos_indc == (pArea->nRow-1)*10) 
		pos_indc = 2;
	else if (pos_indc == (pArea->nRow-1)*10+(pArea->nCol-1)) 
		pos_indc = 3;
	
	Count1=0;Count2=0;
	pArea->czCODE->CodeValue=0; // clear CZ A.I.
//	pArea->czCODE->CodeValue=2; // 初期値を入れる

	switch (pos_indc) {
	case 3:
		for (i=0,j=0; j < (pArea->nRow-1); i += pArea->nCol,j++) { //322
			for (k = 0; k < pArea->nCol-1; k++)
			{
				__android_log_print(ANDROID_LOG_DEBUG, "decoder", "ColorZip_Color_ParityChecker_pos_indc:%d,i:%d,j:%d,k:%d,pArea->m_nCell[i+k]:%d,", pos_indc,i,j,k,pArea->m_nCell[i+k]);

			}
		} // for i,j

		for (i=0,j=0; j < (pArea->nRow-1); i += pArea->nCol,j++) { //322
			for (k = 0; k < pArea->nCol-1; k++)
			{
				pArea->czCODE->CodeValue <<=2;
				pArea->czCODE->CodeValue += pArea->m_nCell[i+k];
//				__android_log_print(ANDROID_LOG_DEBUG, "decoder", "ColorZip_Color_ParityChecker_pos_indc:%d,CodeValue:%llu,", pos_indc,pArea->czCODE->CodeValue);
				__android_log_print(ANDROID_LOG_DEBUG, "decoder", "ColorZip_Color_ParityChecker_pos_indc:%d,i:%d,j:%d,k:%d,index:%d,CodeValue:%llu,", pos_indc,i,j,k,i+k,pArea->czCODE->CodeValue);

			}
		} // for i,j
//        pArea->czCODE->CodeValue <<=2;
//		pArea->czCODE->CodeValue += 99L;
		break;
	case 2:
		for (i=pArea->nCol-1,j=0; j < (pArea->nCol-1); i -= 1,j++) { //322
			for ( k = 0; k < pArea->nRow-1; k++)	
			{
				pArea->czCODE->CodeValue <<=2;
				pArea->czCODE->CodeValue += pArea->m_nCell[i+pArea->nCol*k];
				__android_log_print(ANDROID_LOG_DEBUG, "decoder", "ColorZip_Color_ParityChecker_pos_indc:%d,CodeValue:%llu,", pos_indc,pArea->czCODE->CodeValue);
			}
		} // for
		
		break;
		
	case 1:
		for (i=pArea->nCol*(pArea->nRow-1),j=0; j < (pArea->nCol-1); i += 1,j++) { //322
			for ( k = 0; k < pArea->nRow-1; k++)
			{
				pArea->czCODE->CodeValue <<=2;
				pArea->czCODE->CodeValue += pArea->m_nCell[i-pArea->nCol*k];
				__android_log_print(ANDROID_LOG_DEBUG, "decoder", "ColorZip_Color_ParityChecker_pos_indc:%d,CodeValue:%llu,", pos_indc,pArea->czCODE->CodeValue);

			}
		} // for		
		break;
		
	case 0:
		for (i=pArea->nRow*pArea->nCol-1,j=0; j < (pArea->nRow-1); i -= pArea->nCol,j++) { //322
			for ( k = 0; k < pArea->nCol-1; k++)	
			{
				pArea->czCODE->CodeValue <<=2;
				pArea->czCODE->CodeValue += pArea->m_nCell[i-k];
				__android_log_print(ANDROID_LOG_DEBUG, "decoder", "ColorZip_Color_ParityChecker_pos_indc:%d,CodeValue:%llu,", pos_indc,pArea->czCODE->CodeValue);
			}
		} // for
		break;
		
	default:
		return 6;
    	 // ��ǁEŽ��E�Ҵ�
	}
	
	return 0;
}


 
/**
 * \brief �ȼ��� RGB �� ��ȯ �Լ�E
 *
 * \param yPos : �ȼ��� x ��ǥ
 * \param xPos : �ȼ��� y ��ǥ
 * \param *pImgBuff : �̹���E����
 * \param imgHeight : �̹�����  ���� ������E
 * \param imgWidth : �̹����� ���� ������E
 *
 * \return
 * - �ȼ��� RGB ��
 */
czCOLOR  ColorZip_find_rgb_values(int yPos, int xPos, unsigned char *pImgBuff, int imgHeight, int imgWidth )
{
	czCOLOR point;
	unsigned char 	nR, nG, nB;
	unsigned char 	*pPixel;

#ifdef 	BGR888
	pPixel = (unsigned char*)(pImgBuff + (yPos * imgWidth * 3) +  xPos*3);

	nB = *(unsigned char*)(pPixel);
	nG = *(unsigned char*)(pPixel+1);
	nR = *(unsigned char*)(pPixel+2);
#endif

#ifdef 	RGB888
	pPixel = (unsigned char*)(pImgBuff + (yPos * imgWidth * 3) +  xPos*3);

//	nB = *(unsigned char*)(pPixel);

	nR = *(unsigned char*)(pPixel);
	nG = *(unsigned char*)(pPixel+1);
	nB = *(unsigned char*)(pPixel+2);

//	nR = *(unsigned char*)(pPixel+2);
#endif

#ifdef 	RGBA8888
	pPixel = (unsigned char*)(pImgBuff + (yPos * imgWidth * 4) +  xPos*4);


	nR = *(unsigned char*)(pPixel+0);
	nG = *(unsigned char*)(pPixel+1);
	nB = *(unsigned char*)(pPixel+2);

#endif

#ifdef 	RGB888_TURN
	pPixel = (unsigned char*)(pImgBuff + (((imgHeight-1)-yPos ) * imgWidth * 3) +  xPos*3);

	nB = *(unsigned char*)(pPixel);
	nG = *(unsigned char*)(pPixel+1);
	nR = *(unsigned char*)(pPixel+2);
#endif
	
#ifdef 	RGBA8888_TURN
//	pPixel = (unsigned char*)(pImgBuff + (((imgHeight-1)-yPos ) * imgWidth * 4) +  ((imgWidth -1)- xPos)*4);
	pPixel = (unsigned char*)(pImgBuff + (((imgHeight-1)-yPos ) * imgWidth * 4) +  xPos*4);

	nB = *(unsigned char*)(pPixel);
	nG = *(unsigned char*)(pPixel+1);
	nR = *(unsigned char*)(pPixel+2);

#endif

#ifdef 	BGR888_TURN
	pPixel = (unsigned char*)(pImgBuff + (((imgHeight-1)-yPos ) * imgWidth * 3) +  xPos*3);

	nR = *(unsigned char*)(pPixel);
	nG = *(unsigned char*)(pPixel+1);
	nB = *(unsigned char*)(pPixel+2);
#endif

#ifdef 	RGB888_IPHONE
	pPixel = (unsigned char*)(pImgBuff + (yPos * imgWidth * 4) +  xPos*4);
	
	nR = *(unsigned char*)(pPixel);
	nG = *(unsigned char*)(pPixel+1);
	nB = *(unsigned char*)(pPixel+2);
#endif

#ifdef 	RGB565
	unsigned char   hData, lData;
	pPixel = (unsigned char*)(pImgBuff + ( yPos * imgWidth * 2) +  xPos*2);
	
	lData = *(unsigned char*)(pPixel);
	hData = *(unsigned char*)(pPixel + 1);

	nB	=	((lData << 3) & 0xF8) | ((lData >> 2) & 0x07);
	nG	=	(((hData << 5) & 0xE0) | ((lData >> 3) & 0x1C)) | ((hData >> 1) & 0x03);
	nR	=	(hData & 0xF8) | ((hData >> 5) & 0x07);
#endif

#ifdef 	YUV422
	unsigned char   yData, vData, uData;
	pPixel = (unsigned char*)(pImgBuff + ( yPos * imgWidth * 2) + xPos*2);
	if ( xPos%2 == 0 )
	{
		yData = *(unsigned char*)(pPixel+1);
		uData = *(unsigned char*)(pPixel);
		vData = *(unsigned char*)(pPixel+2);
	}
	else
	{
		yData = *(unsigned char*)(pPixel+1);
		uData = *(unsigned char*)(pPixel-2);
		vData = *(unsigned char*)(pPixel);
	}
	nB = (unsigned char)(yData + 2 + (uData  - 128));
	nG = (unsigned char)(yData - (7141 * (vData - 128))/VALID_DIGIT -  (3441 * (uData -128))/VALID_DIGIT);
	nR = (unsigned char)(yData + (14020 * (vData - 128))/VALID_DIGIT);
#endif

#ifdef 	YUV420_YV
	unsigned char   yData, vData, uData;
	unsigned int		nIndirectPosition;
	pPixel = (unsigned char*) (pImgBuff + ( yPos * imgWidth * 2) + xPos);
	nIndirectPosition =  (yPos / 2) * (imgWidth / 2) + (xPos/2);
	yData = *(unsigned char*)(pPixel);
	uData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition);
	vData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + ((imgHeight * imgWidth) / 4) + nIndirectPosition);
	nB = (unsigned char)(yData + 2 + (uData  - 128));
	nG = (unsigned char)(yData - (7141 * (vData - 128))/VALID_DIGIT -  (3441 * (uData -128))/VALID_DIGIT);
	nR = (unsigned char)(yData + (14020 * (vData - 128))/VALID_DIGIT);
#endif

#ifdef 	YUV420_NV
	unsigned char   yData, vData, uData;
	unsigned int		nIndirectPosition;
	pPixel = (unsigned char*) (pImgBuff + ( yPos * imgWidth) + xPos);
	nIndirectPosition =  (yPos / 2) * imgWidth  + xPos;
	yData = *(unsigned char*)(pPixel);
	if( xPos % 2 == 0) 
	{
		uData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition);
		vData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition + 1);		
	}
	else
	{
		uData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition - 1);
		vData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition);		
	}
	nB = (unsigned char)(yData + 2 + (uData  - 128));
	nG = (unsigned char)(yData - (7141 * (vData - 128))/VALID_DIGIT -  (3441 * (uData -128))/VALID_DIGIT);
	nR = (unsigned char)(yData + (14020 * (vData - 128))/VALID_DIGIT);
#endif


//	if (nR > 255) nR = 255;
//	if (nG > 255) nG = 255;
//	if (nB > 255) nB = 255;
	
	point.B = (unsigned char)nB;
	point.G = (unsigned char)nG;
	point.R = (unsigned char)nR;	

	return point;
}

void  ColorZip_set_rgb_values(int yPos, int xPos, unsigned char *pImgBuff, int imgHeight, int imgWidth , czCOLOR point)
{
	unsigned char 	*pPixel;

#ifdef 	RGB888
	pPixel = (unsigned char*)(pImgBuff + (yPos * imgWidth * 3) +  xPos*3);

	*(unsigned char*)(pPixel) = point.R;
	*(unsigned char*)(pPixel+1) = point.G;
	*(unsigned char*)(pPixel+2) = point.B;
#endif

#ifdef 	RGBA8888
	pPixel = (unsigned char*)(pImgBuff + (yPos * imgWidth * 4) +  xPos*4);

	*(unsigned char*)(pPixel+0) = point.R;
	*(unsigned char*)(pPixel+1) = point.G;
	*(unsigned char*)(pPixel+2) = point.B;
#endif

#ifdef 	RGB888_TURN
	pPixel = (unsigned char*)(pImgBuff + (((imgHeight-1)-yPos ) * imgWidth * 3) +  xPos*3);

	*(unsigned char*)(pPixel) = point.B;
	*(unsigned char*)(pPixel+1) = point.G;
	*(unsigned char*)(pPixel+2) = point.R;
#endif
	
#ifdef 	RGBA8888_TURN
//	pPixel = (unsigned char*)(pImgBuff + (((imgHeight-1)-yPos ) * imgWidth * 4) +  ((imgWidth -1)- xPos)*4);
	pPixel = (unsigned char*)(pImgBuff + (((imgHeight-1)-yPos ) * imgWidth * 4) +  xPos*4);

	*(unsigned char*)(pPixel+0) = point.G;
	*(unsigned char*)(pPixel+1) = point.B;
	*(unsigned char*)(pPixel+2) = point.R;
#endif

#ifdef 	RGB888_IPHONE
	pPixel = (unsigned char*)(pImgBuff + (yPos * imgWidth * 4) +  xPos*4);
	
	*(unsigned char*)(pPixel) = point.R;
	*(unsigned char*)(pPixel+1) = point.G;
	*(unsigned char*)(pPixel+2) = point.B;
#endif

#ifdef 	RGB565
	unsigned char   hData, lData;
	pPixel = (unsigned char*)(pImgBuff + ( yPos * imgWidth * 2) +  xPos*2);
	
	lData = *(unsigned char*)(pPixel);
	hData = *(unsigned char*)(pPixel + 1);

	nB	=	((lData << 3) & 0xF8) | ((lData >> 2) & 0x07);
	nG	=	(((hData << 5) & 0xE0) | ((lData >> 3) & 0x1C)) | ((hData >> 1) & 0x03);
	nR	=	(hData & 0xF8) | ((hData >> 5) & 0x07);
#endif

#ifdef 	YUV422
	unsigned char   yData, vData, uData;
	pPixel = (unsigned char*)(pImgBuff + ( yPos * imgWidth * 2) + xPos*2);
	if ( xPos%2 == 0 )
	{
		yData = *(unsigned char*)(pPixel+1);
		uData = *(unsigned char*)(pPixel);
		vData = *(unsigned char*)(pPixel+2);
	}
	else
	{
		yData = *(unsigned char*)(pPixel+1);
		uData = *(unsigned char*)(pPixel-2);
		vData = *(unsigned char*)(pPixel);
	}
	nB = (unsigned char)(yData + 2 + (uData  - 128));
	nG = (unsigned char)(yData - (7141 * (vData - 128))/VALID_DIGIT -  (3441 * (uData -128))/VALID_DIGIT);
	nR = (unsigned char)(yData + (14020 * (vData - 128))/VALID_DIGIT);
#endif

#ifdef 	YUV420_YV
	unsigned char   yData, vData, uData;
	unsigned int		nIndirectPosition;
	pPixel = (unsigned char*) (pImgBuff + ( yPos * imgWidth * 2) + xPos);
	nIndirectPosition =  (yPos / 2) * (imgWidth / 2) + (xPos/2);
	yData = *(unsigned char*)(pPixel);
	uData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition);
	vData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + ((imgHeight * imgWidth) / 4) + nIndirectPosition);
	nB = (unsigned char)(yData + 2 + (uData  - 128));
	nG = (unsigned char)(yData - (7141 * (vData - 128))/VALID_DIGIT -  (3441 * (uData -128))/VALID_DIGIT);
	nR = (unsigned char)(yData + (14020 * (vData - 128))/VALID_DIGIT);
#endif

#ifdef 	YUV420_NV
	unsigned char   yData, vData, uData;
	unsigned int		nIndirectPosition;
	pPixel = (unsigned char*) (pImgBuff + ( yPos * imgWidth) + xPos);
	nIndirectPosition =  (yPos / 2) * imgWidth  + xPos;
	yData = *(unsigned char*)(pPixel);
	if( xPos % 2 == 0) 
	{
		uData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition);
		vData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition + 1);		
	}
	else
	{
		uData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition - 1);
		vData = *(unsigned char*)(pImgBuff + (imgHeight * imgWidth) + nIndirectPosition);		
	}
	nB = (unsigned char)(yData + 2 + (uData  - 128));
	nG = (unsigned char)(yData - (7141 * (vData - 128))/VALID_DIGIT -  (3441 * (uData -128))/VALID_DIGIT);
	nR = (unsigned char)(yData + (14020 * (vData - 128))/VALID_DIGIT);
#endif
}

/**
 * \brief �ڵ��� �ּ� ũ�⸦ ã�� �Լ�E
 *
 * \param czPOSITION coordp[] : �������� ��ǥ
 *
 * \return
 * 1 ũ�Ⱑ �ּ� �ڵ�E���� ū �濁E
 * 0 ũ�Ⱑ �ּ� �ڵ�E������ �濁E
 */
int ColorZip_is_allowed_size(czPOSITION coordp[])
{
	int minimum_length = MIN_CZIMAGE_LENGTH * MIN_CZIMAGE_LENGTH;   
	int code_length;       // �ڵ��� 4��
	int maximum_length;    // �ڵ��� 4���߿� ���̰� ����E�亯 

	code_length=(coordp[1].x-coordp[0].x)*(coordp[1].x-coordp[0].x)+(coordp[1].y-coordp[0].y)*(coordp[1].y-coordp[0].y);
	if (code_length < minimum_length) return 0;
	maximum_length = code_length;

	code_length=(coordp[2].x-coordp[0].x)*(coordp[2].x-coordp[0].x)+(coordp[2].y-coordp[0].y)*(coordp[2].y-coordp[0].y);
    if (code_length<minimum_length) return 0;
	if (code_length>maximum_length)	maximum_length=code_length;

	code_length=(coordp[2].x-coordp[3].x)*(coordp[2].x-coordp[3].x)+(coordp[2].y-coordp[3].y)*(coordp[2].y-coordp[3].y);
    if (code_length<minimum_length) return 0;
	if (code_length>maximum_length)	maximum_length=code_length;

	code_length=(coordp[1].x-coordp[3].x)*(coordp[1].x-coordp[3].x)+(coordp[1].y-coordp[3].y)*(coordp[1].y-coordp[3].y);
    if (code_length<minimum_length) return 0;
	if (code_length>maximum_length)	maximum_length=code_length;

	// 2006.04.19 ��E����̶�E4���� ����E��E����E�� �Ͽ� ��E����� ���� �濁E���� 
	if ((coordp[0].x-coordp[3].x)*(coordp[0].x-coordp[3].x)+(coordp[0].y-coordp[3].y)*(coordp[0].y-coordp[3].y)<maximum_length) return 0;
    
	if ((coordp[1].x-coordp[2].x)*(coordp[1].x-coordp[2].x)+(coordp[1].y-coordp[2].y)*(coordp[1].y-coordp[2].y)<maximum_length) return 0;

	return 1;
}
