//
// Created by 齋藤浩幸 on 16/06/01.
//
#include "QuomoInterface.h"

QuomoInterface::QuomoInterface() {
    _enabled = false;
    _licenseType = LICENSE_TYPE_TRIAL;
    _licenseMaxCount = 0;
    //
    _width = 500;
    _height = 500;
    _block_x = 6;
    _block_y = 6;
    _block_frame_thickness = 3;
    _nowCodeIndex = 0;
    _dummyCode = 0;
    _nowCodeCount = 0;
    _productCode = 0;
    _readDate = 0L;
    _readCount = 0;
    _key = 99;
    //
    STRUCT_QUOMOCODE quomocode;
    //予約コード
    quomocode.identifier = 100000L;
    quomocode.display = 0;
    quomocode.code = 0L;
    _codes[0] = quomocode;
    //ダミーコード
    quomocode.identifier = 110000L;
    quomocode.display = 0;
    quomocode.code = 0L;
    _codes[1] = quomocode;
    //コード
//    quomocode.identifier = 0;
//    quomocode.display = 0;
//    quomocode.code = 0;
//    _codes[2] = quomocode;
    //
    nextCode();
    //__android_log_print(ANDROID_LOG_DEBUG,"Tag","**** size : %d",_codes.size());
}
QuomoInterface::~QuomoInterface() {
}
/*
 * 企業コードのセット
 */
void QuomoInterface::setProductID(unsigned long long productCode){
    _productCode = productCode;
}
/*
 * 企業コードのゲット
 */
unsigned long long QuomoInterface::getProductID(void){
    return(_productCode);
}
/*
 * 指定コードのセット
 */
int QuomoInterface::addCode(unsigned long long code){
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_Noaddcode", "開始 code:%llu",code);

    if(!_enabled){
        //期限切れ
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_Noaddcode", "期限切れ");

        return 0;
    }
/*
    if(code >= _licenseMaxCount){
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_Noライセンスオーバー", "_licenseMaxCount:%llu, code:%llu", _licenseMaxCount,code);

            return 0;
    }
*/
    STRUCT_QUOMOCODE quomocode;
    quomocode.identifier = _productCode;
    quomocode.display = 0;
    quomocode.code = code;
    _codes[_nowCodeCount+2] = quomocode;

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_Noaddcode", "_nowCodeCount:%d", _nowCodeCount);

    _nowCodeCount++;
    return(_nowCodeCount-1);
}
/*
 * 指定コードのゲット
 */
unsigned long long QuomoInterface::getCode(int index){
    if(index > _nowCodeCount-1){
        return(0L);
    }
    return(_codes[index].code);
}
/*
 * 表示中コードのゲット
 */
unsigned long long QuomoInterface::getDisplayCode(int codeIndex){
    int index = codeIndex + 2;
    if(index < 0 || index >= _codes.size()){
        return 0L;
    }
    return(_codes[index].display);
}
/*
 * 次に表示するコードを取得する
 */
void QuomoInterface::nextCode(){
    int count = _codes.size();
__android_log_print(ANDROID_LOG_DEBUG, "nextCode_No01", "count:%d", count);

    //--------------------
    //プライマリコード取得
    //--------------------
    _primaryCode = _codes[_nowCodeIndex].code;
__android_log_print(ANDROID_LOG_DEBUG, "nextCode_No02", "_primaryCode:%llu", _primaryCode);
    //ダミーコード処理
    if(_codes[_nowCodeIndex].identifier >= 110000L){
        if(_dummyCode != 0){
            _primaryCode = _dummyCode;
__android_log_print(ANDROID_LOG_DEBUG, "nextCode_No03", "_primaryCode:%llu", _primaryCode);
            //__android_log_print(ANDROID_LOG_DEBUG,"Tag","dummy 1: %llu",_primaryCode);
        }
    }
    //システムコード110000処理　時間と数
    if(_codes[_nowCodeIndex].identifier >= 100000L && _codes[_nowCodeIndex].identifier < 110000L){
        _codes[_nowCodeIndex].code = makeCode100000(_nowCodeCount);
        _codes[_nowCodeIndex].display = _codes[_nowCodeIndex].code;
        _primaryCode = _codes[_nowCodeIndex].code;
__android_log_print(ANDROID_LOG_DEBUG, "nextCode_No04", "_nowCodeIndex:%d, _primaryCode:%llu", _nowCodeIndex,_primaryCode);
       //__android_log_print(ANDROID_LOG_DEBUG,"Tag","code 00000 : %llu",_primaryCode);

    }
    //任意コード処理
    if(_codes[_nowCodeIndex].identifier >= 100L && _codes[_nowCodeIndex].identifier < 100000L){
        _primaryCode = makeCode(_codes[_nowCodeIndex].identifier,_codes[_nowCodeIndex].code);
        _codes[_nowCodeIndex].display = _primaryCode;
__android_log_print(ANDROID_LOG_DEBUG, "nextCode_No05", "identifier:%llu,_nowCodeIndex:%d, _primaryCode:%llu",_codes[_nowCodeIndex].identifier,_nowCodeIndex,_primaryCode);
        //__android_log_print(ANDROID_LOG_DEBUG,"Tag","code : %llu / iden:%llu / %d",_primaryCode,_codes[_nowCodeIndex].identifier,_nowCodeIndex);
        //__android_log_print(ANDROID_LOG_DEBUG,"Tag","_nowCodeIndex : %d / %llu",_nowCodeIndex,_codes[_nowCodeIndex].identifier);

    }
    //--------------------
    //セカンダリコード取得
    //--------------------
    _nowCodeIndex++;
    if(_nowCodeIndex >= count){
        _nowCodeIndex = 0;
    }
    _secondaryCode = _codes[_nowCodeIndex].code;
 __android_log_print(ANDROID_LOG_DEBUG, "nextCode_No06", "_secondaryCode:%llu,identifier:%llu", _secondaryCode,_codes[_nowCodeIndex].identifier);

    //ダミーコード処理
    if(_codes[_nowCodeIndex].identifier >= 110000L){
        _secondaryCode = 111;
        for(int i = 0 ; i< 13 ; i++){
            _secondaryCode = _secondaryCode *10LL + lrand48() % 10;
        }
 __android_log_print(ANDROID_LOG_DEBUG, "nextCode_No07", "_secondaryCode:%llu,identifier:%llu", _secondaryCode,_codes[_nowCodeIndex].identifier);
       _dummyCode = _secondaryCode;
    }
    //任意コード処理
 __android_log_print(ANDROID_LOG_DEBUG, "nextCode_No08", "_secondaryCode:%llu,identifier:%llu", _secondaryCode,_codes[_nowCodeIndex].identifier);
     if(_codes[_nowCodeIndex].identifier >= 100L && _codes[_nowCodeIndex].identifier < 100000L){
        _secondaryCode = makeCode(_codes[_nowCodeIndex].identifier,_codes[_nowCodeIndex].code);
 __android_log_print(ANDROID_LOG_DEBUG, "nextCode_No09", "_secondaryCode:%llu,identifier:%llu", _secondaryCode,_codes[_nowCodeIndex].identifier);

    }
    //__android_log_print(ANDROID_LOG_DEBUG,"Tag","****! size : %d",count);
    //__android_log_print(ANDROID_LOG_DEBUG,"Tag","p : %lld / s : %lld",_primaryCode,_secondaryCode);



    for(int i=0 ; i < _block_x * _block_y ; i++){
        _transParentBuffer[i] = 0.0f;
    }
    //_primaryCode=123000000012345LL;  //15 ok
    //_primaryCode=1230000000123456LL; //16 ng
    //_primaryCode=1234534436344353LL;
    //_secondaryCode=_primaryCode;
}
/*
 * Quomoイメージを作成する
 */

void QuomoInterface::makeQuomoImage(cv::Mat &matImage ) {
    char quomoBuffer[_block_x * _block_y];
    char quomoBuffer2[_block_x * _block_y];
    quomoEncode(_primaryCode, quomoBuffer);
    quomoEncode(_secondaryCode, quomoBuffer2);
    matImage.create(_width, _height, CV_8UC4);
    int frameSize = (int)(_width / 10);
    int blockSize = (_width - (frameSize * 2)) / _block_x;
    //黒で塗りつぶし
    cv::rectangle(matImage, cv::Point(0,0), cv::Point(_width,_height), cv::Scalar(64,64,64,255), -1, CV_AA);
    //外形線描画
    cv::rectangle(matImage, cv::Point(0,0), cv::Point(_width,_height), cv::Scalar(255,255,255,255), frameSize*2, 4);
    //ブロック描画
    int  x,y,px,py;
    int r=255,g=255,b=255;
    int r1,g1,b1;
    int r2,g2,b2;
    char c;

    float k1,k2,transparent;
    int countTransparent = 0;

    time_t nowtime = time(NULL);
    struct tm* now = localtime(&nowtime);
    int sec = now->tm_sec % 5;


    for(y = 0; y < _block_y; y++){
        for(x = 0; x < _block_x; x++) {
            transparent = _transParentBuffer[y * _block_y + x];
            float k1 = 1.0f - transparent;
            float k2 = transparent;
            c = quomoBuffer[y * _block_y + x];
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No10", "c1:%c",  c);

            switch(c){
                case 'R':
                    r1=COLOR_STRONG;g1=COLOR_WEAK;b1=COLOR_WEAK;break;
                case 'G':
                    r1=COLOR_WEAK;g1=COLOR_STRONG;b1=COLOR_WEAK;break;
                case 'B':
                    r1=COLOR_WEAK;g1=COLOR_WEAK;b1=COLOR_STRONG;break;
                case 'K':
                    r1=0;g1=0;b1=0;break;
            }
            c = quomoBuffer2[y * _block_y + x];
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No11", "_primaryCode:%llu,_secondaryCode:%llu,c2:%c", _primaryCode,_secondaryCode,c);

            switch(c){
                case 'R':
                    r2=COLOR_STRONG;g2=COLOR_WEAK;b2=COLOR_WEAK;break;
                case 'G':
                    r2=COLOR_WEAK;g2=COLOR_STRONG;b2=COLOR_WEAK;break;
                case 'B':
                    r2=COLOR_WEAK;g2=COLOR_WEAK;b2=COLOR_STRONG;break;
                case 'K':
                    r2=0;g2=0;b2=0;break;
            }
            //
            r = r1 * k1 + r2 * k2;
            g = g1 * k1 + g2 * k2;
            b = b1 * k1 + b2 * k2;

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No12", "x:%d, y:%d, r:%d, g:%d, b:%d", x,y,r,g,b);

            //r=r1;
            //g=g1;
            //b=b1;
            px = x * blockSize + frameSize;
            py = y * blockSize + frameSize;
            cv::rectangle(matImage, cv::Point(px+_block_frame_thickness+1, py+_block_frame_thickness+1), cv::Point(px + blockSize-_block_frame_thickness, py + blockSize-_block_frame_thickness), cv::Scalar(r, g, b, 255), -1, CV_AA);
            //
            //float add = (float)lrand48() / RAND_MAX / 50.0f;
            float add = (float)lrand48() / 2147483647L / 1.0f;
__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No13", "add:%f, transparent:%f, index:%d, _transParentBuffer[index]:%f", add,transparent,y * _block_y + x,_transParentBuffer[y * _block_y + x]);

            transparent += add;
            if(transparent > 1.0){
                transparent = 1.0f;
                countTransparent++;
            }
            _transParentBuffer[y * _block_y + x] = transparent;

__android_log_print(ANDROID_LOG_DEBUG, "makeCC_internal2_No14", "add:%f, transparent:%f, index:%d, _transParentBuffer[index]:%f", add,transparent,y * _block_y + x,_transParentBuffer[y * _block_y + x]);

        }
    }
    //
    //__android_log_print(ANDROID_LOG_DEBUG,"Tag","countTransparent : %d",countTransparent);
    if(countTransparent >= _block_x * _block_y){
        nextCode();
    }
}


void QuomoInterface::makeQuomoColors(QUOMOCOLORS *color) {
    char quomoBuffer[_block_x * _block_y];
    char quomoBuffer2[_block_x * _block_y];
    quomoEncode(_primaryCode, quomoBuffer);
    quomoEncode(_secondaryCode, quomoBuffer2);

    int  x,y,px,py;
    int r=255,g=255,b=255;
    int r1,g1,b1;
    int r2,g2,b2;
    char c;


    float k1,k2,transparent;
    int countTransparent = 0;

    for(y = 0; y < _block_y; y++){
        for(x = 0; x < _block_x; x++) {
            transparent = _transParentBuffer[y * _block_y + x];
            float k1 = 1.0f - transparent;
            float k2 = transparent;
            c = quomoBuffer[y * _block_y + x];
            switch(c){
                case 'R':
                    r1=COLOR_STRONG;g1=COLOR_WEAK;b1=COLOR_WEAK;break;
                case 'G':
                    r1=COLOR_WEAK;g1=COLOR_STRONG;b1=COLOR_WEAK;break;
                case 'B':
                    r1=COLOR_WEAK;g1=COLOR_WEAK;b1=COLOR_STRONG;break;
                case 'K':
                    r1=0;g1=0;b1=0;break;
            }
            c = quomoBuffer2[y * _block_y + x];
            switch(c){
                case 'R':
                    r2=COLOR_STRONG;g2=COLOR_WEAK;b2=COLOR_WEAK;break;
                case 'G':
                    r2=COLOR_WEAK;g2=COLOR_STRONG;b2=COLOR_WEAK;break;
                case 'B':
                    r2=COLOR_WEAK;g2=COLOR_WEAK;b2=COLOR_STRONG;break;
                case 'K':
                    r2=0;g2=0;b2=0;break;
            }
            //
            r = r1 * k1 + r2 * k2;
            g = g1 * k1 + g2 * k2;
            b = b1 * k1 + b2 * k2;
            color[y * _block_y + x].r=r;
            color[y * _block_y + x].g=g;
            color[y * _block_y + x].b=b;
            //
            float add = (float)lrand48() / 2147483647L / 1.0f;

            transparent += add;
            if(transparent > 1.0){
                transparent = 1.0f;
                countTransparent++;
            }
            _transParentBuffer[y * _block_y + x] = transparent;

        }
    }
    //
    //__android_log_print(ANDROID_LOG_DEBUG,"Tag","countTransparent : %d",countTransparent);
    if(countTransparent >= _block_x * _block_y){
        nextCode();
    }
}

/*
 * システムコード100000用のコードを生成する
 */
long long QuomoInterface::makeCode100000(unsigned int count ) {
    unsigned long long work;
    long long result;
    time_t nowtime = time(NULL);
    struct tm* now = localtime(&nowtime);
    long long date = (now->tm_year+1900)*100000000LL + (now->tm_mon + 1)*1000000LL + now->tm_mday*10000LL + now->tm_hour*100LL + now->tm_min;
    //100000コード作成
    result = 1000000000000000L + date * 100L + count;
    return(result);
}
/*
 * 表示用コードを生成する
 * 企業コード、指定コードを受け取り、チェックサムを計算して、表示用コードを作成する
 */
long long QuomoInterface::makeCode(unsigned long long identifier,unsigned long long code) {
    unsigned long long result=0L;
    unsigned long long work;
    //スクランブル用キー取得
    time_t nowtime = time(NULL);
    struct tm* now = localtime(&nowtime);
    unsigned long long key = now->tm_min % 10;
    if(key == 0) key = 10;
    //key = 1;
    //コードをスクランブル
    unsigned long long scrmbledCode = code * key;

    //チェックサム計算
    unsigned int sum = 0;
    work = identifier * 1000000000LL + scrmbledCode;
    for(int i = 0 ; i < 18; i++){
        sum = sum + work % 10;
        work /= 10;
    }
    //表示コード作成
    result = identifier * 10000000000LL + (sum % 10) * 1000000000LL + scrmbledCode;
    //__android_log_print(ANDROID_LOG_DEBUG,"Tag","makecode - o : %lld / s : %lld",code,scrmbledCode);
__android_log_print(ANDROID_LOG_DEBUG, "nextCode_No makeCode", "identifier:%llu,code:%llu,key:%llu,scrmbledCode:%llu,work:%llu,sum:%d,result:%llu", identifier,code,key,scrmbledCode,work,sum,result);

    //
    return(result);
}
/*
 * システムコード100000用のコードを解読する
 */
void QuomoInterface::decode100000(unsigned long long code, unsigned long long *dstDate, unsigned int *dstCount ) {
    *dstDate = (code % 100000000000000) / 100L;
    *dstCount = code % 100L;

    __android_log_print(ANDROID_LOG_DEBUG, "decoder", "decode100000_code:%llu,dstDate:%llu,dstCount:%llu", code,(code % 100000000000000) / 100L,code % 100L);

}
/* 一部のコードを読み取ったら1を返す
 * コードを全て読み取ったら2を返す
 * 何も読み取れない場合は0を返す
 */
unsigned long long QuomoInterface::decoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo){

//__android_log_print(ANDROID_LOG_DEBUG, "decoder", "c=%c, n=%d", (char)c, n);

/*
	short				CodeType;
	int				CodeShapeType;
#ifdef BIGINT
	unsigned long long   	CodeValue;

*/

__android_log_print(ANDROID_LOG_DEBUG, "decoder_No00", "開始");

    unsigned long long result = 0;
    short ret = czDecoder(pArea, codeType, codeSizeType, pCapturedImage , pCodeInfo);

    unsigned long long code = pCodeInfo->CodeValue;

// 1
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No01", "code:%llu,_readCode.size:%d", code,_readCode.size());

//このあたりがシステムコードチェック
//現時点では、countはゼロで設定されている。
    if(ret == -1)return(result);

    //ダミーコード処理
    if(code >= 1100000000000000L){
        result = 1;
    }

    //システムコード 時間と数
    if(code >= 1000000000000000L && code < 1100000000000000L){
//__android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "system code");

        unsigned long long date;
        unsigned int count;
        decode100000(code,&date,&count);

//2
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No02", "code:%llu,date:%llu,count:%llu",  code,date,count);

//__android_log_print(ANDROID_LOG_DEBUG, "decoder", "decode100000_code:%d,date:%d,count:%d", code,&date,&count);
//__android_log_print(ANDROID_LOG_DEBUG, "decoder", "decode100000_code:%llu,date:%llu,count:%llu,_readDate:%llu,date:%llu", code,date,count,_readDate,date);


        if(_readDate != date) {
            //__android_log_print(ANDROID_LOG_DEBUG,"QuomoCamera","date : %llu / count:%u",date,count);
//3
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No03", "_readDate:%llu,_readCount:%llu,_key:%d",  _readDate,_readCount,_key);
            _readDate = date;
            _readCount = count;
            _key = _readDate % 10;

            _readCode.clear();
        }
        result = 1;
    }

/////pArea->m_nCell[i] = color;

//おそらくここからが色読み取りデータチェック
// 最大26ビット（少なくても最初２ビットは00でなければNG）
    //任意コード処理
    if(code < 1000000000000000L && code != 0L && _readDate != 0L){
//////////////////////////////////////////////////
    // (0,0) が緑か黒のみ通る処理をはずしてみる。
    //if(code != 0L && _readDate != 0L){
//////////////////////////////////////////////////
//__android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "unique code");

        result = 1;
        //分チェック
        time_t nowtime = time(NULL);
        struct tm* now = localtime(&nowtime);
        unsigned int nowKey  = now->tm_min % 10;

__android_log_print(ANDROID_LOG_DEBUG, "decoder_No04", "now->tm_min:%d,nowKey:%d",now->tm_min,nowKey);

//入れてみる////////////////////////////////
//_readCount = 10;
/////////////////////////////////

__android_log_print(ANDROID_LOG_DEBUG, "decoder_No05", "_key:%d,nowKey:%d,_readCode.size():%d,_readCount:%d",_key,nowKey,_readCode.size(),_readCount);

        if(_key != nowKey){
//__android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "unique code - reset");
            //分が変わったため、ここまで読んだコードをクリアしてシステムコードの読み取りからやり直し
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No6", "分チェック");
            _key = 99;
            _readCount = 0;
            _readDate = 0L;
            _readCode.clear();
        }
//        else if(_readCode.size() < _readCount) {
        // 初期分引く？
        else if((_readCode.size() /2) < _readCount) {

__android_log_print(ANDROID_LOG_DEBUG, "decoder_No07", "_readCode.size():%d",_readCode.size());
            //読みこんだコードが存在するかチェック
            int index = -1;
            for(int i=0;i<_readCode.size();i++) {
                if(_readCode[i] == code){
                    index = i;
                    break;
                }
            }
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No08", "index:%d",index);

            if (index != -1) {
//__android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "unique code - already");
                //存在するので追加しない
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No09", "存在するので追加しない");
            }
            else {
//__android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "unique code - none");
                //存在しないので追加する
                result = 1;
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No09-1", "_readCode.size:%d,code:%llu",_readCode.size(),code);

                for(int d=0;d<_readCode.size();d++){
                     __android_log_print(ANDROID_LOG_DEBUG, "decoder_No09-2", "index:%d,_readCode[i]:%llu",d,_readCode[d]);
                }



//                _readCode[_readCode.size()] = code;
                int idx = _readCode.size();
                _readCode[idx] = code;
__android_log_print(ANDROID_LOG_DEBUG, "decoder_No10", "idx:%d,_readCode[_readCode.size()]:%llu,_readCode.size():%d,code:%llu",idx,_readCode[_readCode.size()],_readCode.size(),code);

                for(int d=0;d<_readCode.size();d++){
                     __android_log_print(ANDROID_LOG_DEBUG, "decoder_No010-2", "index:%d,_readCode[i]:%llu",d,_readCode[d]);
                }

                //__android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "size : %u / code:%llu", _readCode.size(), code);
//              if (_readCode.size() == _readCount) {
                if (_readCode.size() /2 == _readCount) {
                    //システムコードで指定されているコード数に達したら全て読み終わったと判定する
                    //__android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "all read!!!!");
                    result = 2;
                    _decodedReadCode.clear();
                    unsigned int key = _readDate % 10;
                    if(key == 0) key=10;
                    int j = 0;
                    for(int i=0;i<_readCode.size();i++){
//                        _decodedReadCode[i] = ( _readCode[i] % 1000000000L ) / key;
                        if(_readCode[i] != 0)
                        {
                            _decodedReadCode[j] = ( _readCode[i] % 1000000000L ) / key;
                            j++;
                        }

//                        __android_log_print(ANDROID_LOG_DEBUG, "QuomoCamera", "index:%u / _readCode:%llu / decode:%llu",i,_readCode[i],_decodedReadCode[i]);
                        __android_log_print(ANDROID_LOG_DEBUG, "decoder_No11", "index:%u / _readCode:%llu / key:%d /_decodedReadCode:%llu / _readDate:%llu",i,_readCode[i],key,_decodedReadCode[i],_readDate);
                    }
                    _key = 99;
                    _readCount = 0;
                    _readDate = 0L;
                    _readCode.clear();
                }
            }
        }
    }
    //

__android_log_print(ANDROID_LOG_DEBUG, "decoder_No12", "result=%d", result);

__android_log_print(ANDROID_LOG_DEBUG, "decoder_No99", "終了");

    return(result);
}

bool QuomoInterface::SetProductKey(char *productKey){
    bool result = false;
    char* text = this->decodeProductKey(productKey);
    if(text==NULL) {
        return false;
    }
    //カンマで区切る
    vector<string> elements = this->split(string(text),',');
    //__android_log_print(ANDROID_LOG_DEBUG, "SSL", "%s",text);
    for(int i=0;i<elements.size();i++){
        vector<string> section = this->split(elements[i],':');
        //__android_log_print(ANDROID_LOG_DEBUG, "SSL", "section : %s | %s",section[0].c_str(),section[1].c_str());
        if(section[0]==string(SECTION_PRODUCTID)){//プロダクトID
            stringstream ss;
            unsigned long productID;
            ss << section[1];
            ss >> productID;
            setProductID(productID);
            __android_log_print(ANDROID_LOG_DEBUG, "SSL", "setProductID : %lu",productID);
        }
        if(section[0]==string(SECTION_EXPIRE)){//期限
            time_t nowtime = time(NULL);
            struct tm* now = localtime(&nowtime);
            unsigned long nowDate = (now->tm_year+1900)*10000LL + (now->tm_mon + 1)*100LL + now->tm_mday;
            unsigned long expire;
            stringstream ss;
            ss << section[1];
            ss >> expire;
            if(expire >= nowDate){
                _enabled = true;
                result = true;
            }
            __android_log_print(ANDROID_LOG_DEBUG, "SSL", "nowdate : %lu  |  date : %lu",expire,nowDate);
        }
        if(section[0]==string(SECTION_TYPE)){//ライセンスタイプ
            if(section[1]==string("trial")){
                _licenseType = LICENSE_TYPE_TRIAL;
            }
            if(section[1]==string("normal")){
                _licenseType = LICENSE_TYPE_NORMAL;
            }
            __android_log_print(ANDROID_LOG_DEBUG, "SSL", "_licenseType : %d | %s",_licenseType,section[1].c_str());
        }
        if(section[0]==string(SECTION_LIMIT)){//コード上限
            stringstream ss;
            ss << section[1];
            ss >> _licenseMaxCount;
            //__android_log_print(ANDROID_LOG_DEBUG, "SSL", "_licenseMaxCount : %llu",_licenseMaxCount);
        }
        __android_log_print(ANDROID_LOG_DEBUG, "SSL", "element: %s",elements[i].c_str());
    }
    free(text);
    //
    return(result);
}

char* QuomoInterface::decodeProductKey(char *productKey){
    string result="";
    EVP_CIPHER_CTX  de;
    //char *key = (char *)"Q0M0PV2GGD1AHILZ\0";

    char *key=NULL;
    char *ptr = productKey;
    char *data;
    char buf[80];
    int     datasize,c;
    int     p_len,f_len=0;
    char    *plaintext;

    key = (char *)malloc(17);
    memset(key,0,17);
    strcat(key,"Q0M0");
    char *d1=(char *)"ASDAFAFGEWAAFADA";
    strcat(key,"PV2G");
    char *d2=(char *)"ADFAFASF332SFS2FAFFGGQQAQS";
    strcat(key,"GD1A");
    char *d3=(char *)"123ASFBESDDDDDD";
    strcat(key,"HILZ");

    char *d4=NULL;
    d4=(char *)malloc(256);
    strcat(d4,d1);
    strcat(d4,d2);
    strcat(d4,d3);


    if(strlen(key)!=EVP_CIPHER_key_length(EVP_aes_128_ecb())){
        return(NULL);
    }
    data=(char *)malloc(strlen(ptr));
    for(c=0;*ptr!='\0';ptr+=2,c++){
        buf[0]=*ptr;
        buf[1]=*(ptr+1);
        buf[2]='\0';
        data[c]=strtol(buf,NULL,16);
    }
    datasize=c;

    EVP_CIPHER_CTX_init(&de);
    EVP_DecryptInit_ex(&de,EVP_aes_128_ecb(),NULL,(unsigned char *)key,NULL);

    p_len=datasize;
    plaintext=(char *)calloc(p_len+1,sizeof(char));

    EVP_DecryptUpdate(&de,(unsigned char *)plaintext,&p_len,(unsigned char *)data,datasize);
    EVP_DecryptFinal_ex(&de,(unsigned char *)(plaintext+p_len),&f_len);

    plaintext[p_len+f_len]='\0';
    //printf("%s\n",plaintext);

    EVP_CIPHER_CTX_cleanup(&de);



    //free(plaintext);
    free(data);
    return(plaintext);
    return NULL;
}

//
unsigned int  QuomoInterface::getDecodeCodeCount(){
    if(!_enabled){
        return 0;
    }
    return(_decodedReadCode.size());
}
//
unsigned long long int QuomoInterface::getDecodeCode(unsigned int index){
    unsigned long long int resultCode = 0L;


    if(!_enabled){
        return 0L;
    }
    if(_decodedReadCode.size() <= index)return(0);
    //
    resultCode = _decodedReadCode[index];

    __android_log_print(ANDROID_LOG_DEBUG, "decoder_No13", "resultCode:%llu / _licenseMaxCount:%llu ",resultCode,_licenseMaxCount);
/*
    if(resultCode >= _licenseMaxCount){
        resultCode = 0L;
    }
*/
    //}
    //
    return(resultCode);
}
vector<string> QuomoInterface::split(const string &s, char delim) {
    vector<string> elems;
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        if (!item.empty()) {
            elems.push_back(item);
        }
    }
    return elems;
}