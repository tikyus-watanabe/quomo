/**
 * \brief decoder.c : colorcode decoding modules
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include <android/log.h>

#include "colorcode.h"
#include "decoder.h"

// Took from ColorCodeController.h 
#define WORKIMAGE_WIDTH		280
#define WORKIMAGE_HEIGHT	280
// #define MAXWIDTH 320
// #define MAXHEIGHT 480
#define MAXWIDTH 200*2
#define MAXHEIGHT 200*2

czDecoderArea iArea;
unsigned char ibuf[MAXWIDTH * MAXHEIGHT];
unsigned char *iptr[MAXHEIGHT];

short	czDecoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo);
short	qDecoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo);
czLPDecoderArea		initialize(int  nOrgWidth, 	int nOrgHeight);
void	finalize(czLPDecoderArea pArea);
//short 		czDecoder( short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo);
//int 		initialize( int  nOrgWidth, 	int nOrgHeight) ;
//void 		finalize(void);

/**

 */
short czDecoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo)
{
	short tryCount;

	if ( codeType != COLOR_CZCODE )  ///< currenlty only Color type can be decoded.
	{
		return -1;
	}
	pCodeInfo->CodeType = codeType;

	pArea->czCODE = pCodeInfo;
	pArea->pCapturedImageBuf = (unsigned char *) pCapturedImage;
	pArea->czCODE->CodeValue = 0; //colorcode value init

	tryCount = ColorZip_decode( codeType, pArea );

__android_log_print(ANDROID_LOG_DEBUG, "decoder_czDecoder", "tryCount:%d",tryCount);

	if ( tryCount > 0 ) // done decoding process.
	{
		if (pArea->nCol == 6 && pArea->nRow == 6 )
		{
			pCodeInfo->CodeShapeType = SIX_BY_SIX_CZCODE + pArea->nParityType+1;
__android_log_print(ANDROID_LOG_DEBUG, "decoder_czDecoder", "Col & Row OK");
		}
		else
		{
			tryCount = 0 ;  ///< this is unknown size code, we should return fail
		}
	}
	//pCodeInfo->CodeValue = 123456789L;

	return tryCount;
}// czDecoder().

short qDecoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo)
{
    short				tryCount;
    
    if ( codeType != COLOR_CZCODE )  ///< currenlty only Color type can be decoded.
    {
        return -1;
    }
    pCodeInfo->CodeType = codeType;
    
    pArea->czCODE = pCodeInfo;
    pArea->pCapturedImageBuf = (unsigned char *) pCapturedImage;
    pArea->czCODE->CodeValue = 0; //colorcode value init
    
    tryCount = ColorZip_decode( codeType, pArea );
    
    if ( tryCount > 0 ) // done decoding process.
    {
        if (pArea->nCol == 6 && pArea->nRow == 6 )
            pCodeInfo->CodeShapeType = SIX_BY_SIX_CZCODE + pArea->nParityType+1;
        else
            tryCount = 0 ;  ///< this is unknown size code, we should return fail
    }
    
    
    return tryCount;
}// czDecoder().

/**
 * \brief Deocder ∂Û¿Ã∫ÅEØ∏Æ √ ±‚»≠ «‘ºÅE
 *
 * ±‚∫ª ¡§∫∏∏¶ ∞°¡ˆ∞ÅE¥Ÿ¥œ¥¬ ±∏¡∂√ºø° ±‚∫ª ¡§∫∏ ∞™¿ª  º¬∆√ «—¥Ÿ.
 * 
 *
 * \param int nOrgWidth : ƒ∏√ƒ ¿ÃπÃ¡ˆ¿« ∞°∑Œ ≈©±ÅE
 * \param int nOrgHeight : ƒ∏√ƒ ¿ÃπÃ¡ˆ¿« ºº∑Œ ≈©±ÅE
 *
 * \return czLPDecoderArea pArea : µƒ⁄µÅE«œ±ÅE¿ß«— ±‚∫ª ¡§∫∏∏¶ ∞°¡ˆ∞ÅE¥Ÿ¥œ¥¬ ±∏¡∂√º
 */
czLPDecoderArea initialize(int  nOrgWidth, 	int nOrgHeight)
{
	int i;
	czLPDecoderArea pArea;
#ifdef RGB888_IPHONE
	pArea = &iArea;
#else
	pArea = (czLPDecoderArea)malloc(sizeof(czDecoderArea));
	if (pArea  == NULL )
		return NULL;
#endif

	pArea->CapturedImageWidth = nOrgWidth;
	pArea->CapturedImageHeight = nOrgHeight;
	// ZOOM
	pArea->BinaryImageWidth  = nOrgWidth/ZOOM;
	pArea->BinaryImageHeight = nOrgHeight/ZOOM;
#ifdef RGB888_IPHONE
	pArea->czBImagePTR = (unsigned char **)&iptr;
#else
	pArea->czBImagePTR = (unsigned char **)malloc(pArea->BinaryImageHeight * sizeof(unsigned char*));
	if ( pArea->czBImagePTR == NULL )
	{
		free (pArea);
		return NULL;
	}
#endif
	
#ifdef RGB888_IPHONE
	pArea->czBImage = (unsigned char*)&ibuf;
#else
	pArea->czBImage = (unsigned char *)malloc(pArea->BinaryImageHeight * pArea->BinaryImageWidth);
	if ( pArea->czBImage == NULL )
	{
		free (pArea->czBImagePTR );
		free (pArea);
		return NULL;
	}
#endif
	
	for (i=0; i < pArea->BinaryImageHeight; i++)
	{
		pArea->czBImagePTR[i]=(unsigned char*)(pArea->czBImage+(i*pArea->BinaryImageWidth));
	}

	pArea->nProcess=1;
#ifdef MULTICODE
	pArea->firstScan = TRUE;
	for(i=0; i < MAXNOCELLS;i++)
	pArea->m_nCell[ i ] = 0;
#endif
	return pArea;
}

/**
 * \brief Deocder ∂Û¿Ã∫ÅEØ∏Æ∏¶ ∏∂ƒ°¥¬ «‘ºÅE
 *
 * ªÁøÅE“∑¡∞ÅE¿‚¿∫ ∏ﬁ∏∏ÆµÈ¿ª «ÿ¡¶ Ω√ƒ— ¡ÿ¥Ÿ.
 *
 * \param czLPDecoderArea pArea : µƒ⁄µÅE«œ±ÅE¿ß«— ±‚∫ª ¡§∫∏∏¶ ∞°¡ˆ∞ÅE¥Ÿ¥œ¥¬ ±∏¡∂√º
 *
 * \return void
 */
void finalize(czLPDecoderArea pArea)
{
#ifdef RGB888_IPHONE
#else
	free(pArea->czBImagePTR);
	free(pArea->czBImage);
	free(pArea);
#endif
}
