#include <android/log.h>

#include "colorcode.h"
#include "bbd_rgb.h"
#include "bbd.h"
//#include "utils.h"

int 	ColorZip_Color_CellClassifier_RGB(czLPDecoderArea pArea);
char Color_Vote_RGB(czLPCOLOR point, short env_id, czLPDecoderArea pArea);
char Color_Convert_RGB(czLPCOLOR point, short env_id,czLPDecoderArea pArea);

int ColorZip_Color_CellClassifier_RGB(czLPDecoderArea pArea)
{
	int i, j;
	short color;
	int bReturnVal;
	int czret;
	for (j=0;j<3;j++)
	{
		for( i = 0; i < pArea->nMatrixSize; i++ ){
			pArea->m_nCell[i] = -1;
		}
		bReturnVal = TRUE;
		for (i=0 ;  i < pArea->nMatrixSize; i++){
			color = Color_Vote_RGB(pArea->pointArry[i], j, pArea);

            __android_log_print(ANDROID_LOG_DEBUG, "ColorZip_Color_CellClassifier_RGB_No01", "i:%d, color:%d",i,color);

			if (color == -1)
			{
				bReturnVal = FALSE;
			}
			pArea->m_nCell[i] = color;
		}
		if( bReturnVal ){
			czret = ColorZip_Color_ParityChecker(pArea);
            __android_log_print(ANDROID_LOG_DEBUG, "decoder ColorZip_Color_CellClassifier_RGB aaaaa4", "czret:%d,pCodeInfo.CodeValue:%llu",czret,pArea->czCODE->CodeValue);

			if ( czret == 0 ) 
			{
				return TRUE;			// success      
			} // if
		}// if
	}
	return FALSE; 
}

/**
 * \brief ���� ���Ǻ� �Լ�E
 *
 * ���� �ȼ����� �Ǻ� ���� ���� ���� ������ ���� ���� ���Ѵ�.
 *
 * \param point : ���Ǻ��� �ȼ���E
 * \param env_id : ���Ǻ� Ƚ��E
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 * - ����E: 0 (GREEN)
 *          1 (BLACK)
 *          2 (RED)
 *          4 (BULE)
 * - ���� : -1
 */
char Color_Vote_RGB(czLPCOLOR point, short env_id, czLPDecoderArea pArea)   // decide each color cell by RGB color model
{
	
	short	max = 0;
	short	nVote[NCOLORS];
	short	cwhite = 0;
	char	i;
	char	color;
	char	pcolor;	
	
	for (i=0; i < 4 ; i++)
		nVote[i] = 0;
	
	for (i=0; i < pArea->nNumPoints; i++)   {			// voted pixel color classification
		pcolor = Color_Convert_RGB(&point[i],env_id,pArea);
		if (pcolor < 4)	
			nVote[pcolor]++;
		else 
			cwhite++;
	}
	
	max = nVote[0];
	color = 0;
	
	for (i=1; i < 4; i++)
	{
		if (max < nVote[i])
		{
			max = nVote[i];
			color = i;		// 0 :Green, 1 : Black,  2 : Red,  3: Blue
		}
	}
	if (max <= cwhite)
		return (char)-1;			// can't decide color
	else 
		return color;		
}

/**
 * \brief �ȼ��� ���Ǻ� �Լ�E
 *
 * \param point : ���Ǻ��� �ȼ���E
 * \param env_id : ���Ǻ� Ƚ��E
 * \param pArea : ���ڵ�E�ϱ�E���� �⺻ ������ ������E�ٴϴ� ����ü
 *
 * \return
 * - 0 (COLOR_G)
 * - 1 (COLOR_K)
 * - 2 (COLOR_R)
 * - 3 (COLOR_B)
 * - 4 (COLOR_W)
 */
char Color_Convert_RGB(czLPCOLOR point, short env_id,czLPDecoderArea pArea)
{// RGB, brightness, Saturation, Variation, (black, white)	
	short	nR = point->R;
	short	nG = point->G;
	short	nB = point->B;
	short	maxC, minC, midC;
	short	gap;
	
    maxC = MAX3(nR,nG,nB);
	minC = MIN3(nR,nG,nB);
	if (maxC == nR) {
		midC = MAX(nG,nB);
	} else if (maxC == nG)
		midC = MAX(nR,nB);
	else
		midC = MAX(nR,nG);
	
	gap = maxC - minC;



	if (maxC <= (pArea->ThresholdValue*2/3))// �湁E�Ǻ� ����E2006.05.02
		return COLOR_K;		// Black
//	if (maxC <= pArea->envParam[env_id])					// for differenciate red and black
//		return COLOR_K;		// Black

//	if ((gap < 20) && ((nR/3+nG/3+nB/3)>95)) {				// SKTT P1 test
	//	return COLOR_B;		// Black
//	}


	if ((maxC < pArea->ThresholdValue) && (gap < 20)) {				// black
		return COLOR_K;		// Black
	}

//	if ((maxC < pArea->ThresholdValue) && (gap <= maxC * maxC/1020)) {	// 4 * MAXPVAL = 1020
//		return COLOR_K;		
//	}
	else if (maxC == nB)  {
		if (maxC - midC > C_B_INTERVAL)
			return COLOR_B;		// Blue
		else
			return COLOR_W;		// invalid
	}
	else if (maxC == nG)  {
		if (maxC - midC > C_G_INTERVAL)
			return COLOR_G;		// Green 
		else
			return COLOR_W;		// invalid
	}
	else  {//maxC == nR
		if (maxC - midC > C_R_INTERVAL)
			return COLOR_R;		// Red
		else
			return COLOR_W;		// invalid
	}
	
}
