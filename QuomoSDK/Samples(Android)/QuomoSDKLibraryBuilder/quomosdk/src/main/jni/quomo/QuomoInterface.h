//
// Created by 齋藤浩幸 on 16/06/01.
//
#include <opencv2/opencv.hpp>
#include "quomo/quomoEncode.h"
#include <map>
#include <time.h>
#include <iostream>
#include <typeinfo>
#include <string>
#include <stdlib.h>
#include <android/log.h>
#include "quomo/colorcode.h"
#include "quomo/czdecoder.h"
#include        <openssl/evp.h>
#include        <openssl/aes.h>

#define COLOR_STRONG 255
#define COLOR_WEAK 0

#define SECTION_PRODUCTID "productid"
#define SECTION_EXPIRE "expire"
#define SECTION_TYPE "type"
#define SECTION_LIMIT "limit"

enum LICENSE_TYPE {
    LICENSE_TYPE_TRIAL = 0,
    LICENSE_TYPE_NORMAL = 1,
    LICENSE_TYPE_SMALL = 2,
    LICENSE_TYPE_MEDIUM = 3,
    LICENSE_TYPE_LARGE = 4
};
#define TRIAL_LIMIT 5

using namespace std;

#ifndef QUOMO_INTERFACE_H
#define QUOMO_INTERFACE_H
typedef struct STRUCT_QUOMOCODE{
    unsigned long long identifier;//企業コード
    unsigned long long code;
    unsigned long long display;
};
typedef struct _QUOMOCOLORS{
    int r;
    int g;
    int b;
}QUOMOCOLORS;

enum ENUM_DISPLAYINDEX{
    DISPLAYINDEX_CODE00000 = 0,
    DISPLAYINDEX_CODE = 1,
    DISPLAYINDEX_DUMMY = 2
};
class QuomoInterface{
public:
    QuomoInterface();
    ~QuomoInterface();

    int _width;
    int _height;
    unsigned int _block_x;
    unsigned int _block_y;
    unsigned int _block_frame_thickness;
    float  _transParentBuffer[12*12];
    int _nowCodeIndex;
    int _nowCodeCount;
    unsigned long long _productCode;
    unsigned long long int _primaryCode;
    unsigned long long int _secondaryCode;
    unsigned long long int _dummyCode;

    map<int, STRUCT_QUOMOCODE> _codes;  //企業コード , コード
    map<unsigned int, unsigned long long> _readCode;
    map<unsigned int, unsigned long long> _decodedReadCode;

    unsigned long long int _readDate;
    unsigned int _readCount;
    unsigned int _key;
    //

    //
    void setProductID(unsigned long long identifier);
    unsigned long long getProductID(void);
    int addCode(unsigned long long code);
    unsigned long long getCode(int index);
    unsigned long long getDisplayCode(int index);

    void nextCode();
    void makeQuomoImage(cv::Mat &matImage );
    void makeQuomoColors(QUOMOCOLORS *color) ;
        unsigned long long decoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo);
    unsigned int  getDecodeCodeCount();
    unsigned long long int getDecodeCode(unsigned int index);

    bool SetProductKey(char *productKey);

private:
    //properties
    bool _enabled;//証明書が有効期限内の場合true
    int _licenseType;//ライセンスタイプ
    unsigned long long int _licenseMaxCount;//ライセンスの最大数

    //private function
    long long makeCode100000(unsigned int count );
    long long makeCode(unsigned long long identifier,unsigned long long code ) ;
    void decode100000(unsigned long long code,unsigned long long *dstDate,unsigned int *dstCount ) ;
    char* decodeProductKey(char *productKey);
    vector<string> split(const string &s, char delim) ;


};

#endif //QUOMO_INTERFACE_H
