/**
 * \brief  header file for bbd_kmeans.c
 *
 *
 *
 */
 
#ifndef _BBD_RGB_H_INCLUDED
#define _BBD_RGB_H_INCLUDED


// Macro Definitions
//
#define		C_B_INTERVAL		6
#define		C_G_INTERVAL		6
#define		C_R_INTERVAL		10


// Function declarations
//
extern	int 	ColorZip_Color_CellClassifier_RGB(czLPDecoderArea pArea);
extern	char ColorZip_Color_Vote_RGB(czLPCOLOR point, short env_id, czLPDecoderArea pArea);
extern	char ColorZip_Color_Convert_RGB(czLPCOLOR point, short env_id,czLPDecoderArea pArea);

//Type definitions
//


#endif /*  _BBD_RGB_H_INCLUDED */
