#include "colorcode.h"
#include "cell_detect.h"
//#include "utils.h"
#include "bbd.h"


unsigned char ColorZip_CellDetector(czLPDecoderArea pArea);
int 	ColorZip_Ratio(short x1, short y1, short x2, short y2);
int 	ColorZip_Intercept(short x1, short y1, short x2, short y2);


unsigned char ColorZip_CellDetector(czLPDecoderArea pArea)
{
	short index = 0; 
	long Sqr_len_ratio;
	long fail_ratio = 7744;   // fail_ratio = 0.88f*0.88f*VALID_DIGIT = 0.7744*VALID_DIGIT;
	short i, j;
	long Sqr_length_w;   
	long Sqr_length_h;
	long Sqr_length_w1;
	long Sqr_length_h1;	
	long Sqr_length_max;
	long Sqr_length_min;
	long a[4]={0,0,0,0}, b[4]={0,0,0,0};  // Y= aX + b  up, down, left, right
	czPOSITION Line_XY[4]={{0,0},{0,0},{0,0},{0,0}}; //{y,x} on the each line - up, down, left, right
	long rate_vertical=0, inter_vertical=0, rate_horizon=0, inter_horizon=0;  // cross lines
	long x=0, y=0;  // cross point (x,y) 

/*
 int Sqr_len_ratio;
 int fail_ratio = 7744;   // fail_ratio = 0.88f*0.88f*VALID_DIGIT = 0.7744*VALID_DIGIT;
 short i, j;
 int Sqr_length_w;   
 int Sqr_length_h;
 int Sqr_length_w1;
 int Sqr_length_h1;	
 int Sqr_length_max;
 int Sqr_length_min;
 int a[4]={0,0,0,0}, b[4]={0,0,0,0};  // Y= aX + b  up, down, left, right
 czPOSITION Line_XY[4]={{0,0},{0,0},{0,0},{0,0}}; //{y,x} on the each line - up, down, left, right
 int rate_vertical=0, inter_vertical=0, rate_horizon=0, inter_horizon=0;  // cross lines
 short x=0, y=0;  // cross point (x,y) 

 */
	//quantization treatment
	
	short width_up = pArea->m_nCrop[1].x-pArea->m_nCrop[0].x; // upper line length of tag
	short width_down = pArea->m_nCrop[3].x-pArea->m_nCrop[2].x; // bottom line length of tag
	short height_left = pArea->m_nCrop[2].y-pArea->m_nCrop[0].y; // left line length of tag
	short height_right = pArea->m_nCrop[3].y-pArea->m_nCrop[1].y; // right line length of tag
	
	pArea->m_nCrop[0].x++; pArea->m_nCrop[0].y++;
	pArea->m_nCrop[1].x--; pArea->m_nCrop[1].y++;
	pArea->m_nCrop[2].x++; pArea->m_nCrop[2].y--;
	pArea->m_nCrop[3].x--; pArea->m_nCrop[3].y--;

	if (ColorZip_is_allowed_size(pArea->m_nCrop)==0) return 1;  //�ڵ�E�̹����� ��Eũ�� E�E�ּ���E
	
	// check length
    Sqr_length_w = (pArea->m_nCrop[1].x-pArea->m_nCrop[0].x)*(pArea->m_nCrop[1].x-pArea->m_nCrop[0].x)+(pArea->m_nCrop[1].y-pArea->m_nCrop[0].y)*(pArea->m_nCrop[1].y-pArea->m_nCrop[0].y);   
	Sqr_length_h = (pArea->m_nCrop[2].x-pArea->m_nCrop[0].x)*(pArea->m_nCrop[2].x-pArea->m_nCrop[0].x)+(pArea->m_nCrop[2].y-pArea->m_nCrop[0].y)*(pArea->m_nCrop[2].y-pArea->m_nCrop[0].y);
	Sqr_length_w1 = (pArea->m_nCrop[2].x-pArea->m_nCrop[3].x)*(pArea->m_nCrop[2].x-pArea->m_nCrop[3].x)+(pArea->m_nCrop[2].y-pArea->m_nCrop[3].y)*(pArea->m_nCrop[2].y-pArea->m_nCrop[3].y);
	Sqr_length_h1 = (pArea->m_nCrop[1].x-pArea->m_nCrop[3].x)*(pArea->m_nCrop[1].x-pArea->m_nCrop[3].x)+(pArea->m_nCrop[1].y-pArea->m_nCrop[3].y)*(pArea->m_nCrop[1].y-pArea->m_nCrop[3].y);	

	if (Sqr_length_w < Sqr_length_w1) 
	  	Sqr_len_ratio = Sqr_length_w*VALID_DIGIT / Sqr_length_w1;
	else
	 	Sqr_len_ratio = Sqr_length_w1*VALID_DIGIT / Sqr_length_w;

	if (MAX(Sqr_length_w,Sqr_length_w1) > 50*50)  // MAX LENGTH > 50
		fail_ratio -= 16;   // 0.04f*0,04f*VALID_DIGIT

	if (Sqr_len_ratio < fail_ratio)
		return 2;     
	
	if (Sqr_length_h < Sqr_length_h1) 
		Sqr_len_ratio = Sqr_length_h*VALID_DIGIT / Sqr_length_h1;
	else
		Sqr_len_ratio = Sqr_length_h1*VALID_DIGIT / Sqr_length_h;

	fail_ratio = 7744;

	if (MAX(Sqr_length_h, Sqr_length_h1) > 50*50)
		fail_ratio -= 16;

	if (Sqr_len_ratio < fail_ratio)
		return 3;   //

//	 2005. 1.20 - Code Area checker
		
	Sqr_length_max = MAX(MAX(Sqr_length_w,Sqr_length_w1), MAX(Sqr_length_h,Sqr_length_h1));   
	Sqr_length_min = MIN(MIN(Sqr_length_w,Sqr_length_w1), MIN(Sqr_length_h,Sqr_length_h1));
    if (Sqr_length_max*VALID_DIGIT/Sqr_length_min > 2*VALID_DIGIT) return 4; // undo for 6x6 2015-03-03 ... CZJ A.I. 2012Auly20
	//	pArea->nCol = 5; 
	//	pArea->nRow = 5;
	pArea->nMatrixSize = pArea->nCol*pArea->nRow;
/*	if ( cellDimensions == FIVE_BY_FIVE) //5x5
	{
		if (!( nCol == 5 && nRow ==5 ))
		{
			return FALSE;
		}
		
	}
	else if ( cellDimensions == FOUR_BY_SIX) //4x6
	{
		if ( !( nCol == 4 && nRow ==6 ) && !( nCol == 6 && nRow ==4 ) )
		{
			return FALSE;
		}
	}*/
	//
	//	nCol = nCol;
	//	nRow = nRow;
	//                Line_XY[0]
	//             ------*------- 
	//             |   (y,x)    |
	//  Line_XY[2] *            * Line_XY[3]
	//             |            |
	//             ------*------- 
	//              Line_XY[1]    
	
	
	
	a[0] = ColorZip_Ratio(pArea->m_nCrop[0].x,pArea->m_nCrop[0].y,pArea->m_nCrop[1].x,pArea->m_nCrop[1].y);  
	b[0] = ColorZip_Intercept(pArea->m_nCrop[0].x,pArea->m_nCrop[0].y,pArea->m_nCrop[1].x,pArea->m_nCrop[1].y);  
	
	a[1] = ColorZip_Ratio(pArea->m_nCrop[2].x,pArea->m_nCrop[2].y,pArea->m_nCrop[3].x,pArea->m_nCrop[3].y);     
	b[1] = ColorZip_Intercept(pArea->m_nCrop[2].x,pArea->m_nCrop[2].y,pArea->m_nCrop[3].x,pArea->m_nCrop[3].y); 
	
	//must check overflow
	if (pArea->m_nCrop[0].x != pArea->m_nCrop[2].x) {   // line left is infinite ratio
		a[2] = ColorZip_Ratio(pArea->m_nCrop[0].x,pArea->m_nCrop[0].y,pArea->m_nCrop[2].x,pArea->m_nCrop[2].y); 
		b[2] = ColorZip_Intercept(pArea->m_nCrop[0].x,pArea->m_nCrop[0].y,pArea->m_nCrop[2].x,pArea->m_nCrop[2].y);
	}
	
	if (pArea->m_nCrop[3].x != pArea->m_nCrop[1].x) {
		a[3] = ColorZip_Ratio(pArea->m_nCrop[1].x,pArea->m_nCrop[1].y,pArea->m_nCrop[3].x,pArea->m_nCrop[3].y); 
		b[3] = ColorZip_Intercept(pArea->m_nCrop[1].x,pArea->m_nCrop[1].y,pArea->m_nCrop[3].x,pArea->m_nCrop[3].y);
	}	
	

	for (j = 0; j < pArea->nRow; j++) {
		Line_XY[2].y = pArea->m_nCrop[0].y + (short)((( (2*j+1)*height_left/(2*pArea->nRow) )*2+1)/2);  //Line_XY[2].y = pArea->m_nCrop[0].y + (short)((2.0*j+1.0)*height_left/(2.0*nRow)+0.5);  // center of y in each cell    
																						// center of y in each cell    
		Line_XY[3].y = pArea->m_nCrop[1].y + (short)((( (2*j+1)*height_right/(2*pArea->nRow) )*2+1)/2); //Line_XY[3].y = pArea->m_nCrop[1].y + (short)(( (2*j+1)*height_right/(2*nRow)+0.5);  
																						// center of y in each cell  
		
		if ((pArea->m_nCrop[0].x==pArea->m_nCrop[2].x)||(ABS(a[2]/VALID_DIGIT)>1000)) 
			Line_XY[2].x = pArea->m_nCrop[0].x;  // ratio is infinites
		else 
			Line_XY[2].x = (short)(((Line_XY[2].y*VALID_DIGIT-b[2])/a[2]*2+1)/2);

		if ((pArea->m_nCrop[1].x==pArea->m_nCrop[3].x)||(ABS(a[3]/VALID_DIGIT)>1000)) 
			Line_XY[3].x = pArea->m_nCrop[1].x;  // ratio is infinites
		else 
			Line_XY[3].x = (short)(((Line_XY[3].y*VALID_DIGIT-b[3])/a[3]*2+1)/2);
		
		for (i = 0; i< pArea->nCol; i++) {
			Line_XY[0].x = pArea->m_nCrop[0].x + (short)(((2*i+1)*width_up/(2*pArea->nCol)*2+1)/2);  // center of X in each cell    
			Line_XY[1].x = pArea->m_nCrop[2].x + (short)(((2*i+1)*width_down/(2*pArea->nCol)*2+1)/2);  // center of X in each cell    
			Line_XY[0].y = (short)(((Line_XY[0].x*a[0]+b[0])*2+1)/2/VALID_DIGIT);       // y position of up
			Line_XY[1].y = (short)(((Line_XY[1].x*a[1]+b[1])*2+1)/2/VALID_DIGIT);       // y position of down
			
			
			if (Line_XY[0].x==Line_XY[1].x) x = Line_XY[0].x;
			else {
				rate_vertical = ColorZip_Ratio(Line_XY[0].x,Line_XY[0].y,Line_XY[1].x,Line_XY[1].y);
				inter_vertical = ColorZip_Intercept(Line_XY[0].x,Line_XY[0].y,Line_XY[1].x,Line_XY[1].y);
			}
			
			rate_horizon = ColorZip_Ratio(Line_XY[2].x,Line_XY[2].y,Line_XY[3].x,Line_XY[3].y);
			inter_horizon = ColorZip_Intercept(Line_XY[2].x,Line_XY[2].y,Line_XY[3].x,Line_XY[3].y);
			
			if (Line_XY[0].x!=Line_XY[1].x) 
				x = (short)((inter_horizon-inter_vertical)/(rate_vertical-rate_horizon));
			y = (short)((rate_horizon*x+inter_horizon)/VALID_DIGIT);
			
			if (x<3) x=3;
			if (y<3) y=3;
			if (x>pArea->CapturedImageWidth-3) x=pArea->CapturedImageWidth-3;
			if (y>pArea->CapturedImageHeight-3) y=pArea->CapturedImageHeight-3;
			
			pArea->m_nCellpos[index].y = y;
			pArea->m_nCellpos[index].x = x;
			index++;
		}
	}
	return 0;
}

int ColorZip_Ratio(short x1, short y1, short x2, short y2)
{
	if (x1==x2) {
		return(0);
	}
	else return( (int) ((y2-y1)*VALID_DIGIT/(x2-x1)) );       //a = (y2-y1)/(x2-x1)  // return 1000*a
}


int ColorZip_Intercept(short x1, short y1, short x2, short y2)
{
	if (x1==x2) {
		return(0);
	}
	else {
		int a = (int) ((y2-y1)*VALID_DIGIT/(x2-x1));        // b = y1-ax1  
		return( (int) (y1*VALID_DIGIT - (a*x1)) );                                  // return 1000*b  
	}
}
