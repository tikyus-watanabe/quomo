/**
 * \brief    codearea_detect.c
 *
 *
 *
 */

#include "colorcode.h"
#include "codearea_detect.h"
//#include "utils.h"
#include "bbd.h"


void ColorZip_CalculateSides(czLPDecoderArea pArea);
void ColorZip_CodeAreaDetector(czLPDecoderArea pArea);

short   ColorZip_CodeImageFinder(czLPDecoderArea pArea);
void 	ColorZip_VerticesEdgeTracer(VertexInfo* info, czPOSITION origin, short brightness, czLPDecoderArea pArea);
int 	ColorZip_Adv_Turtle(trace_Edge_Pt *Current,trace_Edge_Pt *Next, int *prev_diagonal, short brightness, czLPDecoderArea pArea);  // Advanced Turtle
void 	ColorZip_Move(trace_Edge_Pt *Current, trace_Edge_Pt Next, enum Direction Next_direction);   // current = next
int 	ColorZip_Detect_pixel(trace_Edge_Pt Current, enum RelativeDirection target_direction, trace_Edge_Pt *Next, short brightness, czLPDecoderArea pArea);         // determine white or black
void 	ColorZip_Target_Location(int x, int y, int direction, trace_Edge_Pt *Next);

void 	ColorZip_VerticeDetector(short brightness, czPOSITION coordp[],unsigned char codetype, czLPDecoderArea pArea); // detect vertices and save to m_nCrop[][]
int 	ColorZip_VerticesSelector(czPOSITION *vertices_nonedge,czPOSITION *vertices_edge); // detect adaptive Vertices 

void 	ColorZip_GetRoughBoundaryRect(czLPDecoderArea pArea); // Finding Tag boundary rectagle 

static enum Direction DIRECTIONS[] = 
	{EAST, NORTHEAST, NORTH, NORTHWEST, WEST, SOUTHWEST, SOUTH, SOUTHEAST};

void ColorZip_CalculateSides(czLPDecoderArea pArea) {

	short max_x=pArea->m_nCrop[0].x, min_x=pArea->m_nCrop[0].x, max_y=pArea->m_nCrop[0].y, min_y=pArea->m_nCrop[0].y;
	short i;

	for (i=1; i<4 ; i++) 	{             // sort detected point - height
		if (min_x > pArea->m_nCrop[i].x) min_x = pArea->m_nCrop[i].x;
		else if (max_x < pArea->m_nCrop[i].x) max_x = pArea->m_nCrop[i].x;
		if (min_y > pArea->m_nCrop[i].y) min_y = pArea->m_nCrop[i].y;
		else if (max_y < pArea->m_nCrop[i].y) max_y = pArea->m_nCrop[i].y;
	}

	pArea->nLeft = min_x;                // left 
	pArea->nRight = max_x;				// right
	pArea->nTop = min_y;					// top
	pArea->nBottom = max_y;	            // bottom
}



void ColorZip_CodeAreaDetector(czLPDecoderArea pArea)
{
	short	i, j; 
	short	gray;
	short	serial_gray = 0;         // number of serial white pixels
	unsigned char tag_cap = FALSE;
	
	// Initialized top-left, top-right, bottom-left, bottom-right
	for (i=0; i<4; i++) {
		pArea->m_nCrop[i].y = 0;
		pArea->m_nCrop[i].x = 0;
	}
	
	// check wrong code area
	if (pArea->nTop < 0) pArea->nTop = 0;
	if (pArea->nBottom >= pArea->BinaryImageHeight) pArea->nBottom = pArea->BinaryImageHeight-1 ;
	if (pArea->nLeft < 0) pArea->nLeft = 0;
	if (pArea->nRight >= pArea->BinaryImageWidth) pArea->nRight = pArea->BinaryImageWidth-1 ;
	
	for (j=pArea->nTop; j<=pArea->nBottom; j++) {  // detect cell area - left to right
		for (i=pArea->nLeft; i<=pArea->nRight; i++) 	{
			if (ColorZip_GetBImagePTR(j,i) == MINPVAL) {
				if (serial_gray == 0) {
					pArea->m_nCrop[0].y = j; 
					pArea->m_nCrop[0].x = i;
				}
				serial_gray++;           // countinuous white pixels
				
				if (serial_gray == 5) {
					tag_cap = TRUE;
					break;
				}
			}
		}	
		if (tag_cap) break;
	}
	
	gray = 0;
	serial_gray = 0;
	tag_cap = FALSE;
	
	for (j=pArea->nBottom; j>=pArea->nTop; j--) { // detect cell area - left to right in bottom
		for (i=pArea->nLeft; i<=pArea->nRight ; i++) {
			if (ColorZip_GetBImagePTR(j,i) == MINPVAL) {
				if (serial_gray == 0) {
					pArea->m_nCrop[1].y = j;     
					pArea->m_nCrop[1].x = i;
				}
				serial_gray++;           // countinuous white pixels
				
				if (serial_gray == 5) {
					tag_cap = TRUE;
					break;
				}
			}
		}
		
		if (tag_cap) break;
	}
	
	gray = 0;
	serial_gray = 0;
	tag_cap = FALSE;
	
	for (i=pArea->nLeft; i<=pArea->nRight; i++)	{ // detect cell area - top to down in left top
		for (j=pArea->nTop; j<=pArea->nBottom; j++) {
			if (ColorZip_GetBImagePTR(j,i) == MINPVAL) {
				if (serial_gray == 0){
					pArea->m_nCrop[2].y = j;     
					pArea->m_nCrop[2].x = i;
				}
				serial_gray++;           // countinuous white pixels
				
				if (serial_gray == 5) {
					tag_cap = TRUE;
					break;
				}
			}
		}
		
		if (tag_cap) break;
	}
	
	gray = 0;
	serial_gray = 0;
    tag_cap = FALSE;
	
	for (i=pArea->nRight; i>=pArea->nLeft; i--) {	// detect cell area - top to down in right top area
		for (j=pArea->nTop; j<=pArea->nBottom ; j++)	{	
			if (ColorZip_GetBImagePTR(j,i) == MINPVAL) {
				if (serial_gray == 0){
					pArea->m_nCrop[3].y = j;     
					pArea->m_nCrop[3].x = i;
				}
				serial_gray++;           // countinuous white pixels
				
				if (serial_gray == 5) {
					tag_cap = TRUE;
					break;
				}
			}
		}
		
		if (tag_cap) break;
	}	
}




 short ColorZip_CodeImageFinder(czLPDecoderArea pArea)
{
	short j,i;
	short max = 0;
	short tag_id = 0;
	VertexInfo info[4];
	for (i=0; i<4; i++) {
		info[i].size = 0;
		info[i].line_size = 0;
		info[i].sides.left=0;
		info[i].sides.right=0;
		info[i].sides.top=0;
		info[i].sides.bottom=0;
	}

	
	j = pArea->m_nCrop[0].y;
	i = pArea->m_nCrop[0].x;
	ColorZip_VerticesEdgeTracer(&info[0], pArea->m_nCrop[0], 50, pArea);         // start position, direction, coloring
 	 	 		
	j = pArea->m_nCrop[1].y;
	i = pArea->m_nCrop[1].x;
	if (ColorZip_GetBImagePTR(j,i) == MINPVAL){ 
		ColorZip_VerticesEdgeTracer(&info[1], pArea->m_nCrop[1], 100, pArea);
	}

	j = pArea->m_nCrop[2].y;
	i = pArea->m_nCrop[2].x;
	if (ColorZip_GetBImagePTR(j,i) == MINPVAL){
		ColorZip_VerticesEdgeTracer(&info[2], pArea->m_nCrop[2], 150, pArea);
	}

	j = pArea->m_nCrop[3].y;
	i = pArea->m_nCrop[3].x;
	if (ColorZip_GetBImagePTR(j,i) == MINPVAL){
		ColorZip_VerticesEdgeTracer(&info[3], pArea->m_nCrop[3], 200, pArea);
	}

	
	for (i=0; i<4; i++){
		if (max < info[i].line_size) {
			max = info[i].line_size;
			tag_id = i;
		}
	}

	pArea->nLeft = info[tag_id].sides.left;             // rearranged codearea with edge tracing
	pArea->nRight = info[tag_id].sides.right;
	pArea->nTop = info[tag_id].sides.top;
	pArea->nBottom = info[tag_id].sides.bottom;

	return info[tag_id].brightness;
}


 void ColorZip_VerticesEdgeTracer(VertexInfo* info, czPOSITION origin, short brightness, czLPDecoderArea pArea)
{
	// Verticesss edge tracing from origin and coloring cycle path with brightness

	trace_Edge_Pt Start, Current, Next={0,0,EAST};
	int prev_diagonal = FALSE;
	
	int tag_closed = FALSE ;   // detect whether closed or not
	czPOSITION min_pt, max_pt;                                     
	info->brightness = brightness;
	info->size = -1;
		
	Start.x = origin.x;   Start.y = origin.y;  // set start point by origin point
	Start.direction = EAST;

	Current.x = Start.x; Current.y = Start.y; 

	min_pt = origin;                    // checking for code image size
	max_pt = origin;

	// initialized direction at start Pt
	// atripp: added boundary checks to void core dump. May 13, 2005
	if (origin.y > 0 && ColorZip_GetBImagePTR(origin.y-1,origin.x) == MAXPVAL) Start.direction = EAST;
	else if (origin.x < pArea->BinaryImageWidth && ColorZip_GetBImagePTR(origin.y,origin.x+1) == MAXPVAL) Start.direction = SOUTH;
	else if (origin.y < pArea->BinaryImageHeight && ColorZip_GetBImagePTR(origin.y+1,origin.x) == MAXPVAL) Start.direction = WEST;
	else if (origin.x > 0 && ColorZip_GetBImagePTR(origin.y,origin.x-1) == MAXPVAL) Start.direction = NORTH;


	Current.direction = Start.direction;
	
	if ((origin.x <= 0) || (origin.x >= pArea->BinaryImageWidth) || (origin.y <= 0) 
	   || (origin.y >= pArea->BinaryImageHeight)) {
		//fprintf(stderr, "ColorZip_VerticesEdgeTracer: invalid origin\n");
		return;
	}
 
	while(1){          // tracing edge while not arrived at start point
		if(!ColorZip_Adv_Turtle(&Current, &Next, &prev_diagonal, brightness, pArea)) break;

		if (ColorZip_Detect_pixel(Current, HERE, &Next, brightness, pArea)) {
			if (Current.x < min_pt.x) min_pt.x = Current.x;
			if (Current.x > max_pt.x) max_pt.x = Current.x;
			if (Current.y < min_pt.y) min_pt.y = Current.y;
			if (Current.y > max_pt.y) max_pt.y = Current.y;
			
			//j = Current.y; i = Current.x;  
			ColorZip_SetBImagePTR(Current.y, Current.x, (unsigned char)brightness);  // coloring by brightness
		}

		Current = Next;   // move to next

		info->size++;

		if ((Current.x == Start.x)&&(Current.y == Start.y)&&(Current.direction == Start.direction)) { 
			// end condition                
			tag_closed = TRUE;              
			break;
		}

		if (info->size > 1000){
			info->size = 0;
			tag_closed = FALSE;
			break;           // trace count over
		}

	} // while


	if ((((max_pt.x - min_pt.x) < 20)||((max_pt.y - min_pt.y) < 20)||(info->size < 5)) && tag_closed){
		tag_closed = FALSE;               // return fault when code area size < 20 or pixels < 5)
		return;
	}
	else         // color marking   
	{
		if ((min_pt.x > pArea->nLeft)&&(min_pt.x > 0)) 
			info->sides.left = min_pt.x;
		else 
			info->sides.left = pArea->nLeft;
		if ((max_pt.x < pArea->nRight)&&(max_pt.x < pArea->BinaryImageWidth)) 
			info->sides.right = max_pt.x;
		else 
			info->sides.right = pArea->nRight;
		if ((min_pt.y > pArea->nTop)&&(min_pt.y > 0)) 
			info->sides.top = min_pt.y;
		else info->sides.top = pArea->nTop;
		if ((max_pt.y < pArea->nBottom)&&(max_pt.y < pArea->BinaryImageHeight)) 
			info->sides.bottom = max_pt.y;
		else 
			info->sides.bottom = pArea->nBottom;
	}

	if (ABS((info->sides.right - info->sides.left) - (info->sides.bottom - info->sides.top)) < 15)    // 2006.10.26   터틀에서  사각�갋이미햨E중 큰거 찾콅E
	{
		if (ABS(info->sides.right - info->sides.left) > ABS(info->sides.bottom - info->sides.top))
		{
			info->line_size = ABS(info->sides.right - info->sides.left);
		}else{
			info->line_size = ABS(info->sides.bottom - info->sides.top);
		}
	}

}

int ColorZip_Adv_Turtle(trace_Edge_Pt *Current,trace_Edge_Pt *Next, int *prev_diagonal, short brightness, czLPDecoderArea pArea)  // Advanced Turtle
{
	trace_Edge_Pt TMP;

	enum Direction direction = Current->direction;
	if (ColorZip_Detect_pixel(*Current, HERE, Next, brightness, pArea)) {                          // Current : black 

		if (ColorZip_Detect_pixel(*Current, LEFT, Next, brightness, pArea)) *prev_diagonal = FALSE;	// Left : black 
																// Save next pt & direction : LEFT
		else                                                    // Left : White  
		{
			if ((!(*prev_diagonal)) && (!ColorZip_Detect_pixel(*Current, LOWER, Next, brightness, pArea)) && (ColorZip_Detect_pixel(*Current, LOWER_LEFT, Next, brightness, pArea))) {  
				// Diagonal
				// detect & save to NeXT pos(TMP) & color
				Next->direction = turn(LOWER, direction);	
				*prev_diagonal = TRUE; 
			}
			else
			{ 
				*prev_diagonal = FALSE;								// not diagonal    

				if (ColorZip_Detect_pixel(*Current, HIGHER_LEFT, Next, brightness, pArea)) {     // HL is black
					ColorZip_Move(&TMP, *Next, Current->direction);                // tmp position
					if (ColorZip_Detect_pixel(*Current, HIGHER, Next, brightness, pArea)) {
	//					BImagePTR[Next.y][Next.x] = brightness;         // Mark  but skip
					}
					ColorZip_Move(Next, TMP, Current->direction);
				}
				else if (ColorZip_Detect_pixel(*Current, HIGHER, Next, brightness, pArea)) {         // Higher
					Next->direction = turn(RIGHT,direction);
				}
				else {
					ColorZip_Move(Next, *Current, Current->direction);
					Next->direction = turn(LOWER,direction);
				} // else 
			} // else 
		}// else 
	} // if 
	else return FALSE ;   // white Pixel
	return TRUE;     
}

void ColorZip_Move(trace_Edge_Pt *Current, trace_Edge_Pt Next, enum Direction Next_direction)   // current = next
{
	Current->x = Next.x;    // copy x, y
	Current->y = Next.y;    // copy x, y
	Current->direction = Next_direction;
}

int ColorZip_Detect_pixel(trace_Edge_Pt Current, enum RelativeDirection target_direction, trace_Edge_Pt *Next, short brightness, czLPDecoderArea pArea)
{         // determine white or black

	if (target_direction == HERE){
		if ( ColorZip_GetBImagePTR(Current.y, Current.x)>brightness ){
			return FALSE;   // white 0
		}else return TRUE;                                    // black 1
	}
	else {                                                   
		Next->direction = turn(target_direction, Current.direction);   // setting new direction
		ColorZip_Target_Location(Current.x, Current.y, Next->direction, Next); // location & svae next inf.

		if( ColorZip_GetBImagePTR(Next->y, Next->x)>brightness ) return FALSE;   // white
		else return TRUE;   		// black
	}
}



void ColorZip_Target_Location(int x, int y, int direction, trace_Edge_Pt *Next)
{ //count 2
	switch(direction){
		case EAST  		: Next->x = x+1; Next->y = y;   break;   // |-----|-----|-----|
		case NORTHEAST 	: Next->x = x+1; Next->y = y-1; break;   // | NW  |  N  |  NE |
		case NORTH  	: Next->x = x;   Next->y = y-1; break;   // |     |     |     |
		case NORTHWEST 	: Next->x = x-1; Next->y = y-1; break;   // |-----|-----|-----|
		case WEST  		: Next->x = x-1; Next->y = y;   break;   // |  W  |     |  E  |
		case SOUTHWEST 	: Next->x = x-1; Next->y = y+1; break;   // |     |     |     |
		case SOUTH  	: Next->x = x;   Next->y = y+1; break;   // |-----|-----|-----|
		case SOUTHEAST 	: Next->x = x+1; Next->y = y+1; break;   // | SW  |  S  |  SE |
		default : break;					           		   // |     |     |     |
	}												  		   // |-----|-----|-----|	
}



enum Direction turn(enum RelativeDirection next_direction, enum Direction current_direction)
{     // New Direction LEFT, RIGHT,...  // Current direction
	int index = 0;
	switch(next_direction){                              // count 1 
		case HIGHER: 
			index = current_direction; 
			break;
		case HIGHER_LEFT: 
			index = (current_direction + 1)%8 ; 
			break;
		case LEFT:  
			index = (current_direction + 2)%8 ; 
			break;
		case LOWER_LEFT:  
			index = (current_direction + 3)%8 ; 
			break;
		case LOWER:  
			index = (current_direction + 4)%8 ; 
			break;
		case LOWER_RIGHT:  
			index =  (current_direction + 5)%8 ; 
			break;
		case RIGHT:  
			index = (current_direction + 6)%8 ; 
			break;
		case HIGHER_RIGHT:  
			index = (current_direction + 7)%8 ; 
			break;
		default : index = current_direction; 
			break;
	}
	return DIRECTIONS[index];
}


void ColorZip_VerticeDetector(short brightness, czPOSITION coordp[],unsigned char codetype, czLPDecoderArea pArea) // detect vertices and save to m_nCrop[][]
{                                           // codetype 0 : gray   1 : color
	short	i, j, k;		
	short	h = pArea->nBottom-pArea->nTop;
	short	w = pArea->nRight-pArea->nLeft;
	short	range_vertice;
	short	h_pos=0, w_pos=0, h_index=0, v_index=0;
//	short	comp_x1, comp_x2, comp_y1, comp_y2;
	unsigned char	tag0, tag1;
	czPOSITION    vertices_point;
//	short crt_x_length;
//	short crt_y_length;
	// 20060330   short -> int  short일경퓖Eoverflow 일엉毆
	int	comp_line_v1, comp_line_v2, comp_line_b1, comp_line_b2;
	czPOSITION    intervertex[4], intboundary[4];

	for (i=0;i<4 ;i++ )
	{
		intervertex[i].x=0;
		intervertex[i].y=0;
		intboundary[i].x=0;
		intboundary[i].y=0;
	}

//	printf("(%d)%d %d %d %d\n",brightness ,pArea->nLeft,pArea->nRight,pArea->nTop,pArea->nBottom);
	
	if (h < w) 
		range_vertice = h;
	else 
		range_vertice = w;
//	if (ColorZip_is_allowed_size(pArea->m_nCrop)==0) return FALSE;
	
	for (i=0; i<4; i++) {                     // initialize 4 vertices of rectangular
		coordp[i].y = 0;
		coordp[i].x = 0;
	}
	tag0 = tag1 = FALSE;
	vertices_point.x = vertices_point.y = 0;
	for (i=0; i<range_vertice; i++) {                   // Left - top point 
		for (j=0; j<=i; j++) {
			k = i - j;
		    h_pos = j+pArea->nTop;
			w_pos = k+pArea->nLeft;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				coordp[0].y = h_pos;
				coordp[0].x = w_pos;				
				tag0 = TRUE;
				break;			
			}
		}
		if (!tag0) continue;//break;

		for (j=0; j<=i; j++) {
			k = i - j;
		    h_pos = k+pArea->nTop;
			w_pos = j+pArea->nLeft;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				vertices_point.y = h_pos;
				vertices_point.x = w_pos;				
				tag1 = TRUE;				
				break;
			}
		}
		if(tag1) {
			intervertex[0].y = (short)((coordp[0].y + vertices_point.y)/2);
			intervertex[0].x = (short)((coordp[0].x + vertices_point.x)/2);
	
			break;
		}
	}
	
	tag0 = tag1 = FALSE;
	vertices_point.x = vertices_point.y = 0;
	for (i=0; i<range_vertice; i++) {                   // Left - top point 
		for (j=0; j<=i; j++) {
			k = i - j;
            h_pos = j+pArea->nTop;
			w_pos = pArea->nRight-k;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				coordp[1].y = h_pos;
				coordp[1].x = w_pos;				
				tag0 = TRUE;
				break;			
			}
		}
		if (!tag0) continue;//break;

		for (j=0; j<=i; j++) {
			k = i - j;
		  h_pos = k+pArea->nTop;
			w_pos = pArea->nRight-j;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				vertices_point.y = h_pos;
				vertices_point.x = w_pos;				
				tag1 = TRUE;				
				break;
			}
		}
		if(tag1) {
			intervertex[1].y = (short)((coordp[1].y + vertices_point.y)/2);
			intervertex[1].x = (short)((coordp[1].x + vertices_point.x)/2);
	
			break;
		}
	}
	
	tag0 = tag1 = FALSE;
	vertices_point.x = vertices_point.y = 0;
	for (i=0; i<range_vertice; i++) {                   // Left - top point 
		for (j=0; j<=i; j++) {
			k = i - j;
            h_pos = pArea->nBottom-j;
			w_pos = k+pArea->nLeft;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				coordp[2].y = h_pos;
				coordp[2].x = w_pos;				
				tag0 = TRUE;
				break;			
			}
		}
		if (!tag0) continue;//break;

		for (j=0; j<=i; j++) {
			k = i - j;
		    h_pos = pArea->nBottom-k;
			w_pos = j+pArea->nLeft;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				vertices_point.y = h_pos;
				vertices_point.x = w_pos;				
				tag1 = TRUE;				
				break;
			}
		}
		if(tag1) {
			intervertex[2].y = (short)((coordp[2].y + vertices_point.y)/2);
			intervertex[2].x = (short)((coordp[2].x + vertices_point.x)/2);
	
			break;
		}
	}
	
	tag0 = tag1 = FALSE;
	vertices_point.x = vertices_point.y = 0;
	for (i=0; i<range_vertice; i++) {                   // Left - top point 
		for (j=0; j<=i; j++) {
			k = i - j;
		        h_pos = pArea->nBottom-j;
			w_pos = pArea->nRight-k;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				coordp[3].y = h_pos;
				coordp[3].x = w_pos;				
				tag0 = TRUE;
				break;			
			}
		}
		if (!tag0) continue;//break;

		for (j=0; j<=i; j++) {
			k = i - j;
		        h_pos = pArea->nBottom-k;
			w_pos = pArea->nRight-j;
			if (ColorZip_GetBImagePTR(h_pos,w_pos) == brightness) {
				vertices_point.y = h_pos;
				vertices_point.x = w_pos;				
				tag1 = TRUE;				
				break;
			}
		}
		if(tag1) {
			intervertex[3].y = (short)((coordp[3].y + vertices_point.y)/2);
			intervertex[3].x = (short)((coordp[3].x + vertices_point.x)/2);
	
			break;
		}
	}
	
	// for diamond shape detection
/*
	tag1 = FALSE;	

	crt_x_length = (pArea->nRight-pArea->nLeft)/5;
	crt_y_length = (pArea->nBottom-pArea->nTop)/5;

	comp_x1 = ABS((coordp[0].x-coordp[3].x));
	comp_x2 = ABS((coordp[1].x-coordp[2].x));
	comp_y1 = ABS((coordp[1].y-coordp[2].y));
	comp_y2 = ABS((coordp[0].y-coordp[3].y));
	
	if (((comp_x1 < crt_x_length) && (comp_y1 < crt_y_length)) || ((comp_x2 < crt_x_length) && (comp_y2 < crt_y_length))
	   || (pArea->nBottom < MAX(coordp[3].y,coordp[2].y) 
	   || pArea->nTop > MIN(coordp[0].y,coordp[1].y))){
		tag1 = TRUE;
	}

	if (tag1){
*/
	tag0 = FALSE;
	for (j = pArea->nTop; j < pArea->nTop+h/2; j++) { // Top
		for (i = pArea->nLeft; i <= pArea->nRight; i++) {
			if (ColorZip_GetBImagePTR(j,i) == brightness) {
				h_index = j;
				v_index = i;
				tag0 = TRUE;
				break;
			}
		}
		if (tag0) {
			for (i = pArea->nRight; i>=pArea->nLeft; i-- ) {
				if (ColorZip_GetBImagePTR(h_index,i)==brightness) 
				{
					intboundary[0].y = h_index;
					intboundary[0].x = (v_index+i)/2;
					break;
				}
			}
			break;
		}
	}    // Top

	tag0 = FALSE;
	for (j = pArea->nBottom; j > pArea->nBottom-h/2; j--)  {   // Bottom
		for (i = pArea->nLeft; i <= pArea->nRight; i++) {
			if (ColorZip_GetBImagePTR(j,i) == brightness) {
				h_index = j;
				v_index = i;
				tag0 = TRUE;
				break;
			}
		}
		if (tag0) {
			for (i = pArea->nRight; i>=pArea->nLeft; i-- ) {
				if (ColorZip_GetBImagePTR(h_index,i)==brightness) 
				{
					intboundary[3].y = h_index;
					intboundary[3].x = (v_index+i)/2;
					break;
				}
			}
			break;
		}
	}    // Bottom

	tag0 = FALSE;
	for (i = pArea->nLeft; i < pArea->nLeft+w/2; i++ ) {  // Left
		for (j = pArea->nTop; j <= pArea->nBottom; j++ ) {
			if (ColorZip_GetBImagePTR(j,i) == brightness) {
				h_index = j;
				v_index = i;
				tag0 = TRUE;
				break;
			}
		}
		if (tag0) {
			for (j = pArea->nBottom; j >= pArea->nTop; j-- ) {
				if (ColorZip_GetBImagePTR(j,v_index)==brightness) 
				{
					intboundary[2].y = (h_index+j)/2;
					intboundary[2].x = v_index;
					break;
				}
			}
			break;
		}
	}    //Left

	tag0 = FALSE;
	for (i = pArea->nRight; i > pArea->nRight-w/2; i-- ) { // Right
		for (j = pArea->nTop; j < pArea->nBottom; j++ ) {
			if (ColorZip_GetBImagePTR(j,i) == brightness) {
				h_index = j;
				v_index = i;
				tag0 = TRUE;
				break;
			}
		}
		if (tag0) {
			for (j = pArea->nBottom; j >= pArea->nTop; j-- ) {
				if (ColorZip_GetBImagePTR(j,v_index)==brightness) 
				{
					intboundary[1].y = (h_index+j)/2;
					intboundary[1].x = v_index;
					break;
				}
			}
			break;
		}
	}    //Right

	// 2006.03.30  사선 추출컖E경계선 추횁E중 큰 이미지를 선택 하게 수정
	comp_line_v1 = ABS(intervertex[0].x-intervertex[3].x)*ABS(intervertex[0].x-intervertex[3].x)+ABS(intervertex[0].y-intervertex[3].y)*ABS(intervertex[0].y-intervertex[3].y);
	comp_line_v2 = ABS(intervertex[1].x-intervertex[2].x)*ABS(intervertex[1].x-intervertex[2].x)+ABS(intervertex[1].y-intervertex[2].y)*ABS(intervertex[1].y-intervertex[2].y);
	comp_line_b1 = ABS(intboundary[0].x-intboundary[3].x)*ABS(intboundary[0].x-intboundary[3].x)+ABS(intboundary[0].y-intboundary[3].y)*ABS(intboundary[0].y-intboundary[3].y);
	comp_line_b2 = ABS(intboundary[1].x-intboundary[2].x)*ABS(intboundary[1].x-intboundary[2].x)+ABS(intboundary[1].y-intboundary[2].y)*ABS(intboundary[1].y-intboundary[2].y);

	if (MIN(comp_line_v1,comp_line_v2) > MIN(comp_line_b1,comp_line_b2)){
		for (i=0;i<4 ;i++ )
		{
			coordp[i].x=intervertex[i].x*ZOOM;
			coordp[i].y=intervertex[i].y*ZOOM;
		}
	}else{
		for (i=0;i<4 ;i++ )
		{
			coordp[i].x=intboundary[i].x*ZOOM;
			coordp[i].y=intboundary[i].y*ZOOM;
		}
	}

	
/*	celld[0]=pArea->nLeft;
	celld[1]=pArea->nRight;
	celld[2]=pArea->nTop;
	celld[3]=pArea->nBottom;
	*/
//	return TRUE;
}


int ColorZip_VerticesSelector(czPOSITION *vertices_nonedge,czPOSITION *vertices_edge) // detect adaptive Vertices 
{             
	short w_nonedge[2], w_edge[2], h_nonedge[2], h_edge[2], comp[4];
	short i, j;
	short nonedge_max, edge_max;

	for(i = 0; i<2; i++){
		j = i * 2; 
		w_nonedge[i] = ABS(vertices_nonedge[j+1].x - vertices_nonedge[j].x);
		w_edge[i]    = ABS(vertices_edge[j+1].x - vertices_edge[j].x);
		h_nonedge[i] = ABS(vertices_nonedge[2+i].y - vertices_nonedge[i].y);
		h_edge[i]    = ABS(vertices_edge[2+i].y - vertices_edge[i].y);
	}

	comp[0] =  ABS(w_nonedge[0]-w_nonedge[1]);
	comp[1] =  ABS(h_nonedge[0]-h_nonedge[1]);
	comp[2] =  ABS(w_edge[0]-w_edge[1]);
	comp[3] =  ABS(h_edge[0]-h_edge[1]);

	if (w_edge[0]<=10 || h_edge[0]<=10 || w_edge[1]<=10 || h_edge[1]<=0)
	{
		return 1;
	}
	nonedge_max = MAX(comp[0], comp[1]);
	edge_max = MAX(comp[2], comp[3]);
	if (nonedge_max<edge_max) {
		return 1;
	}else{
		return 0;
	}
}


/**
 * \brief 코드의 한계사각 영역을 찾는함펯E
 *
 * 픸E� 이미지를 9따櫓 하여 검은 픽셀이 많은 영역을 기준으로 잡아 기준점컖E가장근접하툈E흰색이 제일 많은 셀을 가햨E각 열들을 구하여
 * 최초로 한컖E사각 영역으로 정한다.
 *
 * \param pArea : 디코탛E하콅E위한 기본 정보를 가지컖E다니는 구조체
 *
 * \return
 */
void ColorZip_GetRoughBoundaryRect(czLPDecoderArea pArea) // Finding Tag boundary rectagle 
{                      // location : area of maxinum gray pixels   0 to 8
	short serial_gray, prev_serial_gray;         // number of serial white pixels
	short i, j;
	czPOSITION tmp_crop = {0,0};
	czPOSITION crop[4];
	int WORKIMAGE_HEIGHT = pArea->BinaryImageHeight/CODEAREADIVIDE;
	int WORKIMAGE_WIDTH = pArea->BinaryImageWidth/CODEAREADIVIDE;
	short pos_center_H;    
	short pos_center_W;
	short area_num;	
	short candidate_tag[49]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};  // 25 position for pixel sampling
	short i1, j1;
	short max_area = (short)((CODEAREADIVIDE-1)*(CODEAREADIVIDE-1)/2);  // default is center of image
	short area_center_X, area_center_Y;
	short max_pixel;
	short area_sample;

	for (i=0; i<NVOTES; i++){
		crop[i].y = 0;
		crop[i].x = 0;
	}

    /// 2007.04.13 코탛E영역을 찾을때 비율적으로 심플링 하게 변컖E
	if (WORKIMAGE_HEIGHT > WORKIMAGE_WIDTH)
	{
		area_sample = ((WORKIMAGE_WIDTH/2) - 1);
	}else{
		area_sample = ((WORKIMAGE_HEIGHT/2) - 1);
	}

	if (area_sample <= 0)
	{
		return;
	}
    /// end

 	for (j=0; j<(CODEAREADIVIDE-1); j++) {			     // -------------------   코탛E영역 세분화 2007.01.09
		for (i=0; i<(CODEAREADIVIDE-1); i++) {			 //  | 0  1  2  3  4 |      
			pos_center_H = (j+1)*WORKIMAGE_HEIGHT;       //  | 5  6  7  8  9 |       
			pos_center_W = (i+1)*WORKIMAGE_WIDTH;        //  |10 11 12 13 14 | 
			area_num = j*(CODEAREADIVIDE-1)+i;		     //  |15 16 17 18 19 |
														 //  |20 21 22 23 24 |	
			for (j1=0; j1<area_sample; j1++){			 // -------------------
				for (i1=0; i1<area_sample; i1++) 
				{
					if (ColorZip_GetBImagePTR(pos_center_H-j1,pos_center_W-i1) == MINPVAL) 
						candidate_tag[area_num]++; //calculate black pixels

					if (ColorZip_GetBImagePTR(pos_center_H+j1,pos_center_W+i1) == MINPVAL) 
						candidate_tag[area_num]++; //calculate black pixels

					if (ColorZip_GetBImagePTR(pos_center_H-j1,pos_center_W+i1) == MINPVAL) 
						candidate_tag[area_num]++; //calculate black pixels

					if (ColorZip_GetBImagePTR(pos_center_H+j1,pos_center_W-i1) == MINPVAL) 
						candidate_tag[area_num]++; //calculate black pixels
				}
			}
		}
	}
	
	max_pixel = candidate_tag[max_area];
	for (i=0; i<(CODEAREADIVIDE-1)*(CODEAREADIVIDE-1); i++)
	{
		if (max_pixel < candidate_tag[i]) 
		{
			max_area = i;
			max_pixel = candidate_tag[i];
		}
	}

//		printf("max_area = %d   \n",max_area);
	area_center_X=((int)((max_area%(CODEAREADIVIDE-1))+1) * WORKIMAGE_WIDTH);
	area_center_Y=((int)((max_area/(CODEAREADIVIDE-1))+1) * WORKIMAGE_HEIGHT);
	prev_serial_gray = 0;
	// atripp 2005-6-16 performance enhancement:
	// On each of the four cases below, I break out of the inner loop when
	// it's not possible to find a longer sequence of white pixels.
	// For example, in this first loop, if the longest sequence so far (prev_serial_gray) is
	// 20 pixels, and the image is 100 pixels wide, and we see a non-white
	// pixel at x position 81, there's no way we're going to find a sequence of
	// 20 white pixels in those last 19 pixels on the row.
	for (j=0; j<area_center_Y; j++) { // cadidate point left to right in top
		serial_gray = 0;
		for (i=0; i<pArea->BinaryImageWidth ; i++) {
			if (ColorZip_GetBImagePTR(j,i)==MAXPVAL) 
			{
				if(serial_gray == 0){
					tmp_crop.y=j;     
					tmp_crop.x=i;
				}
				serial_gray++;           // countinuous white pixels
				if (serial_gray >= prev_serial_gray) { 
					crop[0].y = tmp_crop.y;
					crop[0].x = tmp_crop.x;
					prev_serial_gray = serial_gray;
				}
			}
			else {
				serial_gray = 0;
				if (i > (pArea->BinaryImageWidth-prev_serial_gray)) {
					break;
				}
			}			
		}
	}
	
	prev_serial_gray = 0;
	// cadidate point left to right in bottom
	for (j=pArea->BinaryImageHeight-1; j>area_center_Y; j--)  { // j>=0
		serial_gray = 0;
		for (i=0; i<pArea->BinaryImageWidth ; i++) {
			
			if (ColorZip_GetBImagePTR(j,i)==MAXPVAL) {
				if (serial_gray == 0) {
					tmp_crop.y=j;     
					tmp_crop.x=i;
				}
				serial_gray++;           // countinuous white pixels
				if (serial_gray >= prev_serial_gray) { 
					crop[1].y = tmp_crop.y;
					crop[1].x = tmp_crop.x;
					prev_serial_gray = serial_gray;
				}
			}
			else {
				serial_gray = 0;
				if (i > (pArea->BinaryImageWidth-prev_serial_gray)) {
					break;
				}
			}						
		}
	}
	
	prev_serial_gray =0;
	// cadidate points top to down in left 
	for (i=0; i<area_center_X; i++) {
		serial_gray = 0;
		for (j=0; j<pArea->BinaryImageHeight; j++) {
			if (ColorZip_GetBImagePTR(j,i)==MAXPVAL) {
				if (serial_gray == 0){
					tmp_crop.y = j;     
					tmp_crop.x = i;
				}
				serial_gray++;           // countinuous white pixels
				if (serial_gray >= prev_serial_gray) { 
					crop[2].y = tmp_crop.y;
					crop[2].x = tmp_crop.x;
					prev_serial_gray = serial_gray;
				}
			}
			else {
				serial_gray = 0;
				if (j > (pArea->BinaryImageHeight-prev_serial_gray)) {
					break;
				}
			}						
		}		
	}
	
    prev_serial_gray =0;
	// cadidate points top to down in right 
	for (i=pArea->BinaryImageWidth-1; i>area_center_X; i--)  { //i>=0
		serial_gray = 0;
		for (j=pArea->BinaryImageHeight-1; j>=0; j--) {
			if (ColorZip_GetBImagePTR(j,i)==MAXPVAL) {
				if (serial_gray == 0) {
					tmp_crop.y = j;     
					tmp_crop.x = i;
				}
				serial_gray++;           // countinuous white pixels
				if (serial_gray >= prev_serial_gray) { 
					crop[3].y = tmp_crop.y;
					crop[3].x = tmp_crop.x;
					prev_serial_gray = serial_gray;
				}
			}
			else {
				serial_gray = 0;
				if (j < prev_serial_gray) {
					break;
				}
			}						
			
		}		
	}
	
// 2005. 4. 22 Cheolho cheong
	pArea->nLeft = crop[2].x;                // left   
	pArea->nRight = crop[3].x ;				// right
	pArea->nTop = crop[0].y ;				// top    
	pArea->nBottom = crop[1].y;				// bottom

	if (crop[2].x < crop[3].x) {
		pArea->nLeft = crop[2].x;                // left   
		pArea->nRight = crop[3].x ;				// right
	}
	else {
		pArea->nLeft = crop[3].x;                // left   
		pArea->nRight = crop[2].x ;				// right
	}

	if (crop[0].y < crop[1].y) {
		pArea->nTop = crop[0].y ;				// top    
		pArea->nBottom = crop[1].y;				// bottom
	}
	else {
		pArea->nTop = crop[1].y ;				// top    
		pArea->nBottom = crop[0].y;				// bottom
	}
}

// codearea_detect.c
