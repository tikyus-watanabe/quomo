#include <android/log.h>

#include "colorcode.h"
#include  "bbd_clustering.h"
#include "bbd.h"
//#include "utils.h"

/* Function declarations */

int 		ColorZip_Color_CellClassifier_CLUST(czLPDecoderArea pArea);
void 		ColorZip_sortV( STATS arr[], int beg, int end );
void 		ColorZip_sortH( STATS arr[], int beg, int end );
void 		ColorZip_sortJumps( JUMPS arr[], int beg, int end );
void 		ColorZip_swapStats( STATS *a, STATS *b );
void 		ColorZip_swapJumps( JUMPS *a, JUMPS *b );

int ColorZip_Color_CellClassifier_CLUST(czLPDecoderArea pArea)
{
	JUMPS jumps[40];
	STATS cellStats[40];
	short i, j, nThreshold;
	short nThresholdRedGreen = 0;
	short nThresholdGreenBlue = 0;
	short nThresholdBlueRed = 0;
	int nCurrentColor=0;
	short nHue, nHueLast=0, nHuePrev=0;
	short nFirstCell=0;
	short nMaxJump = 0;
	short nJump;
	short nLastBlack=0;
	short nNumColorCells = 0;
	int nRed, nGreen, nBlue;
	int czret;
	short max_value[3] = {0,0,0};
	short min_value[3] = {255,255,255};
	short max_lum,min_lum;  // max & min luminance 

	for( i = 0; i < pArea->nMatrixSize; i++ ){
		pArea->m_nCell[i] = -1;
	}
	for (i=0 ;  i < pArea->nMatrixSize; i++){
		nRed = nGreen = nBlue = 0;
		for( j = 0; j < pArea->nNumPoints; j++ )
		{
			nRed += pArea->pointArry[i][j].R;
			nGreen += pArea->pointArry[i][j].G;
			nBlue += pArea->pointArry[i][j].B;
		}

		// Calculate the mean colour of the cell.
		nRed = nRed / pArea->nNumPoints;
		nGreen = nGreen / pArea->nNumPoints;
		nBlue = nBlue / pArea->nNumPoints;

		cellStats[i].index = i; // cell index
		cellStats[i].R = nRed;
		cellStats[i].G = nGreen;
		cellStats[i].B = nBlue;

		// Calculate maximum difference between R, G and B.
		cellStats[i].max = MAX3( nRed, nGreen, nBlue );
		cellStats[i].min = MIN3( nRed, nGreen, nBlue );
		cellStats[i].diff = cellStats[i].max - cellStats[i].min;

		// Convert to HSV. Classifying colours is MUCH more accurate when comparing hues as opposed to comparing RGB sets.
		// As well, the intensity (V) is a very useful way of differentiating black cells from coloured cells.
		//ColorZip_RGBtoHSV( (unsigned char) nRed, (unsigned char) nGreen, (unsigned char) nBlue, &(cellStats[i].H), &(cellStats[i].S), &(cellStats[i].V) );
		ColorZip_RGBtoHSV( &(cellStats[i]) );
	}

	for (i=0; i<pArea->nMatrixSize; i++){
		if (cellStats[i].R < min_value[0]) min_value[0] = cellStats[i].R;
		if (cellStats[i].R > max_value[0]) max_value[0] = cellStats[i].R; 
		if (cellStats[i].G < min_value[1]) min_value[1] = cellStats[i].G; 
		if (cellStats[i].G > max_value[1]) max_value[1] = cellStats[i].G; 
		if (cellStats[i].B < min_value[2]) min_value[2] = cellStats[i].B; 
		    if (cellStats[i].B > max_value[2]) max_value[2] = cellStats[i].B; 
	}

	max_lum = MAX3(max_value[0],max_value[1],max_value[2]);
	min_lum = MIN3(max_value[0],max_value[1],max_value[2]);
	
	if ( max_lum >= min_lum * 2) {         // 2006.1.3 by Cheolho Cheong
	
		min_lum = MIN3(min_value[0],min_value[1],min_value[2]); // 2006.4.20 by Cheolho Cheong
		
		for (i=0; i<pArea->nMatrixSize; i++){
			cellStats[i].R = (unsigned char)((cellStats[i].R - min_value[0])*(max_lum-min_lum)/(max_value[0]-min_value[0]+1)+min_lum);
			cellStats[i].G = (unsigned char)((cellStats[i].G - min_value[1])*(max_lum-min_lum)/(max_value[1]-min_value[1]+1)+min_lum);
			cellStats[i].B = (unsigned char)((cellStats[i].B - min_value[2])*(max_lum-min_lum)/(max_value[2]-min_value[2]+1)+min_lum);
			ColorZip_RGBtoHSV( &(cellStats[i]) );
		}
	}

	// Categorize the average cell colours into 4 distinct colours. Using the RGB values
	// or the hues doesn't work so well when there's non-white lighting. But this is really
	// a classical clustering problem. We want to divide all the cells into 4 clusters. 
	// So the classic K-Means Clustering Algorithm might work, except that assumes you know nothing
	// about the data and that's not TRUE here. For instance, cells with hues between green and blue
	// definitely go into one or the other, but not into red. So we can use this info to our
	// advantage by sorting the cells according to their hues, from smallest to largest. As well, we
	// can sort by the intensity (V) so as to find the black cells, for which the hue is meaningless.
	// Afterwards, we could analyze the variance of each cluster and try to minimize their sums by 
	// moving the cells near the edges of each cluster.

	// Sort by cell intensity (i.e. by V from HSV) from smallest to largest, in order to find the black cells (for which H is meaningless).
	ColorZip_sortV( cellStats, 0, pArea->nMatrixSize );

	// Find the biggest jump in intensity (V). THAT'S where the black cells stop.
	// Only check halfway through the list, as it is highly unlikely to have more than 12 black cells.
	for( i = 0; i < pArea->nMatrixSize/2 - 1; i++ )
	{
		nJump = cellStats[i+1].V - cellStats[i].V;
		if( nJump > nMaxJump )
		{
			nMaxJump = nJump;
			nLastBlack = i;
		}
	}

	// Now that we've found the end of the black cells, officially classify them.
	for( i = 0; i <= nLastBlack; i++ )
	{
		pArea->m_nCell[cellStats[i].index] = COLOR_K; // black
	}


	// Even though we've classified all the black cells already, there may still be some that
	// weren't dark due to white pixels (in design codes) that raised the average intensity.
	// So classify any grey cells as black.
	for( i = 0; i < pArea->nMatrixSize; i++ )
	{
		if( pArea->m_nCell[cellStats[i].index] != COLOR_K ) // not already classified as black
		{
			if( cellStats[i].diff < 10 )
			{
				pArea->m_nCell[cellStats[i].index] = COLOR_K; // black
			}
		}
	}
	// Now sort by Hue to find the red, green and blue cells.
	// Hues: 0=red, 60=yellow, 120=green, 180=cyan, 240=blue, 300=magenta, 360=red
	// Again, we're looking for big jumps in hue value to indicate the colour has changed.
	ColorZip_sortH( cellStats, 0, pArea->nMatrixSize );



	// Determine if first color cell in list is red or green (reds may be below 360 or above 0).
	for( i = 0; i < pArea->nMatrixSize; i++ )
	{
		if( pArea->m_nCell[cellStats[i].index] != COLOR_K ){ // not black
			if( cellStats[i].H < 60 ){
				nCurrentColor = COLOR_R; // red
				nThreshold = 79;
			}else{
				nCurrentColor = COLOR_G; // green
				nThreshold = 29;
			}
			pArea->m_nCell[cellStats[i].index] = nCurrentColor;
			nHueLast = cellStats[i].H;
			nHuePrev = cellStats[i].H;
			nFirstCell = i+1;
			break;
		}
	}
	// Rather than using hardcoded "jump" thresholds, instead look for the largest 2 or 3 jumps.
	j = 0;
	for( i = 0; i < pArea->nMatrixSize; i++ )
	{
		if( pArea->m_nCell[cellStats[i].index] != COLOR_K ){ // not black
			jumps[j].index = cellStats[i].index;
			nHue = cellStats[i].H;
			jumps[j].H = nHue;
			jumps[j++].nJump = nHue - nHuePrev;
			nHuePrev = nHue;
		}
	}
	nNumColorCells = j;

	ColorZip_sortJumps( jumps, 0, nNumColorCells );
	
	// Look for highest 2 or 3 jumps.
	//if( jumps[nNumColorCells-3].nJump >= 20 ){	// this was too hardcoded to always work

	for( i = nNumColorCells-1; i >= nNumColorCells-3; i-- ){
		if( jumps[i].H >= 300 && (jumps[i].H - jumps[i].nJump) < 300 )
			nThresholdBlueRed = MAX( jumps[i].nJump, nThresholdBlueRed);
		else if( jumps[i].H > 180 && jumps[i].H < 300 && (jumps[i].H - jumps[i].nJump) <= 180 )
			nThresholdGreenBlue = MAX( jumps[i].nJump, nThresholdGreenBlue );
		else if( jumps[i].H >= 60 && jumps[i].H <= 180 && (jumps[i].H - jumps[i].nJump) < 60 )
			nThresholdRedGreen = MAX( jumps[i].nJump, nThresholdRedGreen );
	}

	// Perform sanity-check, for blurry images with no major jumps.
	if( nThresholdGreenBlue == 0 ){
		for( i = nNumColorCells-4; i >= 0; i-- ){
			if( jumps[i].H > 180 && jumps[i].H < 300 && (jumps[i].H - jumps[i].nJump) <= 180 ){
				nThresholdGreenBlue = jumps[i].nJump;
				break;
			}
		}
	}

	if( nThresholdRedGreen > 0 )
		nThreshold = nThresholdRedGreen;
	else
		nThreshold = nThresholdGreenBlue;

	if( nThresholdBlueRed == 0 )
		nThresholdBlueRed = 360; // impossible, which is the point -- there IS no way to reach that threshold


	// Categorize color cells. Look for big jumps. Start with first non-black cell (calculated above).
	for( i = nFirstCell; i < pArea->nMatrixSize; i++ )
	{
		if( pArea->m_nCell[cellStats[i].index] != COLOR_K ) // not already classified as black
		{
			nHue = cellStats[i].H;

			// Did we just jump to the next colour or not?
			//if( (nHue - nHueLast) > nThreshold )
			if( (nHue - nHueLast) == nThreshold )
			{
				// We've moved on to next color.
				if( nCurrentColor == COLOR_R ){
					nCurrentColor = COLOR_G;
					//nThreshold = 29;
					nThreshold = nThresholdGreenBlue;
				}else if( nCurrentColor == COLOR_G ){
					nCurrentColor = COLOR_B;
					//nThreshold = 99;
					nThreshold = nThresholdBlueRed;
				}else if( nCurrentColor == COLOR_B ){
					nCurrentColor = COLOR_R;
					// All the rest, if there are any, are red up to 360 (except black ones).
					for( j = i; j < pArea->nMatrixSize; j++ ){
						if( pArea->m_nCell[cellStats[j].index] != COLOR_K ){ // not black
							pArea->m_nCell[cellStats[j].index] = nCurrentColor;
						}
					}
					break;
				}
			}
			pArea->m_nCell[cellStats[i].index] = nCurrentColor;
			nHueLast = nHue;
		}
	}
	czret = ColorZip_Color_ParityChecker(pArea);
	__android_log_print(ANDROID_LOG_DEBUG, "decoder ColorZip_Color_CellClassifier_CLUST aaaaa2", "czret:%d,pCodeInfo.CodeValue:%llu",czret,pArea->czCODE->CodeValue);

	if ( czret == 0 ) 
	{
		return TRUE;			// success      
	} // if
	return FALSE;
}


/**
 * \brief  Sorting for Values (brightness)
 *
 *  ��Ⱚ�� ascending order�� ������
 *
 * \param  
 *  arr[]: ����ü, beg: ���� �ε���, end: ����E�ε���	
 *
 * \return
 *  void type
 */
void ColorZip_sortV( STATS arr[], int beg, int end )
{
  if( end > beg + 1 ){
    int piv = arr[beg].V, l = beg + 1, r = end;
    while( l < r ){
			if( arr[l].V <= piv ){
        l++;
			}else{ 
        ColorZip_swapStats(&arr[l], &arr[--r]);
			}
    }
    ColorZip_swapStats( &arr[--l], &arr[beg] );
    ColorZip_sortV( arr, beg, l );
    ColorZip_sortV( arr, r, end );
  }
}

/**
 * \brief  Sorting for Hue (Colors)
 *
 *  HSV���� Hue���� ascending order�� ������
 *
 * \param  
 *  arr[]: ����ü, beg: ���� �ε���, end: ����E�ε���	
 *
 * \return
 *  void type
 */
void ColorZip_sortH( STATS arr[], int beg, int end )
{
  if( end > beg + 1 ){
    int piv = arr[beg].H, l = beg + 1, r = end;
    while( l < r ){
			if( arr[l].H <= piv ){
        l++;
			}else{ 
        ColorZip_swapStats(&arr[l], &arr[--r]);
			}
    }
    ColorZip_swapStats( &arr[--l], &arr[beg] );
    ColorZip_sortH( arr, beg, l );
    ColorZip_sortH( arr, r, end );
  }
}

/**
 * \brief  Sorting for distances
 *
 *  Hue��, Brightness������ ������ ������� �� ���������� �Ÿ��� �軁EϿ� ������  
 *  ���� ������� ����Eū ������ ã��E������
 *
 * \param  
 *  arr[].jump: ���������� �Ÿ�, beg: ���� �ε���, end: ����E�ε���	
 *
 * \return
 *  void type
 */
void ColorZip_sortJumps( JUMPS arr[], int beg, int end )
{
  if( end > beg + 1 ){
    int piv = arr[beg].nJump, l = beg + 1, r = end;
    while( l < r ){
			if( arr[l].nJump <= piv ){
        l++;
			}else{ 
        ColorZip_swapJumps(&arr[l], &arr[--r]);
			}
    }
    ColorZip_swapJumps( &arr[--l], &arr[beg] );
    ColorZip_sortJumps( arr, beg, l );
    ColorZip_sortJumps( arr, r, end );
  }
}

/**
 * \brief  STAT ����ü���� �� ��ȯ 
 *
 * \param  STAT ����ü��E
 *
 * \return
 *      void type
 */ 
void ColorZip_swapStats( STATS *a, STATS *b )
{
	STATS t=*a;
	*a=*b;
	*b=t;
}


/**
* \brief  JUMPS ����ü���� �� ��ȯ 
 *
 * \param  JUMPS ����ü��E
 *
 * \return
 *      void type
 */
void ColorZip_swapJumps( JUMPS *a, JUMPS *b )
{
	JUMPS t=*a;
	*a=*b;
	*b=t;
}

