/**
 * \brief  header file for bbd_kmeans.c
 *
 *
 *
 */
 
#ifndef _CELL_DETECT_H_INCLUDED
#define _CELL_DETECT_H_INCLUDED



extern	unsigned char ColorZip_CellDetector(czLPDecoderArea pArea);
extern	int 	ColorZip_Ratio(short x1, short y1, short x2, short y2);
extern 	int 	ColorZip_Intercept(short x1, short y1, short x2, short y2);

//Type definitions
//


#endif /*  _CELL_DETECT_H_INCLUDED */
