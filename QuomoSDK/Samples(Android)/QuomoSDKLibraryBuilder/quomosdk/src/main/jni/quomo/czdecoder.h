#ifndef _CZDECODER_H_INCLUDED
#define _CZDECODER_H_INCLUDED


#ifdef __cplusplus
extern "C" {
#endif

	extern czLPDecoderArea	initialize(int  nOrgWidth, 	int nOrgHeight);
extern short	czDecoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo);
extern short	qDecoder(czLPDecoderArea pArea, short  codeType,  short codeSizeType, void *pCapturedImage , lpCZCODE  pCodeInfo);
extern void	    finalize(czLPDecoderArea pArea);

#ifdef __cplusplus
}
#endif

#endif   /* _DECODER_H_INCLUDED */
