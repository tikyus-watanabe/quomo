//
// Created by 齋藤浩幸 on 16/05/27.
//
#include <jni.h>
#include <vector>

#include <opencv2/opencv.hpp>
#include "quomo/QuomoInterface.h"
#include "quomo/colorcode.h"
#include "quomo/czdecoder.h"
#include        <openssl/evp.h>
#include        <openssl/aes.h>

using namespace std;
using namespace cv;


extern "C" {
typedef struct IMAGE_DATA {
    int width;
    int height;
    int depth;
    unsigned char *pixels;
} ImageData;

QuomoInterface *qmif;


void Java_jp_co_tikyus_quomo_sdk_QuomoInitialize(JNIEnv *env, jobject obj) {
    qmif = new QuomoInterface;

}
void Java_jp_co_tikyus_quomo_sdk_QuomoSetProductID(JNIEnv *env, jobject obj, jlong identifier) {
    qmif->setProductID(identifier);
}
jlong Java_jp_co_tikyus_quomo_sdk_MainActivity_QuomoGetProductID(JNIEnv *env, jobject obj) {
    return (qmif->getProductID());

}
jint Java_jp_co_tikyus_quomo_sdk_QuomoAddCode(JNIEnv *env, jobject obj, jlong code) {
    return (qmif->addCode(code));

}
jlong Java_jp_co_tikyus_quomo_sdk_QuomoGetCode(JNIEnv *env, jobject obj, jint index) {
    return (qmif->getCode(index));

}
jlong Java_jp_co_tikyus_quomo_sdk_QuomoGetDisplayCode(JNIEnv *env, jobject obj, jint index) {
    return (qmif->getDisplayCode(index));

}
jint Java_jp_co_tikyus_quomo_sdk_QuomoMakeBitmap(JNIEnv *env, jobject obj, jlong matPtr) {
    cv::Mat &matImage = *(cv::Mat *) matPtr;
    qmif->makeQuomoImage(matImage);
    return (0);
}
jint Java_jp_co_tikyus_quomo_sdk_QuomoMakeColors(JNIEnv *env, jobject obj, jintArray ptr) {
    jint *jptr = env->GetIntArrayElements(ptr, NULL);
    QUOMOCOLORS *colors = (QUOMOCOLORS *) jptr;
    /*
    colors[0].r = 123;
    colors[0].g = 244;
    colors[0].b = 98;
    colors[1].r = 123;
    colors[1].g = 23;
    colors[1].b = 11;
     */
    qmif->makeQuomoColors(colors);
    return (sizeof(jptr));
}
/**
 * yuv420の画像をrgbに変換する
 */
JNIEXPORT void JNICALL Java_jp_co_tikyus_quomo_sdk_QuomoDecodeYUV420SP(JNIEnv *env, jobject obj, jbyteArray src, jint width, jint height, jintArray dst) {
    jboolean bo;
    int j, i, y, v = 0, u = 0, uvp, yp = 0;
    int y1192;
    int r, g, b;
    int frameSize = width * height;

    // javaから渡ってきた配列には直接アクセスできない
    jbyte *yuv420sp = env->GetByteArrayElements(src, &bo);
    jint *retArray = env->GetIntArrayElements(dst, &bo);

    for (j = 0; j < height; j++) {
        uvp = frameSize + (j >> 1) * width;
        for (i = 0; i < width; i++, yp++) {
            y = (0xff & yuv420sp[yp]) - 16;
            if (y < 0) y = 0;
            if ((i & 1) == 0) {
                v = (0xff & yuv420sp[uvp++]) - 128;
                u = (0xff & yuv420sp[uvp++]) - 128;
            }

            y1192 = 1192 * y;
            r = (y1192 + 1634 * v);
            g = (y1192 - 833 * v - 400 * u);
            b = (y1192 + 2066 * u);

            if (r < 0) r = 0; else if (r > 262143) r = 262143;
            if (g < 0) g = 0; else if (g > 262143) g = 262143;
            if (b < 0) b = 0; else if (b > 262143) b = 262143;

            retArray[yp] = (0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff));
        }
    }

    // 要開放
    env->ReleaseByteArrayElements(src, yuv420sp, 0);
    env->ReleaseIntArrayElements(dst, retArray, 0);
}

/**
 * デコーダー本体
 */
JNIEXPORT jlong JNICALL Java_jp_co_tikyus_quomo_sdk_QuomoCzDecoder(JNIEnv *env, jobject obj, jint width, jint height, jbyteArray rgb) {

    jboolean b;
    // javaから渡ってきた配列には直接アクセスできない
    jbyte *image = env->GetByteArrayElements(rgb, NULL);

    czDecoderArea *pArea;
    CZCODE code;
    unsigned long long ret;
    ImageData *img = new ImageData();

    img->width = width;
    img->height = height;
    img->depth = 24;
    img->pixels = (unsigned char *) image;

    // 解析操作
    if ((pArea = initialize(img->width, img->height)) != NULL) {
        ret = qmif->decoder(pArea, COLOR_CZCODE, 1, img->pixels, &code);
        //ret = czDecoder(pArea, COLOR_CZCODE, 1, img->pixels, &code);
        //LOGI(" Code Value: %x",code.CodeValue);
    }
    else {
//			DBGPRINTF("CC decoder init Error");
        delete (img);
        return 0;
    }
    // 後処理
    if (pArea->nParityType != 0) // CCL A.Itoh 2014/01/09
    {
        code.CodeValue = 0;
    }
    finalize(pArea);
    delete (img);
    // 要開放
    env->ReleaseByteArrayElements(rgb, image, 0);
//	return 2000;
    //return code.CodeValue;
    return (ret);
}
JNIEXPORT jint JNICALL Java_jp_co_tikyus_quomo_sdk_QuomoGetDecodeCodeCount(JNIEnv *env, jobject obj) {
    return (qmif->getDecodeCodeCount());
}
JNIEXPORT jlong JNICALL Java_jp_co_tikyus_quomo_sdk_QuomoGetDecodeCode(JNIEnv *env, jobject obj,jint index) {
    return (qmif->getDecodeCode(index));
}
JNIEXPORT jboolean JNICALL Java_jp_co_tikyus_quomo_sdk_QuomoSetProductKey(JNIEnv *env, jobject obj,jstring jProductKey) {
    const char *productKey = env->GetStringUTFChars(jProductKey, 0);
    bool result = qmif->SetProductKey((char *)productKey);
    env->ReleaseStringUTFChars(jProductKey, productKey);
    return(result);
}
/*

JNIEXPORT jlong JNICALL Java_jp_co_tikyus_quomo_sdk_openssltest(){
    EVP_CIPHER_CTX  de;
    char *key = "1234567890123456\0";
    char *ptr = "5914edf989ecbd0497c9913704c6ca7c\0";
    char *p,*data;
    char buf[80];
    int     datasize,c;
    int     p_len,f_len=0;
    char    *plaintext;


    if(strlen(key)!=EVP_CIPHER_key_length(EVP_aes_128_ecb())){
        fprintf(stderr,"key length must be %d\n",EVP_CIPHER_key_length(EVP_aes_128_ecb()));
        return(-1);
    }
    data=(char *)malloc(strlen(ptr));
    for(c=0;*ptr!='\0';ptr+=2,c++){
        buf[0]=*ptr;
        buf[1]=*(ptr+1);
        buf[2]='\0';
        data[c]=strtol(buf,NULL,16);
    }
    datasize=c;

    EVP_CIPHER_CTX_init(&de);
    EVP_DecryptInit_ex(&de,EVP_aes_128_ecb(),NULL,(unsigned char *)key,NULL);

    p_len=datasize;
    plaintext=(char *)calloc(p_len+1,sizeof(char));

    EVP_DecryptUpdate(&de,(unsigned char *)plaintext,&p_len,(unsigned char *)data,datasize);
    EVP_DecryptFinal_ex(&de,(unsigned char *)(plaintext+p_len),&f_len);

    plaintext[p_len+f_len]='\0';
    printf("%s\n",plaintext);

    EVP_CIPHER_CTX_cleanup(&de);


    __android_log_print(ANDROID_LOG_DEBUG,"Tag","plaintext : %s",plaintext);


    free(plaintext);


    free(data);
}
 */
}