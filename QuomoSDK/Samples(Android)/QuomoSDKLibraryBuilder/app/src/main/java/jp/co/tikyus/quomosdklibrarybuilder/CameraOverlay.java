package jp.co.tikyus.quomosdklibrarybuilder;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by saitouhiroyuki on 16/06/13.
 */
public class CameraOverlay extends SurfaceView {
    public CameraOverlay(Context context) {
        super(context);
    }

    public CameraOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas){
        SurfaceHolder holder = getHolder();
        Canvas c = holder.lockCanvas();
        if(c != null) {
            int base = Math.min(getHeight(), getWidth()) / 3;
            int frameSize = base * 2;
            int start = base / 2;
            int end = start + frameSize;

            c.save();
            Paint p = new Paint();
            p.setStrokeWidth(10);
            p.setColor(Color.RED);
            p.setStyle(Paint.Style.STROKE);
            canvas.drawRect(start, start, end, end, p);

            c.restore();
            holder.unlockCanvasAndPost(c);
        }

    }

}
