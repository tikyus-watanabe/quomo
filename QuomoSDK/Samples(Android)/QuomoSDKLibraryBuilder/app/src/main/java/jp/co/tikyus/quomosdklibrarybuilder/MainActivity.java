package jp.co.tikyus.quomosdklibrarybuilder;

import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import jp.co.tikyus.quomo.sdk;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.opencv.core.Mat;
import org.opencv.android.Utils;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity  implements CameraView.OnListener {
    private Timer timer;
    private QuomoTimerTask timerTask = null;
    private Handler handler = new Handler();
    private Boolean flgTimer = false;
    private sdk _quomosdk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Quomo初期設定
        _quomosdk=new sdk();
        _quomosdk.QuomoInitialize();
        //_quomosdk.setProductID(10001);


        boolean licenseResult;
        //productid:00978,expire:20160913,type:normal,limit:99999999 有効期限内 normal証明書
        licenseResult = _quomosdk.QuomoSetProductKey("63f198a3fec26fe8021ddb0dfbbccb01ad5dd8d8676b5f68452ab61f5b49ba7d276c605e7c5a0c336fd6f566c1a187cbd147f30f9a9a18790c40525a3ed28b05");

        //productid:00978,expire:20160613,type:normal,limit:1000 有効期限外 normal証明書
        //licenseResult = _quomosdk.QuomoSetProductKey("63f198a3fec26fe8021ddb0dfbbccb0162d69c52cb1a4b0129325582ed45c549276c605e7c5a0c336fd6f566c1a187cb091b59024e67c020931298445ceee33b");

        //productid:00978,expire:20160913,type:trial,limit:5 有効期限内 trial証明書
        //licenseResult = _quomosdk.QuomoSetProductKey("63f198a3fec26fe8021ddb0dfbbccb01ad5dd8d8676b5f68452ab61f5b49ba7d44adcbcc8c24758d9a496562befc4b952f6277b2e1a37965ccad280de28fbb15");

        //productid:00978,expire:20160913,type:normal,limit:99999999 有効期限内 normal証明書
        //licenseResult = _quomosdk.QuomoSetProductKey("63f198a3fec26fe8021ddb0dfbbccb01ad5dd8d8676b5f68452ab61f5b49ba7d276c605e7c5a0c336fd6f566c1a187cbd147f30f9a9a18790c40525a3ed28b05");


        Log.d("MAIN","licenseResult:"+licenseResult);

        _quomosdk.QuomoAddCode(10000001L);
        _quomosdk.QuomoAddCode(10000002L);
        Bitmap bitmap = _quomosdk.makeBitmap();
        _quomosdk.makeBitmap();
        _quomosdk.makeBitmap();
        _quomosdk.makeBitmap();
        _quomosdk.makeBitmap();
        _quomosdk.makeBitmap();
        _quomosdk.makeBitmap();
        //リーダー用カメラ設定
        CameraView cameraView = (CameraView)findViewById(R.id.viewCamera);
        cameraView.setOnListener(this);
        cameraView._quomosdk = _quomosdk;
        cameraView._imageView =  (ImageView)findViewById(R.id.imageView);

/*
        //ビューワー用タイマー設定
        timer = new Timer();
        timerTask = new QuomoTimerTask();
        timer.schedule(timerTask, 0, 1);
*/
    }
    class QuomoTimerTask extends TimerTask {
        @Override
        public void run() {
            if(flgTimer) {
                return;
            }
            flgTimer = true;
            handler.post(new Runnable() {
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Bitmap bitmap = _quomosdk.makeBitmap();
                            ImageView imageView = (ImageView) findViewById(R.id.imageView);
                            imageView.setImageBitmap(bitmap);
                            _quomosdk.deleteBitmap();
                            flgTimer = false;
                        }
                    });


                }
            });
        }
    }
    @Override
    public void onDecode(CameraView cameraView, long id) {
        //Toast.makeText(this, "id=" + id, Toast.LENGTH_SHORT).show();
        ToneGenerator toneGenerator
                = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        if(id == 1) {
            toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP);
        }
        if(id == 2) {
            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ANSWER);
            int count = _quomosdk.QuomoGetDecodeCodeCount();
            for(int i=0;i<count;i++){
                long code  = _quomosdk.QuomoGetDecodeCode(i);
                Log.d("MAIN","code:" + code);
            }
        }

    }

}
