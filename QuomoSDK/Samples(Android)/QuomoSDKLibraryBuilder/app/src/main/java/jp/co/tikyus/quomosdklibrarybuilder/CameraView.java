package jp.co.tikyus.quomosdklibrarybuilder;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.RecoverySystem;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;
import jp.co.tikyus.quomo.sdk;
import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;


@SuppressWarnings("deprecation")
public class CameraView extends SurfaceView implements Camera.PreviewCallback, SurfaceHolder.Callback {
    public sdk _quomosdk;
    private Bitmap mBitmap;
    private Camera mCamera;
    private Thread mDecodeThread[];
    private long mDecodeTime;
    private Paint mFramePaint;
    private OnListener mOnListener;
    private int[] mRgb;
    private Bitmap _workBitmap;
    public ImageView _imageView;


    //public static final String TAG = CameraView.class.getSimpleName();
    public static final String TAG = "CAMERA";

    public interface OnListener {
        public void onDecode(CameraView cameraView, long id);
    }

    private class DecodeTask implements Runnable {
        private final int[] mRgb24;
        private final int mHeight;
        private final int mWidth;
        private final int mThreadIndex;

        public DecodeTask(int[] rgb, int width, int height,int threadIndex) {
            mHeight = height;
            mWidth = width;
            mRgb24 = new int[mWidth * mHeight];
            mThreadIndex = threadIndex;
            System.arraycopy(rgb, 0, mRgb24, 0, mRgb24.length);
        }

        @Override
        public void run() {
            if (mDecodeThread[mThreadIndex] == null) {
                return;
            }

            int height = mHeight;
            int width = mWidth;

            byte[] rgb = new byte[width * height * 3];

            int j = 0;
            for(int k=0;k<height;k++){
                for(int i = 0;i < width*3; i += 3, j++){
                    rgb[i+0+width*3*k] = (byte) ((mRgb24[j] >> 16) & 0xff);
                    rgb[i+1+width*3*k] = (byte) ((mRgb24[j] >>  8) & 0xff);
                    rgb[i+2+width*3*k] = (byte) ( mRgb24[j]        & 0xff);
                }
            }
            //
            long start = System.currentTimeMillis();
            final long id = _quomosdk.QuomoCzDecoder(width, height, rgb);
            long end = System.currentTimeMillis();
            Log.d(TAG,"decode time:" + (end - start) + "ms");
            //
            if (id != 0) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        onDecode(id);
                    }
                });
            }

            mDecodeThread[mThreadIndex] = null;
        }
    }

    public CameraView(Context context) {
        super(context);
        init();
    }

    public CameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);

        mFramePaint = new Paint();
        mFramePaint.setColor(Color.RED);
        mFramePaint.setStrokeWidth(5.0f);
        mFramePaint.setStyle(Style.STROKE);
        //
        mDecodeThread = new Thread[1];
        for(int i=0;i<mDecodeThread.length;i++){
            mDecodeThread[i]=null;
        }
    }

    private void onDecode(long id) {
        if (mOnListener != null) {
            mOnListener.onDecode(this, id);
        }

//        if (System.currentTimeMillis() - mDecodeTime > 2000) {
//            mFramePaint.setColor(Color.YELLOW);
//            mDecodeTime = System.currentTimeMillis();
//        }
    }

    public void setOnListener(OnListener onListener) {
        mOnListener = onListener;
    }

    // Camera.PreviewCallback

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        long nowTime = System.currentTimeMillis();
        int threadIndex = -1;

        //スレッドの空きを探す
        for(int i=0;i<mDecodeThread.length;i++){
            if(mDecodeThread[i]==null){
                threadIndex=i;
                break;
            }
        }
        if(threadIndex == -1)return;
        //

        //int height = mBitmap.getHeight();
        //int width = mBitmap.getWidth();
        Camera.Parameters params = camera.getParameters();
        Size cameraSize = params.getPictureSize();
        int height = cameraSize.height;
        int width = cameraSize.width;
        //_quomosdk.decodeYUV420SP(data, width, height, mRgb);
        _workBitmap = null;
        _workBitmap = getBitmapImageFromYUV(data,width,height);
        _workBitmap.getPixels(mRgb, 0, width, 0, 0, width, height);


        post(new Runnable() {
            //run()の中の処理はメインスレッドで動作されます。
            public void run() {
                if (_imageView != null && _workBitmap != null) {
                    _imageView.setImageBitmap(_workBitmap);

                }
            }
        });
        float scale = Math.max((float)getWidth() / width, (float)getHeight() / height);
        float left = (getWidth() / scale - width) / 2;
        int base = Math.min(getHeight(), getWidth()) / 3;
        int frameSize = base * 2;
        int start = base / 2;

        left = 0 ;
        frameSize = cameraSize.height;
        start = 0;

        //frameSize /= scale;
        //start /= scale;
//        int[] rgb = new int[frameSize * frameSize];
//        for (int index = 0; index < frameSize; index++) {
//            System.arraycopy(mRgb, width * (index + start) + start - (int)left, rgb, frameSize * index, frameSize);
//        }
//        mDecodeThread[threadIndex] = new Thread(new DecodeTask(rgb, frameSize, frameSize,threadIndex));
        int targetWidth = width /2;
        int targetHeight = height;
        int[] rgb = new int[targetWidth * targetHeight];
        for (int index = 0; index < targetHeight; index++) {
            System.arraycopy(mRgb, width * index, rgb, targetWidth * index, targetWidth);
        }
        mDecodeThread[threadIndex] = new Thread(new DecodeTask(rgb, targetWidth, targetHeight,threadIndex));
        mDecodeThread[threadIndex].start();
    }

    // SurfaceHolder.Callback

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = Camera.open();
        Camera.Parameters params = mCamera.getParameters();
        List< Size > sizeList = params.getSupportedPictureSizes();
        for (int i=0; i < sizeList.size(); i++) {
            Log.d(TAG, "Size = " + sizeList.get(i).width + "x" + sizeList.get(i).height);
        }

        //params.setPictureSize(640, 480);
        params.setPictureSize(320, 240);
        mCamera.setParameters(params);
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mCamera.stopPreview();

        Camera.Parameters parameters = mCamera.getParameters();
        /*
        List<Size> previewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        Size size = previewSizes.get(0);
        for (Size previewSize : previewSizes) {
            if ((previewSize.height * previewSize.width) > (size.height * size.width)) {
                size = previewSize;
            }
        }
        */
        Size size = parameters.getPictureSize();
        //size.width=320;
        //size.height=240;

        mBitmap = Bitmap.createBitmap(size.width, size.height, Bitmap.Config.ARGB_8888);
        mRgb = new int[size.height * size.width];

        parameters.setPreviewSize(size.width, size.height);
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        mCamera.setParameters(parameters);
        mCamera.setPreviewCallback(this);
        mCamera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.setPreviewCallback(null);
        mCamera.release();
        mCamera = null;
    }

    public static Bitmap getBitmapImageFromYUV(byte[] data, int width, int height) {
        YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
        byte[] jdata = baos.toByteArray();
        BitmapFactory.Options bitmapFatoryOptions = new BitmapFactory.Options();
        bitmapFatoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length, bitmapFatoryOptions);
        return bmp;
    }
}

