package jp.co.tikyus.QuomoSDKSampleViewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Timer;
import java.util.TimerTask;

import jp.co.tikyus.quomo.sdk;
import jp.co.tikyus.work001.R;

import android.view.View;
import android.widget.Toast;
import android.widget.Button;
import java.util.ArrayList;

import android.widget.TextView;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URL;

public class ColorActivity extends AppCompatActivity {
    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("quomosdk");
    }

    private Timer timer;
    private ColorActivity.QuomoTimerTask timerTask = null;
    private sdk _quomosdk;
    private Boolean flgTimer = false;
    private Handler handler = new Handler();
    private ArrayList codelist = new ArrayList();

    private Boolean flgcheckTimer = false;
    private Timer checktimer;
    private ColorActivity.QuomoCheckTimerTask checktimerTask = null;


    Long customer_id = 0L;
    Long checkpoint_id = 0L;
    Long qr_key = 0L;
    Boolean result_qr_key = false;

    //API
    private final String API_URL_PREFIX = "xigu4d46-nta.nta81online.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);

        Intent intent = getIntent();
        customer_id = intent.getLongExtra("customer_id",0);
        checkpoint_id = intent.getLongExtra("checkpoint_id",0);
        qr_key = intent.getLongExtra("qr_key",0);
        result_qr_key = intent.getBooleanExtra("result_qr_key",false);

        //Quomo初期設定
        _quomosdk=new sdk();
        _quomosdk.QuomoInitialize();
        boolean licenseResult;
        licenseResult = _quomosdk.QuomoSetProductKey("1148112069b0be3b20dacf2399a8266e88afcd15476589febf64a6fd4ec07800276c605e7c5a0c336fd6f566c1a187cb6cb2f15368b8be66ac7d333c97b1557b");
        Log.d("MAIN","licenseResult:"+licenseResult);

        _quomosdk.QuomoAddCode(qr_key);
//        _quomosdk.QuomoAddCode(12345678L);
//        _quomosdk.QuomoAddCode(21212123L);

        //ビューワー用タイマー設定
        timer = new Timer();
        timerTask = new QuomoTimerTask();
        timer.schedule(timerTask, 0, 1);

        //ビューワー用タイマー設定
        checktimer = new Timer();
        checktimerTask = new QuomoCheckTimerTask();
        checktimer.schedule(checktimerTask, 1000, 1000);
    }

    class QuomoTimerTask extends TimerTask {
        @Override
        public void run() {
            if (flgTimer) {
                return;
            }
            flgTimer = true;
            handler.post(new Runnable() {
                public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = _quomosdk.makeBitmap();
                        ImageView imageView = (ImageView) findViewById(R.id.imageView);
                        imageView.setImageBitmap(bitmap);
                        _quomosdk.deleteBitmap();
                        flgTimer = false;
                    }
                });
                }
            });
        }
    }

    class QuomoCheckTimerTask extends TimerTask {
        @Override
        public void run() {
            if (flgTimer) {
                return;
            }
            flgcheckTimer = true;
            ColorActivity.MyAsync asynk = new ColorActivity.MyAsync();
            asynk.execute();
        }
    }

    class MyAsync extends AsyncTask<String, Void, String> {
/*
        private final WeakReference<TextView> titleViewReference;
        private final WeakReference<TextView> dateViewReference;

        public MyAsync(TextView titletView, TextView dateView) {
            titleViewReference = new WeakReference<TextView>(titletView);
            dateViewReference = new WeakReference<TextView>(dateView);
        }
*/

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection con = null;
            StringBuffer result = new StringBuffer();
            Uri.Builder uriBuilder = new Uri.Builder();

            uriBuilder.scheme("https");
            uriBuilder.authority(API_URL_PREFIX);

            JSONObject jsonObject = new JSONObject();


            try {
                jsonObject.put("customer_id" , customer_id)
                        .put("checkpoint_id" , checkpoint_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            //チェックイン結果取得
            uriBuilder.path("/Quomo/get_Quomo_result");
            //String json = "{\"customer_id\":\"15284\",\"checkpoint_id\":\"69\"}";
            String json = String.valueOf(jsonObject);


            final String uriStr = uriBuilder.build().toString();

            try {

                URL url = new URL(uriStr);
                con = (HttpURLConnection) url.openConnection();
                // HTTPリクエストコード
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.setRequestProperty("Accept-Language", "jp");
                // データがJSONであること、エンコードを指定する
                con.setRequestProperty("Content-Type", "application/JSON; charset=utf-8");
                // POSTデータの長さを設定
                con.setRequestProperty("Content-Length", String.valueOf(json.length()));
                // リクエストのbodyにJSON文字列を書き込む
                OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
                out.write(json);
                out.flush();
                con.connect();

                // HTTPレスポンスコード
                final int status = con.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    // 通信に成功した
                    // テキストを取得する
                    final InputStream in = con.getInputStream();
                    String encoding = con.getContentEncoding();
                    if (null == encoding) {
                        encoding = "UTF-8";
                    }
                    final InputStreamReader inReader = new InputStreamReader(in, encoding);
                    final BufferedReader bufReader = new BufferedReader(inReader);
                    String line = null;
                    // 1行ずつテキストを読み込む
                    while ((line = bufReader.readLine()) != null) {
                        result.append(line);
                    }
                    bufReader.close();
                    inReader.close();
                    in.close();
                } else {
                    // 通信が失敗した場合のレスポンスコードを表示
                    System.out.println(status);
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (con != null) {
                    // コネクションを切断
                    con.disconnect();
                }
            }

            String wk =  result.toString();

            return wk;

//            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) { //doInBackgroundが終わると呼び出される
            try {
                    // JSONオブジェクトのインスタンス作成
                    JSONObject jsonObj = new JSONObject(result);
                    // キー"users"の値（JSON配列オブジェクト）をパース
                    Boolean checkin = jsonObj.getBoolean("result");

                    if(checkin == true)
                    {
                        timer.cancel();
                        checktimer.cancel();

                        Intent intent = new Intent(ColorActivity.this, EndActivity.class);
//                        Intent intent = new Intent(ColorActivity.this, MainActivity.class);

                        finish();
                        startActivity(intent);
                    }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}