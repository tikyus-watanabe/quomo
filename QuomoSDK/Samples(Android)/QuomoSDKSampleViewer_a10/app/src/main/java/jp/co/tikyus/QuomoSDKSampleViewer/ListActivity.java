package jp.co.tikyus.QuomoSDKSampleViewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import jp.co.tikyus.quomo.sdk;
import jp.co.tikyus.work001.R;

import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.Button;
import java.util.ArrayList;

import android.widget.TextView;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URL;




public class ListActivity extends AppCompatActivity {

    ArrayList <String> _id = new ArrayList<>();
    ArrayList <String> _name = new ArrayList<>();
    ArrayList <String> _day = new ArrayList<>();
    ArrayList <String> _start_time = new ArrayList<>();
    ArrayList <String> _end_time = new ArrayList<>();

    Long customer_id = 0L;
    Long qr_key = 0L;
    Boolean result_qr_key = false;
    Boolean result_list = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Intent intent = getIntent();
        customer_id = intent.getLongExtra("customer_id",0);
        qr_key = intent.getLongExtra("qr_key",0);
        result_qr_key = intent.getBooleanExtra("result_qr_key",false);
        result_list = intent.getBooleanExtra("result_list",false);

/*
        Long customer_id = intent.getLongExtra("customer_id",0);
        Long qr_key = intent.getLongExtra("qr_key",0);
        Boolean result_qr_key = intent.getBooleanExtra("result_qr_key",false);
        Boolean result_list = intent.getBooleanExtra("result_list",false);
*/
        ArrayList checkpoint_list = new ArrayList<>();
        checkpoint_list = intent.getStringArrayListExtra("checkpoint_list");

        _id = intent.getStringArrayListExtra("_id");
        _name = intent.getStringArrayListExtra("_name");
        _day = intent.getStringArrayListExtra("_day");
        _start_time = intent.getStringArrayListExtra("_start_time");
        _end_time = intent.getStringArrayListExtra("_end_time");


        ArrayList data = new ArrayList<>();
        String BR = System.getProperty("line.separator");

        String[] mainText = new String[checkpoint_list.size()];
        String[] subText = new String[checkpoint_list.size()];
        if (checkpoint_list != null) {
            int len = checkpoint_list.size();
            for (int i=0;i<len;i++){
//                String[] main,sub;
                mainText[i] = "名称：" + _name.get(i).toString();
                subText[i] =  "日時：" + _day.get(i).toString() + " " + _start_time.get(i).toString() + "～" + _end_time.get(i).toString();

/*
                str = "cpid：" + _id.get(i).toString() + BR +
                      "名称：" + _name.get(i).toString() + BR +
                      "日付：" + _day.get(i).toString() + BR +
                      "開始：" + _start_time.get(i).toString() + BR +
                      "終了：" + _end_time.get(i).toString();

                data.add(str);
 */
            }
        }

        // ListView に表示する文字列を生成
        final List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        for (int i=0; i<mainText.length; i++) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("main", mainText[i]);
            map.put("sub", subText[i]);
            list.add(map);
        }


        SimpleAdapter adapter = new SimpleAdapter(
                this,
                list,
                android.R.layout.simple_list_item_2,
                new String[] {"main", "sub"},
                new int[] {android.R.id.text1, android.R.id.text2}
                );

        // リストビューにデータ (アダプタ) を追加
        ListView listView1 = (ListView)findViewById(R.id.listView);
        listView1.setAdapter(adapter);

        // アイテムクリック時のイベントを設定 (テキスト 2 行表示)
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                // 項目選択時の処理を記述
                // 引数 pos が選択された項目のインデックス (0 始まり)

                Long cpid = Long.parseLong(_id.get(pos).toString());

                Intent intent = new Intent(ListActivity.this, ColorActivity.class);

                intent.putExtra("customer_id", customer_id);
                intent.putExtra("checkpoint_id", cpid);
                intent.putExtra("qr_key", qr_key);
                intent.putExtra("result_qr_key", result_qr_key);

                finish();
                startActivity(intent);

            }
        });

/*
        // リスト項目とListViewを対応付けるArrayAdapterを用意する
        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data);

        // ListViewにArrayAdapterを設定する
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);
*/

    }
}