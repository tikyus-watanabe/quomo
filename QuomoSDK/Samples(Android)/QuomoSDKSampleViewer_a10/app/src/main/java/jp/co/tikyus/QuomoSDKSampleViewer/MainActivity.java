package jp.co.tikyus.QuomoSDKSampleViewer;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Timer;
//import java.util.TimerTask;

import jp.co.tikyus.quomo.sdk;
import jp.co.tikyus.work001.R;

import android.view.View;
import android.widget.Toast;
import android.widget.Button;
import java.util.ArrayList;

import android.widget.TextView;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("quomosdk");
    }


//    private Timer timer;
//    private QuomoTimerTask timerTask = null;
//    private sdk _quomosdk;
//    private Boolean flgTimer = false;
//    private Handler handler = new Handler();
//    private ArrayList codelist = new ArrayList();


    //レイアウト要素
    Button button;
    Button input;
    TextView titleView;
    TextView dateView;

    //API
//    private final String API_URL_PREFIX = "www.googleapis.com";
    private final String API_URL_PREFIX = "xigu4d46-nta.nta81online.com";
//    private final String API_URL_PREFIX = "ap-northeast-1.console.aws.amazon.com/cloud9/ide/7abc108467f6450fb0209fe2b333c1f6";
//    https://xigu4d46-nta.nta81online.com/Quomo/checkin/69/MzzxsDlDG9H2NpGN/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = findViewById(R.id.input);

        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //②この中でAPIを叩きます。
                //入力された数字をEditText型のeditTextValueに入れる
                EditText editUserName = (EditText) findViewById(R.id.username);
                EditText editEventCode = (EditText) findViewById(R.id.eventcode);

                editUserName.setText("h.watanabe.tikyus.co.jp@gmail.com");
                editEventCode.setText("WES7LP");

//                Button buttonlongin = (Button) findViewById(R.id.login);
//                buttonlongin.setEnabled(true);
            }
        });

        button = findViewById(R.id.login);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //②この中でAPIを叩きます。
//                Display_start(v);
                //APItest(v);
                EditText editUserName = (EditText) findViewById(R.id.username);
                EditText editEventCode = (EditText) findViewById(R.id.eventcode);

                String username = editUserName.getText().toString();     // 入力文字の取得
                String eventcode = editEventCode.getText().toString();

                if(username.length() != 0 && eventcode.length() != 0){
                    MyAsync asynk = new MyAsync(titleView, dateView);
                    asynk.execute();
                }

//                Intent intent = new Intent(MainActivity.this, ColorActivity.class);
//                startActivity(intent);


                //http://android-note.open-memo.net/sub/activity__set_result_of_finish.html
/*
                Intent resultIntent = new Intent(ImageChoiceActivity.this, ImageActivity.class);
                resultIntent.putExtra("IMAGE_PATH",
                        "選択された画像パス");
                setResult(RESULT_OK, resultIntent);
                finish();

 */
            }
        });


    }

/*
        //Quomo初期設定
        _quomosdk=new sdk();
        _quomosdk.QuomoInitialize();
        boolean licenseResult;
        licenseResult = _quomosdk.QuomoSetProductKey("1148112069b0be3b20dacf2399a8266e88afcd15476589febf64a6fd4ec07800276c605e7c5a0c336fd6f566c1a187cb6cb2f15368b8be66ac7d333c97b1557b");
        Log.d("MAIN","licenseResult:"+licenseResult);

        _quomosdk.QuomoAddCode(10001L);
        _quomosdk.QuomoAddCode(12345678L);
        _quomosdk.QuomoAddCode(21212123L);

        //ビューワー用タイマー設定
        timer = new Timer();
        timerTask = new QuomoTimerTask();
        timer.schedule(timerTask, 0, 1);

    }
 */

/*
    class QuomoTimerTask extends TimerTask {
        @Override
        public void run() {
            if (flgTimer) {
                return;
            }
            flgTimer = true;
            handler.post(new Runnable() {
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Bitmap bitmap = _quomosdk.makeBitmap();
                            ImageView imageView = (ImageView) findViewById(R.id.imageView);
                            imageView.setImageBitmap(bitmap);
                            _quomosdk.deleteBitmap();
                            flgTimer = false;
                        }
                    });


                }
            });
        }
    }
*/
/*
    //ボタンをクリックした時に呼ばれる
    public void Display_start(View view) {

        //入力された数字をEditText型のeditTextValueに入れる
        EditText editTextValue = (EditText) findViewById(R.id.EditText);
        //Integer.parseIntでString型をint型に変換
        long value = Long.parseLong(editTextValue.getText().toString());

        // ここに処理を記述
        //Quomo初期設定
        _quomosdk = new sdk();
        _quomosdk.QuomoInitialize();
        boolean licenseResult;
        licenseResult = _quomosdk.QuomoSetProductKey("1148112069b0be3b20dacf2399a8266e88afcd15476589febf64a6fd4ec07800276c605e7c5a0c336fd6f566c1a187cb6cb2f15368b8be66ac7d333c97b1557b");
        Log.d("MAIN", "licenseResult:" + licenseResult);

        codelist.add(value);
        for (int i = 0; i < codelist.size(); i++) {
            _quomosdk.QuomoAddCode(Long.parseLong(codelist.get(i).toString()));
        }

        //ビューワー用タイマー設定
        timer = new Timer();
        timerTask = new QuomoTimerTask();
        timer.schedule(timerTask, 0, 1);
    }
*/
    public void APItest(View view) {

        //入力された数字をEditText型のeditTextValueに入れる
//        EditText editTextValue = (EditText) findViewById(R.id.EditText);
//        editTextValue.setText("成功");
        //Integer.parseIntでString型をint型に変換
//        long value = Long.parseLong(editTextValue.getText().toString());

    }


    class MyAsync extends AsyncTask<String, Void, String> {

        private final WeakReference<TextView> titleViewReference;
        private final WeakReference<TextView> dateViewReference;

        public MyAsync(TextView titletView, TextView dateView) {
            titleViewReference = new WeakReference<TextView>(titletView);
            dateViewReference = new WeakReference<TextView>(dateView);
        }


        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection con = null;
            StringBuffer result = new StringBuffer();
            Uri.Builder uriBuilder = new Uri.Builder();

            uriBuilder.scheme("https");
            uriBuilder.authority(API_URL_PREFIX);

            EditText EventcodeEdit = (EditText)findViewById(R.id.eventcode);
            EditText UsernameEdit = (EditText)findViewById(R.id.username);

            String eventcode = EventcodeEdit.getText().toString();
            String username = UsernameEdit.getText().toString();

            JSONObject jsonObject = new JSONObject();


            try {
                jsonObject.put("email" , username)
                          .put("event_code" , eventcode);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            //qr_key取得
            //uriBuilder.path("/Quomo/get_Quomo_qrkey");
            uriBuilder.path("/Quomo/get_Quomo_qrkey_checkpoint");
            //String json = "{\"email\":\"h.watanabe.tikyus.co.jp@gmail.com\",\"event_code\":\"WES7LP\"}";

            //チェックイン結果取得
            //uriBuilder.path("/Quomo/get_Quomo_result");
            //String json = "{\"customer_id\":\"15284\",\"checkpoint_id\":\"69\"}";

            //チェックポイントリスト取得（本日分）
            //uriBuilder.path("/Quomo/get_Quomo_checkpoint");
            //String json = "{\"event_code\":\"WES7LP\"}";

            //チェックイン
            //uriBuilder.path("/Quomo/Quomo_checkin");
            //String json = "{\"event_code\":\"WES7LP\",\"qr_key\":\"12345678\",\"checkpoint_id\":\"69\"}";

            String json = String.valueOf(jsonObject);

            final String uriStr = uriBuilder.build().toString();

            try {

                URL url = new URL(uriStr);
                con = (HttpURLConnection) url.openConnection();
                // HTTPリクエストコード
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.setRequestProperty("Accept-Language", "jp");
                // データがJSONであること、エンコードを指定する
                con.setRequestProperty("Content-Type", "application/JSON; charset=utf-8");
                // POSTデータの長さを設定
                con.setRequestProperty("Content-Length", String.valueOf(json.length()));
                // リクエストのbodyにJSON文字列を書き込む
                OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
                out.write(json);
                out.flush();
                con.connect();

                // HTTPレスポンスコード
                final int status = con.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    // 通信に成功した
                    // テキストを取得する
                    final InputStream in = con.getInputStream();
                    String encoding = con.getContentEncoding();
                    //EUC_JP,SJIS
                    if (null == encoding) {
                        encoding = "UTF-8";
                        //encoding = "SJIS";
                    }
                    final InputStreamReader inReader = new InputStreamReader(in, encoding);
                    final BufferedReader bufReader = new BufferedReader(inReader);

                    String line = bufReader.readLine();
                    result.append(line);

                    // JSONObject に変換します
//                    JSONObject job = new JSONObject(line);
//
//                    result.append(job);
/*
                    String wk2 =  job.toString();

                    // JSONObject を文字列に変換してログ出力します
                    Log.d("debug", wk2);
*/
/*


                    //String day = response.getJSONObject("open").getString("day");

                    // キー"user"の値（JSONオブジェクト）をパース

                    //JSONObject item = job.getJSONObject("checkpoint_list");

                    // 各キーの値を出力
                    //Integer id = item.getInt("_id");
                    //Integer event_id = item.getInt("_event_id");


*/
/*
                    String line = null;
                    // 1行ずつテキストを読み込む
                    while ((line = bufReader.readLine()) != null) {
                        result.append(line);
                    }
*/
                    bufReader.close();
                    inReader.close();
                    in.close();
                } else {
                    // 通信が失敗した場合のレスポンスコードを表示
                    System.out.println(status);
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (con != null) {
                    // コネクションを切断
                    con.disconnect();
                }
            }

            String wk =  result.toString();

            return wk;

//            return result.toString();
        }

//android post 送信
//https://akira-watson.com/android/httpurlconnection-post.html
//put用

/*
        @Override
        protected String doInBackground(String... params) {
//            String word = "word="+params[0];
            String word = "id=12345678";
            String postret = null;
            String buffer = "";

            final StringBuilder result = new StringBuilder();
            Uri.Builder uriBuilder = new Uri.Builder();
            uriBuilder.scheme("https");
            uriBuilder.authority(API_URL_PREFIX);
//            uriBuilder.path("/books/v1/volumes");
//            uriBuilder.path("/Quomo/checkin/69/12345678");
            uriBuilder.path("/Quomo/test");

            //key=value&をつけたい場合はこちら
//            uriBuilder.appendQueryParameter("q", "夏目漱石");
            final String uriStr = uriBuilder.build().toString();

            final String json =
                    "{" +
                        "\"email\":\"wata@test.net\","+
                        "\"Event_id\":\"ABCDEF\","+
                    "}";

            try {
                URL url = new URL(uriStr);
                HttpURLConnection con = null;

 //               con = (HttpURLConnection) url.openConnection();
 //               con.setRequestMethod("GET");
 //               con.setDoInput(true);   //レスポンスデータを許可

 //               con.setRequestProperty("Accept-Language", "jp");
 //               con.setRequestProperty("Content-Type", "application/json; charset=utf-8");

 //               con.connect(); //HTTP接続

//                OutputStream os = con.getOutputStream();
//                PrintStream ps = new PrintStream(os);
//                ps.print(json);
//                ps.close();




                con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoInput(true);   //レスポンスデータを許可
                con.setInstanceFollowRedirects(false);
//                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                con.setRequestProperty("Accept", "application/json");
                OutputStream os = con.getOutputStream();
                PrintStream ps = new PrintStream(os);
                ps.print(json);
                ps.close();

                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                buffer = reader.readLine();

                JSONArray jsonArray = new JSONArray(buffer);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Log.d("HTTP REQ", jsonObject.getString("name"));
                }

                con.disconnect();

                // 時間制限
//                con.setReadTimeout(10000);
//                con.setConnectTimeout(20000);




//                try(// POSTデータ送信処理
//                    OutputStream outStream = con.getOutputStream()) {
//                    outStream.write( word.getBytes(StandardCharsets.UTF_8));
//                    outStream.flush();
//                    Log.d("debug","flush");
//                } catch (Exception e) {
//                    // POST送信エラー
//                    e.printStackTrace();
//                    postret = "POST送信エラー";
//                }
//                final int status = con.getResponseCode();
//                if (status == HttpURLConnection.HTTP_OK) {
//                    // レスポンスを受け取る処理等
//                    postret="HTTP_OK";
//                }
//                else{
//                    postret="status="+String.valueOf(status);
//                }


//                final InputStream in = con.getInputStream();
//                final InputStreamReader inReader = new InputStreamReader(in);
//                final BufferedReader bufReader = new BufferedReader(inReader);

//                String line = null;
//                while ((line = bufReader.readLine()) != null) {
//                    result.append(line);
//                }
//                Log.e("but", result.toString());
//                bufReader.close();
//                inReader.close();
//                in.close();

            } catch (Exception e) { //エラー
                Log.e("button", e.getMessage());
            }

            return result.toString(); //onPostExecuteへreturn
        }
*/
//get用
/*
        @Override
        protected String doInBackground(String... params) {

            final StringBuilder result = new StringBuilder();
            Uri.Builder uriBuilder = new Uri.Builder();
            uriBuilder.scheme("https");
            uriBuilder.authority(API_URL_PREFIX);
//            uriBuilder.path("/books/v1/volumes");
//            uriBuilder.path("/Quomo/checkin/69/12345678");
            uriBuilder.path("/Quomo/test");

            //key=value&をつけたい場合はこちら
//            uriBuilder.appendQueryParameter("q", "夏目漱石");
            final String uriStr = uriBuilder.build().toString();

            try {
                URL url = new URL(uriStr);
                HttpURLConnection con = null;
                con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setDoInput(true);   //レスポンスデータを許可

                con.connect(); //HTTP接続

                final InputStream in = con.getInputStream();
                final InputStreamReader inReader = new InputStreamReader(in);
                final BufferedReader bufReader = new BufferedReader(inReader);

                String line = null;
                while ((line = bufReader.readLine()) != null) {
                    result.append(line);
                }
                Log.e("but", result.toString());
                bufReader.close();
                inReader.close();
                in.close();
            } catch (Exception e) { //エラー
                Log.e("button", e.getMessage());
            }

            return result.toString(); //onPostExecuteへreturn
        }
*/
        @Override
        protected void onPostExecute(String result) { //doInBackgroundが終わると呼び出される
            try {
/*
                JSONObject json = new JSONObject(result);
                String items = json.getString("items");
                JSONArray itemsArray = new JSONArray(items);
                JSONObject bookInfo = itemsArray.getJSONObject(0).getJSONObject("volumeInfo");

                String title = bookInfo.getString("title");
                String publishedDate = bookInfo.getString("publishedDate");
*/
/*
                // JSONオブジェクトのインスタンス作成
                JSONObject jsonObj = new JSONObject(sbSentence.toString());
                // キー"user"の値（JSONオブジェクト）をパース
                JSONObject item = jsonObj.getJSONObject("user");
                // 各キーの値を出力
                System.out.println(item.get("id"));
                System.out.println(item.get("name"));
*/

                // JSONオブジェクトのインスタンス作成
                JSONObject jsonObj = new JSONObject(result);
                // キー"users"の値（JSON配列オブジェクト）をパース
                Long customer_id = jsonObj.getLong("customer_id");
                Long qr_key = jsonObj.getLong("qr_key");
                Boolean result_qr_key = jsonObj.getBoolean("result_qr_key");
                Boolean result_list = jsonObj.getBoolean("result_list");
                JSONArray checkpoint_list = jsonObj.getJSONArray("checkpoint_list");
/*
                JSONArray _id = jsonObj.getJSONArray("checkpoint_list");
                JSONArray _name = jsonObj.getJSONArray("checkpoint_list");
                JSONArray _day = jsonObj.getJSONArray("checkpoint_list");
                JSONArray _start_time = jsonObj.getJSONArray("checkpoint_list");
                JSONArray _end_time = jsonObj.getJSONArray("checkpoint_list");
*/
                //checkpoint_list":[{
                // "_id":"70",
                // "_event_id":"33",
                // "_name":"\u3072\u3064\u3088\u3046",
                // "_day":"2020-11-10",
                // "_start_time":"15:00:00",
                // "_end_time":"16:00:00",
                // "_note":"","_participate_option_flag":1,"_create_user_id":"1","_created":"1604890340","_updated":"1604890352","_del_flag":"0"}

                ArrayList<String> list = new ArrayList<String>();
                ArrayList<String> _id = new ArrayList<String>();
                ArrayList<String> _name = new ArrayList<String>();
                ArrayList<String> _day = new ArrayList<String>();
                ArrayList<String> _start_time = new ArrayList<String>();
                ArrayList<String> _end_time = new ArrayList<String>();
//                List<IParcelable> values = new List<IParcelable>();
/*
                if (checkpoint_list != null) {
                    int len = checkpoint_list.length();
                    for (int i=0;i<len;i++){
                        list.add(checkpoint_list.get(i).toString());
                    }
                }
*/

                for (int i = 0; i < checkpoint_list.length(); i++) {
                    list.add(checkpoint_list.get(i).toString());

                    JSONObject jsonObject = checkpoint_list.getJSONObject(i);
                    _id.add(jsonObject.getString("_id"));
                    _name.add(jsonObject.getString("_name"));
                    _day.add(jsonObject.getString("_day"));
                    _start_time.add(jsonObject.getString("_start_time"));
                    _end_time.add(jsonObject.getString("_end_time"));
                }


                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putExtra("customer_id", customer_id);
                intent.putExtra("qr_key", qr_key);
                intent.putExtra("result_qr_key", result_qr_key);
                intent.putExtra("result_list", result_list);
//                intent.putExtra("checkpoint_list", list);
                intent.putStringArrayListExtra("checkpoint_list", list);
                intent.putStringArrayListExtra("_id", _id);
                intent.putStringArrayListExtra("_name", _name);
                intent.putStringArrayListExtra("_day", _day);
                intent.putStringArrayListExtra("_start_time", _start_time);
                intent.putStringArrayListExtra("_end_time", _end_time);
//                intent.putStringArrayListExtra("checkpoint_list", list);

/*
                ContentValues cv = new ContentValues();
                cv.put("checkpoint_list", "checkpoint_list");

                Bundle b = new Bundle();
                b.putParcelable("List", cv);
                intent.putExtras(b);
*/
                finish();
                startActivity(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
